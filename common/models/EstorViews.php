<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "SYS.USER_VIEWS".
 *
 * @property string $VIEW_NAME
 * @property string $TEXT_LENGTH
 * @property string $TEXT
 * @property string $TYPE_TEXT_LENGTH
 * @property string $TYPE_TEXT
 * @property string $OID_TEXT_LENGTH
 * @property string $OID_TEXT
 * @property string $VIEW_TYPE_OWNER
 * @property string $VIEW_TYPE
 * @property string $SUPERVIEW_NAME
 * @property string $EDITIONING_VIEW
 * @property string $READ_ONLY
 */
class EstorViews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SYS.USER_VIEWS';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['VIEW_NAME'], 'required'],
            [['TEXT_LENGTH', 'TYPE_TEXT_LENGTH', 'OID_TEXT_LENGTH'], 'number'],
            [['TEXT'], 'string'],
            [['VIEW_NAME', 'VIEW_TYPE_OWNER', 'VIEW_TYPE', 'SUPERVIEW_NAME'], 'string', 'max' => 30],
            [['TYPE_TEXT', 'OID_TEXT'], 'string', 'max' => 4000],
            [['EDITIONING_VIEW', 'READ_ONLY'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'VIEW_NAME' => Yii::t('app', 'View  Name'),
            'TEXT_LENGTH' => Yii::t('app', 'Text  Length'),
            'TEXT' => Yii::t('app', 'Text'),
            'TYPE_TEXT_LENGTH' => Yii::t('app', 'Type  Text  Length'),
            'TYPE_TEXT' => Yii::t('app', 'Type  Text'),
            'OID_TEXT_LENGTH' => Yii::t('app', 'Oid  Text  Length'),
            'OID_TEXT' => Yii::t('app', 'Oid  Text'),
            'VIEW_TYPE_OWNER' => Yii::t('app', 'View  Type  Owner'),
            'VIEW_TYPE' => Yii::t('app', 'View  Type'),
            'SUPERVIEW_NAME' => Yii::t('app', 'Superview  Name'),
            'EDITIONING_VIEW' => Yii::t('app', 'Editioning  View'),
            'READ_ONLY' => Yii::t('app', 'Read  Only'),
        ];
    }
}
