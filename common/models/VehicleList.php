<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "VEHICLE_LIST".
 *
 * @property string $ID
 * @property string $REG_NO
 * @property string $MODEL
 * @property string $TYPE
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class VehicleList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'CREATED_AT',
                'updatedAtAttribute' => 'UPDATED_AT',
                'value' => date('d-M-y h.i.s a'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CREATED_BY',
                'updatedByAttribute' => 'UPDATED_BY',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'VEHICLE_LIST';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['ID'], 'required'],
            [['ID'], 'number'],
            [['CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
            [['CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['REG_NO', 'TYPE'], 'string', 'max' => 20],
            [['MODEL'], 'string', 'max' => 100],
            [['ID', 'ID'], 'unique', 'targetAttribute' => ['ID', 'ID'], 'message' => 'The combination of  and ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'REG_NO' => Yii::t('app', 'No Plat'),
            'MODEL' => Yii::t('app', 'Model'),
            'TYPE' => Yii::t('app', 'Jenis'),
            'CREATED_AT' => Yii::t('app', 'Dijana Pada'),
            'UPDATED_AT' => Yii::t('app', 'Dikemaskini Pada'),
            'CREATED_BY' => Yii::t('app', 'Dijana Oleh'),
            'UPDATED_BY' => Yii::t('app', 'Dikemaskini Oleh'),
            'DELETED' => Yii::t('app', 'Status'),
            'DELETED_AT' => Yii::t('app', 'Tarikh Kemaskini Status'),
        ];
    }
    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
    }
    public function getUpdator() {
        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
    }
}
