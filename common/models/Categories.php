<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "CATEGORIES".
 *
 * @property string $ID
 * @property string $NAME
 * @property integer $DELETED
 * @property string $DELETED_AT
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'CREATED_AT',
                'updatedAtAttribute' => 'UPDATED_AT',
                'value' => date('d-M-y h.i.s a'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CREATED_BY',
                'updatedByAttribute' => 'UPDATED_BY',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CATEGORIES';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['ID'], 'required'],
            [['ID'], 'number'],
            [['DELETED', 'CREATED_BY', 'UPDATED_BY'], 'integer'],
            [['DELETED_AT', 'CREATED_AT', 'UPDATED_AT'], 'safe'],
            [['NAME'], 'string', 'max' => 255],
            [['ID', 'ID'], 'unique', 'targetAttribute' => ['ID', 'ID'], 'message' => 'The combination of  and ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'NAME' => Yii::t('app', 'Kategori'),
            'DELETED' => Yii::t('app', 'Status'),
            'DELETED_AT' => Yii::t('app', 'Tarikh Kemaskini Status'),
            'CREATED_AT' => Yii::t('app', 'Tarikh Dijana'),
            'UPDATED_AT' => Yii::t('app', 'Tarikh Dikemaskini'),
            'CREATED_BY' => Yii::t('app', 'Dijana Oleh'),
            'UPDATED_BY' => Yii::t('app', 'Dikemaskini Oleh'),
        ];
    }
    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
    }
    public function getUpdator() {
        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
    }
}
