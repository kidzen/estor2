<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "ACTIVITY_LOGS".
 *
 * @property string $ID
 * @property integer $USER_ID
 * @property string $REMOTE_IP
 * @property string $ACTION
 * @property string $CONTROLLER
 * @property string $PARAMS
 * @property string $ROUTE
 * @property string $STATUS
 * @property string $MESSAGES
 * @property string $CREATED_AT
 */
class ActivityLogs extends \yii\db\ActiveRecord {

    const LOG_STATUS_SUCCESS = 'success';
    const LOG_STATUS_INFO = 'info';
    const LOG_STATUS_WARNING = 'warning';
    const LOG_STATUS_ERROR = 'error';
    const LOG_STATUS_FAIL = 'fail';

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'CREATED_AT',
                ],
                'value' => date('d-M-y h.i.s a'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'USER_ID',
                ],
                'value' => Yii::$app->user->id,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'ACTIVITY_LOGS';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['ID'], 'number'],
            [['USER_ID'], 'integer'],
            [['CREATED_AT'], 'safe'],
            [['REMOTE_IP', 'ACTION', 'CONTROLLER', 'ROUTE', 'STATUS', 'MESSAGES'], 'string', 'max' => 255],
            [['PARAMS'], 'string', 'max' => 4000], 
//            [['ID', 'ID', 'ID', 'ID', 'ID'], 'unique', 'targetAttribute' => ['ID', 'ID', 'ID', 'ID', 'ID'], 'message' => 'The combination of  and ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'ID' => 'ID',
            'USER_ID' => 'Author',
            'REMOTE_IP' => 'Remote  Ip',
            'ACTION' => 'Action',
            'CONTROLLER' => 'Controller',
            'PARAMS' => 'Params',
            'ROUTE' => 'Route',
            'STATUS' => 'Status',
            'MESSAGES' => 'Messages',
            'CREATED_AT' => 'Created  At',
        ];
    }

    /**
     * Adds a message to ActionLog model
     *
     * @param string $status The log status information
     * @param mixed $message The log message
     * @param int $uID The user id
     */
    public static function add($status = null, $message = null) {
        if (Yii::$app->params['logActivity']) {

            $model = Yii::createObject(__CLASS__);
//            $model = new ActivityLogs();
//            $model->USER_ID = (int) Yii::$app->user->id;
            $model->REMOTE_IP = $_SERVER['REMOTE_ADDR'];
            $model->ACTION = Yii::$app->requestedAction->id;
            $model->CONTROLLER = Yii::$app->requestedAction->controller->id;
            if (Yii::$app->request->isPost) {
                $posted = Yii::$app->request->post();
                unset($posted['_csrf']);
                $model->PARAMS = serialize($posted);
            } else {
                $model->PARAMS = serialize(Yii::$app->requestedAction->controller->actionParams);
            }
            $model->ROUTE = Yii::$app->requestedRoute;
            $model->STATUS = $status;
            $model->MESSAGES = ($message !== null) ? serialize($message) : null;
            if (!$model->save())
                Yii::$app->notify->warning('fail to track');
//            return $model->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(People::className(), ['ID' => 'USER_ID']);
    }

}
