<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[TransactionsAll]].
 *
 * @see TransactionsAll
 */
class TransactionsAllQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TransactionsAll[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TransactionsAll|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
