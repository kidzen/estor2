<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "SYS.USER_TABLES".
 *
 * @property string $TABLE_NAME
 * @property string $TABLESPACE_NAME
 * @property string $CLUSTER_NAME
 * @property string $IOT_NAME
 * @property string $STATUS
 * @property string $PCT_FREE
 * @property string $PCT_USED
 * @property string $INI_TRANS
 * @property string $MAX_TRANS
 * @property string $INITIAL_EXTENT
 * @property string $NEXT_EXTENT
 * @property string $MIN_EXTENTS
 * @property string $MAX_EXTENTS
 * @property string $PCT_INCREASE
 * @property string $FREELISTS
 * @property string $FREELIST_GROUPS
 * @property string $LOGGING
 * @property string $BACKED_UP
 * @property string $NUM_ROWS
 * @property string $BLOCKS
 * @property string $EMPTY_BLOCKS
 * @property string $AVG_SPACE
 * @property string $CHAIN_CNT
 * @property string $AVG_ROW_LEN
 * @property string $AVG_SPACE_FREELIST_BLOCKS
 * @property string $NUM_FREELIST_BLOCKS
 * @property string $DEGREE
 * @property string $INSTANCES
 * @property string $CACHE
 * @property string $TABLE_LOCK
 * @property string $SAMPLE_SIZE
 * @property string $LAST_ANALYZED
 * @property string $PARTITIONED
 * @property string $IOT_TYPE
 * @property string $TEMPORARY
 * @property string $SECONDARY
 * @property string $NESTED
 * @property string $BUFFER_POOL
 * @property string $FLASH_CACHE
 * @property string $CELL_FLASH_CACHE
 * @property string $ROW_MOVEMENT
 * @property string $GLOBAL_STATS
 * @property string $USER_STATS
 * @property string $DURATION
 * @property string $SKIP_CORRUPT
 * @property string $MONITORING
 * @property string $CLUSTER_OWNER
 * @property string $DEPENDENCIES
 * @property string $COMPRESSION
 * @property string $COMPRESS_FOR
 * @property string $DROPPED
 * @property string $READ_ONLY
 * @property string $SEGMENT_CREATED
 * @property string $RESULT_CACHE
 */
class EstorTables extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SYS.USER_TABLES';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TABLE_NAME'], 'required'],
            [['PCT_FREE', 'PCT_USED', 'INI_TRANS', 'MAX_TRANS', 'INITIAL_EXTENT', 'NEXT_EXTENT', 'MIN_EXTENTS', 'MAX_EXTENTS', 'PCT_INCREASE', 'FREELISTS', 'FREELIST_GROUPS', 'NUM_ROWS', 'BLOCKS', 'EMPTY_BLOCKS', 'AVG_SPACE', 'CHAIN_CNT', 'AVG_ROW_LEN', 'AVG_SPACE_FREELIST_BLOCKS', 'NUM_FREELIST_BLOCKS', 'SAMPLE_SIZE'], 'number'],
            [['TABLE_NAME', 'TABLESPACE_NAME', 'CLUSTER_NAME', 'IOT_NAME', 'CLUSTER_OWNER'], 'string', 'max' => 30],
            [['STATUS', 'TABLE_LOCK', 'ROW_MOVEMENT', 'SKIP_CORRUPT', 'DEPENDENCIES', 'COMPRESSION'], 'string', 'max' => 8],
            [['LOGGING', 'PARTITIONED', 'NESTED', 'GLOBAL_STATS', 'USER_STATS', 'MONITORING', 'DROPPED', 'READ_ONLY', 'SEGMENT_CREATED'], 'string', 'max' => 3],
            [['BACKED_UP', 'TEMPORARY', 'SECONDARY'], 'string', 'max' => 1],
            [['DEGREE', 'INSTANCES'], 'string', 'max' => 10],
            [['CACHE'], 'string', 'max' => 5],
            [['LAST_ANALYZED', 'BUFFER_POOL', 'FLASH_CACHE', 'CELL_FLASH_CACHE', 'RESULT_CACHE'], 'string', 'max' => 7],
            [['IOT_TYPE', 'COMPRESS_FOR'], 'string', 'max' => 12],
            [['DURATION'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'TABLE_NAME' => Yii::t('app', 'Table  Name'),
            'TABLESPACE_NAME' => Yii::t('app', 'Tablespace  Name'),
            'CLUSTER_NAME' => Yii::t('app', 'Cluster  Name'),
            'IOT_NAME' => Yii::t('app', 'Iot  Name'),
            'STATUS' => Yii::t('app', 'Status'),
            'PCT_FREE' => Yii::t('app', 'Pct  Free'),
            'PCT_USED' => Yii::t('app', 'Pct  Used'),
            'INI_TRANS' => Yii::t('app', 'Ini  Trans'),
            'MAX_TRANS' => Yii::t('app', 'Max  Trans'),
            'INITIAL_EXTENT' => Yii::t('app', 'Initial  Extent'),
            'NEXT_EXTENT' => Yii::t('app', 'Next  Extent'),
            'MIN_EXTENTS' => Yii::t('app', 'Min  Extents'),
            'MAX_EXTENTS' => Yii::t('app', 'Max  Extents'),
            'PCT_INCREASE' => Yii::t('app', 'Pct  Increase'),
            'FREELISTS' => Yii::t('app', 'Freelists'),
            'FREELIST_GROUPS' => Yii::t('app', 'Freelist  Groups'),
            'LOGGING' => Yii::t('app', 'Logging'),
            'BACKED_UP' => Yii::t('app', 'Backed  Up'),
            'NUM_ROWS' => Yii::t('app', 'Num  Rows'),
            'BLOCKS' => Yii::t('app', 'Blocks'),
            'EMPTY_BLOCKS' => Yii::t('app', 'Empty  Blocks'),
            'AVG_SPACE' => Yii::t('app', 'Avg  Space'),
            'CHAIN_CNT' => Yii::t('app', 'Chain  Cnt'),
            'AVG_ROW_LEN' => Yii::t('app', 'Avg  Row  Len'),
            'AVG_SPACE_FREELIST_BLOCKS' => Yii::t('app', 'Avg  Space  Freelist  Blocks'),
            'NUM_FREELIST_BLOCKS' => Yii::t('app', 'Num  Freelist  Blocks'),
            'DEGREE' => Yii::t('app', 'Degree'),
            'INSTANCES' => Yii::t('app', 'Instances'),
            'CACHE' => Yii::t('app', 'Cache'),
            'TABLE_LOCK' => Yii::t('app', 'Table  Lock'),
            'SAMPLE_SIZE' => Yii::t('app', 'Sample  Size'),
            'LAST_ANALYZED' => Yii::t('app', 'Last  Analyzed'),
            'PARTITIONED' => Yii::t('app', 'Partitioned'),
            'IOT_TYPE' => Yii::t('app', 'Iot  Type'),
            'TEMPORARY' => Yii::t('app', 'Temporary'),
            'SECONDARY' => Yii::t('app', 'Secondary'),
            'NESTED' => Yii::t('app', 'Nested'),
            'BUFFER_POOL' => Yii::t('app', 'Buffer  Pool'),
            'FLASH_CACHE' => Yii::t('app', 'Flash  Cache'),
            'CELL_FLASH_CACHE' => Yii::t('app', 'Cell  Flash  Cache'),
            'ROW_MOVEMENT' => Yii::t('app', 'Row  Movement'),
            'GLOBAL_STATS' => Yii::t('app', 'Global  Stats'),
            'USER_STATS' => Yii::t('app', 'User  Stats'),
            'DURATION' => Yii::t('app', 'Duration'),
            'SKIP_CORRUPT' => Yii::t('app', 'Skip  Corrupt'),
            'MONITORING' => Yii::t('app', 'Monitoring'),
            'CLUSTER_OWNER' => Yii::t('app', 'Cluster  Owner'),
            'DEPENDENCIES' => Yii::t('app', 'Dependencies'),
            'COMPRESSION' => Yii::t('app', 'Compression'),
            'COMPRESS_FOR' => Yii::t('app', 'Compress  For'),
            'DROPPED' => Yii::t('app', 'Dropped'),
            'READ_ONLY' => Yii::t('app', 'Read  Only'),
            'SEGMENT_CREATED' => Yii::t('app', 'Segment  Created'),
            'RESULT_CACHE' => Yii::t('app', 'Result  Cache'),
        ];
    }
}
