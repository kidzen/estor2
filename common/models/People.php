<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use common\models\Roles;
use common\models\MpspStaff;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\Security;
use common\models\People;
use yii\web\UploadedFile;

/**
 * This is the model class for table "PEOPLE".
 *
 * @property string $ID
 * @property string $USERNAME
 * @property string $EMAIL
 * @property string $PASSWORD
 * @property integer $ROLE_ID
 * @property integer $DELETED
 * @property string $DELETED_AT
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property string $PASSWORD_RESET_TOKEN
 * @property string $AUTH_KEY
 */
class People extends ActiveRecord implements IdentityInterface {

    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public $PASSWORD_TEMP;
    // public $STAFF_NO;
    public $REPASSWORD_TEMP;
    public $PROFILE_PIC_FILE;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'CREATED_AT',
                'updatedAtAttribute' => 'UPDATED_AT',
                'value' => date('d-M-y h.i.s a'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'PEOPLE';
    }

    public static function attribues() {
        return array_merge(parent::attributes(),[
            'mpspProfile.NAMA', 'mpspProfile.JAWATAN','mpspProfile.KETERANGAN',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['ID','STAFF_NO'], 'number'],
            [['STAFF_NO', 'EMAIL'], 'required'],
           // [['USERNAME', 'EMAIL'], 'required'],
            [['PASSWORD_TEMP', 'REPASSWORD_TEMP'], 'required', 'on' => ['admin-create']],
            ['REPASSWORD_TEMP', 'compare', 'compareAttribute' => 'PASSWORD_TEMP', 'message' => 'Both password are not the same.'],
            [['ROLE_ID', 'DELETED'], 'integer'],
//            [['ROLE_ID', 'DELETED'], 'integer'],
            [['DELETED_AT', 'CREATED_AT', 'UPDATED_AT'], 'safe'],
//            [['USERNAME', 'EMAIL', 'PASSWORD','PASSWORD_RESET_TOKEN', 'AUTH_KEY'], 'string', 'max' => 255],
//            [['USERNAME', 'PROFILE_PIC', 'EMAIL', 'PASSWORD', 'PROFILE_PIC', 'PASSWORD_TEMP', 'REPASSWORD_TEMP', 'PASSWORD_RESET_TOKEN', 'AUTH_KEY'], 'string', 'max' => 255],
            [['USERNAME', 'STAFF_NO', 'PROFILE_PIC', 'EMAIL', 'PASSWORD', 'PROFILE_PIC', 'PASSWORD_TEMP', 'REPASSWORD_TEMP', 'PASSWORD_RESET_TOKEN', 'AUTH_KEY'], 'string', 'max' => 255],
            [['mpspProfile.NAMA', 'mpspProfile.JAWATAN','mpspProfile.KETERANGAN',], 'string', 'max' => 255],
            ['USERNAME', 'unique', 'message' => 'The combination of  and ID has already been taken.'],
//            [['STAFF_NO'], 'unique', 'message' => 'The Staff No has already been registered.'],
//            [['ID'], 'unique', 'targetAttribute' => ['ID'], 'message' => 'The combination of  and ID has already been taken.'],
            [['ID', 'ID'], 'unique', 'targetAttribute' => ['ID', 'ID'], 'message' => 'The combination of  and ID has already been taken.'],
            [['STAFF_NO', 'STAFF_NO'], 'unique', 'targetAttribute' => ['STAFF_NO', 'STAFF_NO'], 'message' => 'The combination of  and STAFF NO has already been taken.'],
            [['ROLE_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Roles::className(), 'targetAttribute' => ['ROLE_ID' => 'ID']],
            ['DELETED', 'default', 'value' => self::STATUS_ACTIVE],
            ['DELETED', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['PROFILE_PIC_FILE'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    public function scenarios() {
        return [
            'default' => ['USERNAME', 'STAFF_NO', 'EMAIL', 'PASSWORD','DELETED'],
//            'default' => ['STAFF_NO', 'EMAIL', 'PASSWORD'],
            'admin-create' => ['USERNAME', 'EMAIL', 'PASSWORD_TEMP', 'REPASSWORD_TEMP', 'ROLE_ID', 'PROFILE_PIC', 'STAFF_NO'],
            'admin-update' => ['USERNAME', 'EMAIL', 'PASSWORD_TEMP', 'REPASSWORD_TEMP', 'ROLE_ID', 'PROFILE_PIC', 'STAFF_NO','DELETED']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'ID' => Yii::t('app', 'ID'),
            'USERNAME' => Yii::t('app', 'Id Staff'),
            'STAFF_NO' => Yii::t('app', 'No Pekerja'),
//            'NAME' => Yii::t('app', 'Nama Penuh'),
//            'POSITION' => Yii::t('app', 'Jawatan'),
//            'DEPARTMENT' => Yii::t('app', 'Jabatan'),
            'PROFILE_PIC' => Yii::t('app', 'Gambar'),
            'PROFILE_PIC_FILE' => Yii::t('app', 'Gambar Profil'),
            'EMAIL' => Yii::t('app', 'Email'),
            'PASSWORD' => Yii::t('app', 'Kata Laluan'),
            'PASSWORD_TEMP' => Yii::t('app', 'Kata Laluan'),
            'REPASSWORD_TEMP' => Yii::t('app', 'Ulang Kata Laluan'),
            'ROLE_ID' => Yii::t('app', 'ID Akses'),
            'DELETED' => Yii::t('app', 'Status'),
            'DELETED_AT' => Yii::t('app', 'Tarikh Kemaskini Status'),
            'CREATED_AT' => Yii::t('app', 'Tarikh Dijana'),
            'UPDATED_AT' => Yii::t('app', 'Tarikh Dikemaskini'),
            'PASSWORD_RESET_TOKEN' => Yii::t('app', 'Token Kata Laluan'),
            'AUTH_KEY' => Yii::t('app', 'Kunci Identiti'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return static::findOne(['ID' => $id, 'DELETED' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($USERNAME) {
        return static::findOne(['USERNAME' => $USERNAME, 'DELETED' => self::STATUS_ACTIVE]);
    }

    public static function findByStaffNo($STAFF_NO) {
        return static::findOne(['STAFF_NO' => $STAFF_NO, 'DELETED' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
                    'PASSWORD_RESET_TOKEN' => $token,
                    'DELETED' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    public function getUsername() {
        return isset($this->USERNAME) ? $this->USERNAME : NULL;
    }

//    public function getStaffDetails() {
//        return isset($this->USERNAME) ? $this->USERNAME : NULL;
//    }

    public function getAvatar() {
        return isset($this->PROFILE_PIC) ? $this->PROFILE_PIC : NULL;
    }

    public function getName() {
//        return isset($this->mpspProfile->NAMA) ? $this->mpspProfile->NAMA : NULL;
        return isset($this->USERNAME) ? $this->USERNAME : NULL;
    }

    public function getRole() {
        return isset($this->roles->NAME) ? $this->roles->NAME : NULL;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->AUTH_KEY;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($PASSWORD) {
        return Yii::$app->security->validatePassword($PASSWORD, $this->PASSWORD);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($PASSWORD) {
        $this->PASSWORD = Yii::$app->security->generatePasswordHash($PASSWORD);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->AUTH_KEY = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->PASSWORD_RESET_TOKEN = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->PASSWORD_RESET_TOKEN = null;
    }

    public function getRoles() {
        return $this->hasOne(Roles::className(), ['ID' => 'ROLE_ID']);
    }

    public function getMpspProfile() {
        return $this->hasOne(MpspStaff::className(), ['NO_PEKERJA' => 'STAFF_NO']);
    }

//    public function beforeSave($insert) {
//        if ($this->beforeSave($insert)) {
//            if ($this->isNewRecord || (!$this->isNewRecord && $this->password)) {
////                $this->setPassword($this->password);
////                $this->generateAuthKey();
////                $this->generatePasswordResetToken();
////                var_dump($insert);
////                die();
//            }
//            return true;
//        }
////        var_dump($insert);
////                die();
//        return false;
//    }
}
