<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "REPORT_IN_QUARTER".
 *
 * @property string $YEAR
 * @property string $ID
 * @property string $QUARTER
 * @property string $COUNT
 * @property string $TOTAL_PRICE
 */
class ReportInQuarter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'CREATED_AT',
                'updatedAtAttribute' => 'UPDATED_AT',
                'value' => date('d-M-y h.i.s a'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CREATED_BY',
                'updatedByAttribute' => 'UPDATED_BY',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'REPORT_IN_QUARTER';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['YEAR', 'ID', 'COUNT', 'TOTAL_PRICE'], 'number'],
            [['QUARTER'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'YEAR' => Yii::t('app', 'Tahun'),
            'ID' => Yii::t('app', 'ID'),
            'QUARTER' => Yii::t('app', 'Suku'),
            'COUNT' => Yii::t('app', 'Bilangan'),
            'TOTAL_PRICE' => Yii::t('app', 'Jumlah'),
        ];
    }

    /**
     * @inheritdoc
     * @return ReportInQuarterQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ReportInQuarterQuery(get_called_class());
    }
    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
    }
    public function getUpdator() {
        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
    }
}
