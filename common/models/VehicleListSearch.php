<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VehicleList;

/**
 * VehicleListSearch represents the model behind the search form about `common\models\VehicleList`.
 */
class VehicleListSearch extends VehicleList {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['ID'], 'number'],
            [['REG_NO', 'MODEL', 'TYPE', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
            [['CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = VehicleList::find();
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['VEHICLE_LIST.DELETED' => 0]);
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'CREATED_AT' => $this->CREATED_AT,
            'UPDATED_AT' => $this->UPDATED_AT,
            'CREATED_BY' => $this->CREATED_BY,
            'UPDATED_BY' => $this->UPDATED_BY,
            'DELETED' => $this->DELETED,
            'DELETED_AT' => $this->DELETED_AT,
        ]);

        $query->andFilterWhere(['like', 'REG_NO', $this->REG_NO])
                ->andFilterWhere(['like', 'MODEL', $this->MODEL])
                ->andFilterWhere(['like', 'TYPE', $this->TYPE]);

        return $dataProvider;
    }

}
