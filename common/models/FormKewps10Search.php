<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FormKewps10;

/**
 * FormKewps10Search represents the model behind the search form about `common\models\FormKewps10`.
 */
class FormKewps10Search extends FormKewps10
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ORDER_ID', 'ORDER_ITEM_ID', 'TRANSACTION_ID', 'RQ_QUANTITY', 'APP_QUANTITY', 'CURRENT_BALANCE', 'UNIT_PRICE', 'AVERAGE_UNIT_PRICE', 'BATCH_TOTAL_PRICE'], 'number'],
            [['ORDER_REQUIRED_DATE', 'ORDER_NO', 'ORDERED_BY', 'ORDER_DATE', 'CARD_NO', 'CODE_NO', 'DESCRIPTION', 'CREATED_DATE', 'APPROVED_DATE'], 'safe'],
            [['CREATED_BY', 'APPROVED_BY'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormKewps10::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ORDER_ID' => $this->ORDER_ID,
            'ORDER_ITEM_ID' => $this->ORDER_ITEM_ID,
            'TRANSACTION_ID' => $this->TRANSACTION_ID,
            'RQ_QUANTITY' => $this->RQ_QUANTITY,
            'APP_QUANTITY' => $this->APP_QUANTITY,
            'CURRENT_BALANCE' => $this->CURRENT_BALANCE,
            'UNIT_PRICE' => $this->UNIT_PRICE,
            'AVERAGE_UNIT_PRICE' => $this->AVERAGE_UNIT_PRICE,
            'BATCH_TOTAL_PRICE' => $this->BATCH_TOTAL_PRICE,
            'CREATED_BY' => $this->CREATED_BY,
            'APPROVED_BY' => $this->APPROVED_BY,
        ]);

        $query->andFilterWhere(['like', 'ORDER_REQUIRED_DATE', $this->ORDER_REQUIRED_DATE])
            ->andFilterWhere(['like', 'ORDER_NO', $this->ORDER_NO])
            ->andFilterWhere(['like', 'ORDERED_BY', $this->ORDERED_BY])
            ->andFilterWhere(['like', 'ORDER_DATE', $this->ORDER_DATE])
            ->andFilterWhere(['like', 'CARD_NO', $this->CARD_NO])
            ->andFilterWhere(['like', 'CODE_NO', $this->CODE_NO])
            ->andFilterWhere(['like', 'DESCRIPTION', $this->DESCRIPTION])
            ->andFilterWhere(['like', 'CREATED_DATE', $this->CREATED_DATE])
            ->andFilterWhere(['like', 'APPROVED_DATE', $this->APPROVED_DATE]);

        return $dataProvider;
    }
}
