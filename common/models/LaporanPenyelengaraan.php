<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "LAPORAN_PENYELENGGARAAN_VIEW".
 *
 * @property string $ID
 * @property string $TAHUN
 * @property string $BULAN
 * @property string $NO_KERJA
 * @property string $ARAHAN_KERJA
 * @property string $JK_JG
 * @property integer $BENGKEL_PANEL_ID
 * @property string $NAMA_PANEL
 * @property string $KATEGORI_PANEL
 * @property integer $SELENGGARA_ID
 * @property string $TEMPOH_SELENGGARA
 * @property integer $ALASAN_ID
 * @property string $ALASAN
 * @property string $TARIKH_ARAHAN
 * @property string $NO_SEBUTHARGA
 * @property string $TARIKH_KENDERAAN_TIBA
 * @property string $TARIKH_JANGKA_SIAP
 * @property string $TARIKH_SEBUTHARGA
 * @property string $STATUS_SEBUTHARGA
 * @property string $NO_DO
 * @property string $TARIKH_SIAP
 * @property string $TEMPOH_JAMINAN
 * @property string $TARIKH_DO
 * @property string $TARIKH_CETAK
 * @property string $STATUS
 * @property string $CATATAN_SEBUTHARGA
 * @property string $CATATAN
 * @property string $NAMA_PIC
 * @property string $TARIKH_PERMOHONAN
 * @property integer $ID_KENDERAAN
 * @property string $KATEGORI_KEROSAKAN_ID
 * @property string $KATEGORI_KEROSAKAN
 * @property string $KATEGORI
 * @property string $ISSERVICE
 * @property string $ISPANCIT
 * @property string $ODOMETER_TERKINI
 * @property string $NO_PLAT
 * @property string $MODEL
 * @property string $KOD_JABATAN
 * @property string $NAMA_JABATAN
 * @property string $JUMLAH_KOS
 */
class LaporanPenyelengaraan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'CREATED_AT',
                'updatedAtAttribute' => 'UPDATED_AT',
                'value' => date('d-M-y h.i.s a'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CREATED_BY',
                'updatedByAttribute' => 'UPDATED_BY',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'LAPORAN_PENYELENGGARAAN_VIEW';
    }

    public static function primaryKey()
    {
        return ["ID"];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID'], 'number'],
            [['BENGKEL_PANEL_ID', 'SELENGGARA_ID', 'ALASAN_ID', 'ID_KENDERAAN'], 'integer'],
            [['TAHUN', 'BULAN', 'NO_KERJA', 'ARAHAN_KERJA', 'JK_JG', 'NAMA_PANEL', 'KATEGORI_PANEL', 'TEMPOH_SELENGGARA', 'ALASAN', 'TARIKH_ARAHAN', 'NO_SEBUTHARGA', 'TARIKH_KENDERAAN_TIBA', 'TARIKH_JANGKA_SIAP', 'TARIKH_SEBUTHARGA', 'STATUS_SEBUTHARGA', 'NO_DO', 'TARIKH_SIAP', 'TEMPOH_JAMINAN', 'TARIKH_DO', 'TARIKH_CETAK', 'STATUS', 'CATATAN_SEBUTHARGA', 'CATATAN', 'NAMA_PIC', 'TARIKH_PERMOHONAN', 'KATEGORI_KEROSAKAN_ID', 'KATEGORI_KEROSAKAN', 'KATEGORI', 'ISSERVICE', 'ISPANCIT', 'ODOMETER_TERKINI', 'NO_PLAT', 'MODEL', 'KOD_JABATAN', 'NAMA_JABATAN', 'JUMLAH_KOS'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'TAHUN' => Yii::t('app', 'Tahun'),
            'BULAN' => Yii::t('app', 'Bulan'),
            'NO_KERJA' => Yii::t('app', 'No  Kerja'),
            'ARAHAN_KERJA' => Yii::t('app', 'Arahan  Kerja'),
            'JK_JG' => Yii::t('app', 'Jk  Jg'),
            
            'BENGKEL_PANEL_ID' => Yii::t('app', 'Bengkel  Panel  ID'),
            'NAMA_PANEL' => Yii::t('app', 'Nama  Panel'),
            'KATEGORI_PANEL' => Yii::t('app', 'Kategori  Panel'),
            
            'SELENGGARA_ID' => Yii::t('app', 'Selenggara  ID'),
            'TEMPOH_SELENGGARA' => Yii::t('app', 'Tempoh  Selenggara'),
            
            'ALASAN_ID' => Yii::t('app', 'Alasan  ID'),
            'ALASAN' => Yii::t('app', 'Alasan'),
            
            'TARIKH_ARAHAN' => Yii::t('app', 'Tarikh  Arahan'),
            'NO_SEBUTHARGA' => Yii::t('app', 'No  Sebutharga'),
            'TARIKH_KENDERAAN_TIBA' => Yii::t('app', 'Tarikh  Kenderaan  Tiba'),
            'TARIKH_JANGKA_SIAP' => Yii::t('app', 'Tarikh  Jangka  Siap'),
            'TARIKH_SEBUTHARGA' => Yii::t('app', 'Tarikh  Sebutharga'),
            'STATUS_SEBUTHARGA' => Yii::t('app', 'Status  Sebutharga'),
            
            'NO_DO' => Yii::t('app', 'No  Do'),
            'TARIKH_SIAP' => Yii::t('app', 'Tarikh  Siap'),
            'TEMPOH_JAMINAN' => Yii::t('app', 'Tempoh  Jaminan'),
            'TARIKH_DO' => Yii::t('app', 'Tarikh  Do'),
            'TARIKH_CETAK' => Yii::t('app', 'Tarikh  Cetak'),
            'STATUS' => Yii::t('app', 'Status'),
            'CATATAN_SEBUTHARGA' => Yii::t('app', 'Catatan  Sebutharga'),
            'CATATAN' => Yii::t('app', 'Catatan'),
            'NAMA_PIC' => Yii::t('app', 'Nama  Pic'),
            'TARIKH_PERMOHONAN' => Yii::t('app', 'Tarikh  Permohonan'),
            
            'ID_KENDERAAN' => Yii::t('app', 'Id  Kenderaan'),
            
            'KATEGORI_KEROSAKAN_ID' => Yii::t('app', 'Kategori  Kerosakan  ID'),
            'KATEGORI_KEROSAKAN' => Yii::t('app', 'Kategori  Kerosakan'),
            'KATEGORI' => Yii::t('app', 'Kategori'),
            'ISSERVICE' => Yii::t('app', 'Isservice'),
            'ISPANCIT' => Yii::t('app', 'Ispancit'),
            'ODOMETER_TERKINI' => Yii::t('app', 'Odometer  Terkini'),
            'NO_PLAT' => Yii::t('app', 'No  Plat'),
            'MODEL' => Yii::t('app', 'Model'),
            
            'KOD_JABATAN' => Yii::t('app', 'Kod  Jabatan'),
            'NAMA_JABATAN' => Yii::t('app', 'Nama  Jabatan'),
            
            'JUMLAH_KOS' => Yii::t('app', 'Jumlah  Kos'),
        ];
    }
    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
    }
    public function getUpdator() {
        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
    }
}
