<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "INVENTORIES_CHECKIN".
 *
 * @property string $ID
 * @property integer $TRANSACTION_ID
 * @property integer $INVENTORY_ID
 * @property integer $VENDOR_ID
 * @property string $ITEMS_QUANTITY
 * @property string $ITEMS_TOTAL_PRICE
 * @property string $CHECK_DATE
 * @property integer $CHECK_BY
 * @property integer $APPROVED
 * @property integer $APPROVED_BY
 * @property string $APPROVED_AT
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class InventoriesCheckIn extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'CREATED_AT',
                'updatedAtAttribute' => 'UPDATED_AT',
                'value' => date('d-M-y h.i.s a'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CREATED_BY',
                'updatedByAttribute' => 'UPDATED_BY',
                'value' => Yii::$app->user->id,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'INVENTORIES_CHECKIN';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['INVENTORY_ID', 'ITEMS_QUANTITY'], 'required', 'on' => 'entry-create'],
            [['ITEMS_QUANTITY'], 'compare', 'compareValue' => 0, 'operator' => '>', 'type' => 'number', 'on' => 'entry-create'],
            [['ID', 'ITEMS_QUANTITY', 'ITEMS_TOTAL_PRICE'], 'number'],
            [['TRANSACTION_ID', 'INVENTORY_ID', 'VENDOR_ID', 'CHECK_BY', 'APPROVED', 'APPROVED_BY', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['APPROVED_AT', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
            [['CHECK_DATE'], 'string'],
            [['ID'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'ID' => Yii::t('app', 'ID'),
            'TRANSACTION_ID' => Yii::t('app', 'ID Transaksi'),
            'INVENTORY_ID' => Yii::t('app', 'ID Inventori'),
            'VENDOR_ID' => Yii::t('app', 'ID Vendor'),
            'ITEMS_QUANTITY' => Yii::t('app', 'Kuantiti Barang'),
//            'ITEMS_UNIT_PRICE' => Yii::t('app', 'Jumlah Harga Seunit (RM)'),
            'ITEMS_TOTAL_PRICE' => Yii::t('app', 'Jumlah Harga Barang (RM)'),
            'CHECK_DATE' => Yii::t('app', 'Tarikh Cek'),
            'CHECK_BY' => Yii::t('app', 'Dicek Oleh'),
            'APPROVED' => Yii::t('app', 'Status Pengesahan'),
            'APPROVED_BY' => Yii::t('app', 'Disahkan Oleh'),
            'APPROVED_AT' => Yii::t('app', 'Disahkan Pada'),
            'CREATED_AT' => Yii::t('app', 'Tarikh Dijana'),
            'UPDATED_AT' => Yii::t('app', 'Tarikh Dikemaskini'),
            'CREATED_BY' => Yii::t('app', 'Dijana Oleh'),
            'UPDATED_BY' => Yii::t('app', 'Dikemaskini Oleh'),
            'DELETED' => Yii::t('app', 'Status'),
            'DELETED_AT' => Yii::t('app', 'Tarikh Kemaskini Status'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getTransaction() {
        return $this->hasOne(Transactions::className(), ['ID' => 'TRANSACTION_ID']);
    }

    public function getCategory() {
        return $this->hasOne(Categories::className(), ['ID' => 'CATEGORY_ID'])
                        ->via('inventory');
    }

    public function getInventory() {
        return $this->hasOne(Inventories::className(), ['ID' => 'INVENTORY_ID']);
    }

    public function getItems() {
        return $this->hasMany(InventoryItems::className(), ['CHECKIN_TRANSACTION_ID' => 'ID']);
    }

    public function getItemsDetails() {
        return $this->hasOne(InventoryItems::className(), ['CHECKIN_TRANSACTION_ID' => 'ID']);
    }

    public function getVendor() {
        return $this->hasOne(Vendors::className(), ['ID' => 'VENDOR_ID']);
    }

    public function getCreator() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
    }

    public function getUpdator() {
        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
    }

    public static function getQuantityCheckIn($id) {
        $quantity = InventoryItems::find()
                ->where(['CHECKIN_TRANSACTION_ID' => $id])
                ->andWhere(['INVENTORY_ITEMS.DELETED' => 0])
                ->asArray()
                ->count();
        return $quantity;
    }

    public static function updateQuantityCheckIn($id) {
        $model = static::findOne($id);
        $model->ITEMS_QUANTITY = static::getQuantityCheckIn($id);
        if ($model->save()) {
            return true;
        } else {
            return false;
        }
    }
    public static function getTotalPriceCheckIn($id) {
        $total_price = InventoryItems::find()
                ->where(['CHECKIN_TRANSACTION_ID' => $id])
                ->andWhere(['INVENTORY_ITEMS.DELETED' => 0])
                ->asArray()
                ->sum('UNIT_PRICE');
        return $total_price;
    }

    public static function updateTotalPriceCheckIn($id) {
        $model = static::findOne($id);
        $model->ITEMS_TOTAL_PRICE = static::getTotalPriceCheckIn($id);
        if ($model->save()) {
            return true;
        } else {
            return false;
        }
    }
    public static function updateQuantityAndTotalPriceCheckIn($id) {
        $model = static::findOne($id);
        $model->ITEMS_QUANTITY = static::getQuantityCheckIn($id);
        $model->ITEMS_TOTAL_PRICE = static::getTotalPriceCheckIn($id);
        if ($model->save()) {
            return true;
        } else {
            return false;
        }
    }

}
