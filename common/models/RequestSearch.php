<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrderItems;

/**
 * OrderItemsSearch represents the model behind the search form about `common\models\OrderItems`.
 */
class RequestSearch extends OrderItems {

    /**
     * @inheritdoc
     */
    public $INVENTORY_DETAILS;

    public function attributes() {
        // add related fields to searchable attributes
//        return array_merge(parent::attributes(), ['category.NAME', 'inventory.QUANTITY', 'inventory.DESCRIPTION', 'inventory.CARD_NO', 'inventory.CODE_NO',
//            'checkInTransaction.CHECK_DATE', 'checkOutTransaction.CHECK_DATE', 'INVENTORY_DETAILS']);
        return array_merge(parent::attributes(), [
            'category.NAME',
            'order.ORDER_NO', 'order.ORDER_DATE', 'order.REQUIRED_DATE', 'order.APPROVED',
            'arahanKerja.NO_KERJA',
            'inventory.QUANTITY', 'inventory.DESCRIPTION', 'inventory.CARD_NO', 'inventory.CODE_NO',
            'INVENTORY_DETAILS', 'USAGE_DETAIL'
        ]);
    }

    public function rules() {
        return [
            [['ID', 'RQ_QUANTITY', 'APP_QUANTITY', 'CURRENT_BALANCE', 'UNIT_PRICE'], 'number'],
            [['INVENTORY_ID', 'ORDER_ID', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['CREATED_AT', 'UPDATED_AT', 'DELETED_AT', 'category.NAME', 'category.NAME', 'order.ORDER_NO', 'order.ORDER_DATE', 'order.REQUIRED_DATE', 'order.APPROVED', 'arahanKerja.NO_KERJA',
            'inventory.QUANTITY', 'inventory.DESCRIPTION', 'inventory.CARD_NO', 'inventory.CODE_NO',
//                'INVENTORY_DETAILS', 'USAGE_DETAIL'
                ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = OrderItems::find();
        $query->joinWith('order');
        $query->joinWith('inventory');
        $query->joinWith('transaction');
        $query->joinWith('arahanKerja');
        $query->joinWith('vehicle');
//        $query->joinWith('items');
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['ORDER_ITEMS.DELETED' => 0]);
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // var_dump($dataProvider->models);
        // die;

        $dataProvider->sort->defaultOrder = ['ID' => SORT_DESC];
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
//        order Items
        $dataProvider->sort->attributes['QUANTITY'] = ['asc' => ['ORDER_ITEMS.APP_QUANTITY' => SORT_ASC], 'desc' => ['ORDER_ITEMS.APP_QUANTITY' => SORT_DESC],];
        $dataProvider->sort->attributes['CREATED_AT'] = ['asc' => ['ORDER_ITEMS.CREATED_AT' => SORT_ASC], 'desc' => ['ORDER_ITEMS.CREATED_AT' => SORT_DESC],];
        $dataProvider->sort->attributes['UPDATED_AT'] = ['asc' => ['ORDER_ITEMS.UPDATED_AT' => SORT_ASC], 'desc' => ['ORDER_ITEMS.UPDATED_AT' => SORT_DESC],];
        $dataProvider->sort->attributes['ID'] = ['asc' => ['ORDER_ITEMS.ID' => SORT_ASC], 'desc' => ['ORDER_ITEMS.ID' => SORT_DESC],];
//        order
        $dataProvider->sort->attributes['order.ORDER_NO'] = ['asc' => ['ORDERS.ORDER_NO' => SORT_ASC], 'desc' => ['ORDERS.ORDER_NO' => SORT_DESC],];
        $dataProvider->sort->attributes['order.ORDER_DATE'] = ['asc' => ['ORDERS.ORDER_DATE' => SORT_ASC], 'desc' => ['ORDERS.ORDER_DATE' => SORT_DESC],];
        $dataProvider->sort->attributes['order.REQUIRED_DATE'] = ['asc' => ['ORDERS.REQUIRED_DATE' => SORT_ASC], 'desc' => ['ORDERS.REQUIRED_DATE' => SORT_DESC],];
        $dataProvider->sort->attributes['order.ORDERED_BY'] = ['asc' => ['ORDERS.ORDERED_BY' => SORT_ASC], 'desc' => ['ORDERS.ORDERED_BY' => SORT_DESC],];
        $dataProvider->sort->attributes['order.APPROVED'] = ['asc' => ['ORDERS.APPROVED' => SORT_ASC], 'desc' => ['ORDERS.APPROVED' => SORT_DESC],];
//        inventories
        $dataProvider->sort->attributes['INVENTORY_DETAILS'] = ['asc' => ['INVENTORIES.CODE_NO' => SORT_ASC], 'desc' => ['INVENTORIES.CODE_NO' => SORT_DESC],];
        $dataProvider->sort->attributes['inventory.QUANTITY'] = ['asc' => ['INVENTORIES.QUANTITY' => SORT_ASC], 'desc' => ['INVENTORIES.QUANTITY' => SORT_DESC],];
//        transactions
        $dataProvider->sort->attributes['transaction.CHECK_DATE'] = ['asc' => ['TRANSACTIONS.CHECK_DATE' => SORT_ASC], 'desc' => ['TRANSACTIONS.CHECK_DATE' => SORT_DESC],];
//        arahan kerja
        $dataProvider->sort->attributes['arahanKerja.NO_KERJA'] = ['asc' => ['LAPORAN_PENYELENGARAAN_SISKEN.NO_KERJA' => SORT_ASC], 'desc' => ['LAPORAN_PENYELENGARAAN_SISKEN.NO_KERJA' => SORT_DESC],];
//        vehicle
        $dataProvider->sort->attributes['USAGE_DETAIL'] = ['asc' => ['VEHICLE_LIST.REG_NO' => SORT_ASC], 'desc' => ['VEHICLE_LIST.REG_NO' => SORT_DESC],];

        // grid filtering conditions
        $query->andFilterWhere([
            'ORDER_ITEMS.ID' => $this->ID,
            'ORDERS.APPROVED' => $this->getAttribute('order.APPROVED'),
            'ORDER_ITEMS.INVENTORY_ID' => $this->INVENTORY_ID,
            'ORDER_ITEMS.ORDER_ID' => $this->ORDER_ID,
            'ORDER_ITEMS.RQ_QUANTITY' => $this->RQ_QUANTITY,
            'ORDER_ITEMS.APP_QUANTITY' => $this->APP_QUANTITY,
            'ORDER_ITEMS.CURRENT_BALANCE' => $this->CURRENT_BALANCE,
            'ORDER_ITEMS.UNIT_PRICE' => $this->UNIT_PRICE,
            'ORDER_ITEMS.CREATED_AT' => $this->CREATED_AT,
            'ORDER_ITEMS.UPDATED_AT' => $this->UPDATED_AT,
            'ORDER_ITEMS.CREATED_BY' => $this->CREATED_BY,
            'ORDER_ITEMS.UPDATED_BY' => $this->UPDATED_BY,
            'ORDER_ITEMS.DELETED' => $this->DELETED,
            'ORDER_ITEMS.DELETED_AT' => $this->DELETED_AT,
        ]);

        $query->andFilterWhere(['LIKE', 'LAPORAN_PENYELENGARAAN_SISKEN.NO_KERJA', $this->getAttribute('arahanKerja.NO_KERJA')]);
        $query->andFilterWhere(['LIKE', 'ORDERS.ORDER_NO', $this->getAttribute('order.ORDER_NO')]);
        $query->andFilterWhere(['LIKE', 'ORDERS.ORDER_DATE', $this->getAttribute('order.ORDER_DATE')]);
        $query->andFilterWhere(['LIKE', 'ORDERS.REQUIRED_DATE', $this->getAttribute('order.REQUIRED_DATE')]);
        $query->andFilterWhere(['LIKE', 'INVENTORIES.CARD_NO', $this->getAttribute('inventory.CARD_NO')]);
        $query->andFilterWhere(['LIKE', 'INVENTORIES.CODE_NO', $this->getAttribute('inventory.CODE_NO')]);
        $query->andFilterWhere(['LIKE', 'INVENTORIES.DESCRIPTION', $this->getAttribute('inventory.DESCRIPTION')]);
        $query->andFilterWhere(['LIKE', 'INVENTORIES.QUANTITY', $this->getAttribute('inventory.QUANTITY')]);
//        $query->orFilterWhere(['LIKE', 'INVENTORIES.CARD_NO', $this->INVENTORY_DETAILS]);
//        $query->orFilterWhere(['LIKE', 'INVENTORIES.CODE_NO', $this->INVENTORY_DETAILS]);
//        $query->orFilterWhere(['LIKE', 'INVENTORIES.DESCRIPTION', $this->INVENTORY_DETAILS]);
//        $query->orFilterWhere(['LIKE', 'INVENTORIES.CARD_NO', $this->getAttribute('INVENTORY_DETAILS')]);
//        $query->orFilterWhere(['LIKE', 'INVENTORIES.CODE_NO', $this->getAttribute('INVENTORY_DETAILS')]);
//        $query->orFilterWhere(['LIKE', 'INVENTORIES.DESCRIPTION', $this->getAttribute('INVENTORY_DETAILS')]);
//        if($this->getAttribute('INVENTORY_DETAILS')) {
//            $query->andFilterWhere(['LIKE', 'INVENTORIES.CARD_NO', $this->getAttribute('INVENTORY_DETAILS')]);
//        }
//        if($this->getAttribute('INVENTORY_DETAILS')) {
//            $query->andFilterWhere(['LIKE', 'INVENTORIES.CODE_NO', $this->getAttribute('INVENTORY_DETAILS')]);
//        }
//        if($this->getAttribute('INVENTORY_DETAILS')) {
//            $query->andFilterWhere(['LIKE', 'INVENTORIES.DESCRIPTION', $this->getAttribute('INVENTORY_DETAILS')]);
//        }
//        $query->andFilterWhere(['LIKE', 'INVENTORIES.CODE_NO', $this->getAttribute('order.CODE_NO')]);
//        $query->andFilterWhere(['LIKE', 'INVENTORIES.CARD_NO', $this->getAttribute('order.CARD_NO')]);
//        $query->andFilterWhere(['LIKE', 'INVENTORIES.DESCRIPTION', $this->getAttribute('order.DESCRIPTION')]);


        return $dataProvider;
    }

}
