<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "INVENTORIES".
 *
 * @property string $ID
 * @property integer $CATEGORY_ID
 * @property string $CARD_NO
 * @property string $CODE_NO
 * @property string $DESCRIPTION
 * @property string $QUANTITY
 * @property integer $MIN_STOCK
 * @property string $LOCATION
 * @property integer $APPROVED
 * @property string $APPROVED_AT
 * @property integer $APPROVED_BY
 * @property string $CREATED_AT
 * @property integer $CREATED_BY
 * @property string $UPDATED_AT
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class Inventories extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

//    public $DETAIL;
    public function attributes() {
        // add related fields to searchable attributes
//        return array_merge(parent::attributes(), ['category.NAME', 'inventory.QUANTITY', 'inventory.DESCRIPTION', 'inventory.CARD_NO', 'inventory.CODE_NO',
//            'checkInTransaction.CHECK_DATE', 'checkOutTransaction.CHECK_DATE', 'INVENTORY_DETAILS']);
//        $DETAIL = $this->getDetail();
//        $DETAIL = 0;
        return array_merge(parent::attributes(), ['DETAIL']);
    }

//    public function setDetail() {
//        $this->DETAIL = 1;
//    }

    public function behaviors() {
        return [
        [
        'class' => TimestampBehavior::className(),
        'createdAtAttribute' => 'CREATED_AT',
        'updatedAtAttribute' => 'UPDATED_AT',
        'value' => date('d-M-y h.i.s a'),
        ],
        [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'CREATED_BY',
        'updatedByAttribute' => 'UPDATED_BY',
        'value' => Yii::$app->user->id,
        ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'INVENTORIES';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
        [['ID', 'QUANTITY'], 'number'],
        [['CATEGORY_ID', 'MIN_STOCK', 'APPROVED', 'APPROVED_BY', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
        [['APPROVED_AT', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
        [['CARD_NO', 'CODE_NO', 'DESCRIPTION', 'LOCATION'], 'string', 'max' => 255],
        [['ID', 'CODE_NO'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
        'ID' => Yii::t('app', 'ID'),
        'CATEGORY_ID' => Yii::t('app', 'ID Kategori'),
        'CARD_NO' => Yii::t('app', 'No Kad'),
        'CODE_NO' => Yii::t('app', 'No Kod'),
        'DESCRIPTION' => Yii::t('app', 'Nama Inventori'),
        'QUANTITY' => Yii::t('app', 'Kuantiti'),
        'MIN_STOCK' => Yii::t('app', 'Min  Stok'),
        'LOCATION' => Yii::t('app', 'Lokasi'),
        'APPROVED' => Yii::t('app', 'Status Pengesahan'),
        'APPROVED_AT' => Yii::t('app', 'Disahkan Oleh'),
        'APPROVED_BY' => Yii::t('app', 'Disahkan Pada'),
        'CREATED_AT' => Yii::t('app', 'Tarikh Dijana'),
        'CREATED_BY' => Yii::t('app', 'Dijana Oleh'),
        'UPDATED_AT' => Yii::t('app', 'Tarikh Dikemaskini'),
        'UPDATED_BY' => Yii::t('app', 'Dikemaskini Oleh'),
        'DELETED' => Yii::t('app', 'Status'),
        'DELETED_AT' => Yii::t('app', 'Tarikh Kemaskini Status'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getCategory() {
        return $this->hasOne(Categories::className(), ['ID' => 'CATEGORY_ID']);
    }

    public function getDetail() {
        return $this->CODE_NO . ' - ' . $this->DESCRIPTION;
    }

    public function getAllCheckInTransactions() {
        return $this->hasMany(InventoriesCheckIn::className(), ['INVENTORY_ID' => 'ID']);
    }

    public function getAllCheckOutTransactions() {
        return $this->hasMany(OrderItems::className(), ['INVENTORY_ID' => 'ID']);
    }

    public function getCheckInTransactions() {
        return $this->hasOne(InventoriesCheckIn::className(), ['INVENTORY_ID' => 'ID']);
    }

    public function getCheckOutTransactions() {
        return $this->hasOne(OrderItems::className(), ['INVENTORY_ID' => 'ID']);
    }

    public function getTransactionIn() {
        return $this->hasMany(Transactions::className(), ['ID' => 'TRANSACTION_ID'])
        ->via('checkInTransactions');
    }

    public function getTransactionOut() {
        return $this->hasMany(Transactions::className(), ['ID' => 'TRANSACTION_ID'])
        ->via('checkOutTransactions');
    }

    public function getCreator() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
    }

    public function getUpdator() {
        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
    }

    public function getItems() {
        return $this->hasMany(InventoryItems::className(), ['ID' => 'CHECKIN_TRANSACTION_ID'])
        ->via('checkInTransactions');
    }

    public static function getActiveItemsByInventoryId($id) {
        return InventoryItems::find()->joinWith('inventory')->where(['INVENTORIES.ID' => $id, 'INVENTORY_ITEMS.DELETED' => 0])->all();
    }

    public static function getQuantityInStore($id) {
        $quantity = InventoryItems::find()
        ->joinWith('inventory')
        ->where(['INVENTORIES.ID' => $id])
        ->andWhere(['CHECKOUT_TRANSACTION_ID' => null])
        ->andWhere(['INVENTORY_ITEMS.DELETED' => 0])
        ->asArray()
        ->count();
        return $quantity;
    }

    public static function updateQuantity($id) {
        $model = static::findOne($id);
        $model->QUANTITY = static::getQuantityInStore($id);
        if ($model->save()) {
            return true;
        }
        return false;
    }

    public static function deletePermanent($id) {
        $model = static::findOne($id);
//        check if there is any model related to this inventory id : true -> cannot delete
        $flag = !empty($model->allCheckInTransactions) || !empty($model->allCheckOutTransactions) || !empty($model->items);
//        var_dump($flag);die();
//        delete if allowed
        if (!$flag) {
//            if ($model->DELETED = 0) {
//                static::updateQuantity($id);
//            }
            $model->delete();
            return true;
        }
        return false;
    }

//    https://github.com/yiisoft/yii2/issues/1282 :get all relation
//    public function getModelRelations() {
//        $reflector = new \ReflectionClass($this->modelClass);
//        $model = new $this->modelClass;
//        $stack = array();
//        foreach ($reflector->getMethods() AS $method) {
//            if (substr($method->name, 0, 3) !== 'get')
//                continue;
//            if ($method->name === 'getRelation')
//                continue;
//            if ($method->name === 'getBehavior')
//                continue;
//            if ($method->name === 'getFirstError')
//                continue;
//            if ($method->name === 'getAttribute')
//                continue;
//            if ($method->name === 'getAttributeLabel')
//                continue;
//            if ($method->name === 'getOldAttribute')
//                continue;
//
//            $relation = call_user_func(array($model, $method->name));
//            if ($relation instanceof yii\db\ActiveRelation) {
//                $stack[] = $relation;
//            }
//        }
//        return $stack;
//    }
//    public function getAll() {
//
//        $result = \yii\helpers\ArrayHelper::map($get, 'id', 'name');
//        return $result;
//    }

    public function findList($id) {
        return $this->CODE_NO . ' - ' . $this->DESCRIPTION;
    }

    public static function printInventoryBarcode($id){
        $data = static::findOne($id);
        // $mpdf = new \mPDF('utf-8', array(50, 20), 0, '', 1, 1, 5, 1, 2, 0, 'P');
        $mpdf = new \mPDF('utf-8', array(50, 20), 0, '', 1, 1, 5, 0, 1.5, 0, 'P');
        $mpdf->defaultheaderfontsize = 5;
        $mpdf->defaultfooterfontsize = 5;
        $mpdf->SetHeader('|© Majlis Perbandaran Seberang Perai|');
       // $mpdf->SetFooter('|© MPSP|');
        $mpdf->WriteHTML(''
            . 'barcode {padding: 1.5mm;margin: 0;vertical-align: top;color: #000044;}'
            . '.barcodecell {text-align: center;vertical-align: middle;color:white;text-align:center;padding:2px;}'
            . '.barcodetext {text-align:center;font-size:6px;}'
            , 1);
        $mpdf->WriteHTML(''
            . '<div style="text-align:center;font-size:8px">' . $data->DESCRIPTION . '</div>'
            . '<div class="barcodecell">'
            . '<barcode class="barcode" code="' . $data->CODE_NO . '" type="C128A" size="0.5" height="1.5"/></div>'
            . '<div class="barcodetext">CODE : ' . $data->CODE_NO . '</div>'
            , 2);
        $mpdf->Output();

    }
    public static function printAllSku($id,$printInventoryCode){
        $model = InventoryItems::find()->where(['in','INVENTORIES.ID',$id])
        ->andFilterWhere(['INVENTORY_ITEMS.DELETED' => 0])
        ->andFilterWhere(['CHECKOUT_TRANSACTION_ID' => null])
        ->joinWith('inventory')->all();
        // var_dump($model);die();
        // $mpdf = new \mPDF('utf-8', array(50, 20), 0, '', 1, 1, 5, 1, 2, 0, 'P');
        $mpdf = new \mPDF('utf-8', array(50, 20), 0, '', 1, 1, 5, 0, 1.5, 0, 'P');
        $mpdf->defaultheaderfontsize = 5;
        $mpdf->defaultfooterfontsize = 5;
        $mpdf->SetHeader('|© Majlis Perbandaran Seberang Perai|');
       // $mpdf->SetFooter('|© MPSP|');
        $css = ''
        . 'barcode {padding: 1.5mm;margin: 0;vertical-align: top;color: #000044;}'
        . '.barcodecell {text-align: center;vertical-align: middle;color:white;text-align:center;padding:2px;}'
        . '.barcodetext {text-align:center;font-size:6px;}'
        .'';
        $mpdf->WriteHTML($css, 1);
        foreach ($model as $key => $data) {
            $content1[] = ''
            . '<div style="text-align:center;font-size:8px">' . $data->inventory->DESCRIPTION . '</div>'
            . '<div class="barcodecell"><barcode class="barcode" code="' . $data->inventory->CODE_NO . '" type="C128A" size="0.5" height="1.5"/></div>'
            . '<div class="barcodetext">CODE : ' . $data->inventory->CODE_NO . '</div>';
            $content2[] = ''
            . '<div style="text-align:center;font-size:8px">' . $data->inventory->DESCRIPTION . '</div>'
            . '<div class="barcodecell"><barcode class="barcode" code="' . $data->SKU . '" type="C128A" size="0.48" height="1.5"/></div>'
            . '<div class="barcodetext">SKU : ' . $data->SKU . '</div>';
        }
        foreach ($content2 as $i => $c) {
            if($printInventoryCode){
                $mpdf->WriteHTML($content1[$i], 2);
            }
            $mpdf->WriteHTML($c, 2);
            $mpdf->addPage();
        }

        $mpdf->Output();

    }

}
