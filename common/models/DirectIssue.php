<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class DirectIssue extends Model
{
    public $transaction_id;
    public $order_no;
    public $ordered_by;
    public $usage_id;
    public $order_date;
    public $required_date;
    public $approved;
    public $approved_by;
    public $approved_at;
    public $deleted;
    public $deleted_by;
    public $deleted_at;
    public $created_by;
    public $created_at;
    public $updated_by;
    public $updated_at;
    public $checkout_date;
    public $work_order_id;
    public $inventory_id = [];
    public $rq_quantity = [];



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['order_no', 'ordered_by'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],

            [['transaction_id'], 'required'],
            [['transaction_id', 'work_order_id', 'approved', 'approved_by', 'usage_id', 'checkout_by', 'created_by', 'updated_by', 'deleted'], 'integer'],
            [['approved_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['order_date', 'required_date', 'checkout_date'], 'string', 'max' => 7],
            [['ordered_by', 'order_no'], 'string', 'max' => 20],
            // [['ID', 'ID', 'DELETED', 'DELETED'], 'unique', 'targetAttribute' => ['ID', 'ID', 'DELETED', 'DELETED'], 'message' => 'The combination of ID and Deleted has already been taken.'],
            // [['ID', 'ID', 'ID'], 'unique', 'targetAttribute' => ['ID', 'ID', 'ID'], 'message' => 'The combination of  and ID has already been taken.']
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
