<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrderItems;

/**
 * OrderItemsSearch represents the model behind the search form about `common\models\OrderItems`.
 */
class OrderItemsSearch extends OrderItems {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['ID', 'RQ_QUANTITY', 'APP_QUANTITY', 'CURRENT_BALANCE', 'UNIT_PRICE'], 'number'],
            [['INVENTORY_ID', 'ORDER_ID', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = OrderItems::find();
        $query->joinWith('order');
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['ORDER_ITEMS.DELETED' => 0]);
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'INVENTORY_ID' => $this->INVENTORY_ID,
            'ORDER_ID' => $this->ORDER_ID,
            'RQ_QUANTITY' => $this->RQ_QUANTITY,
            'APP_QUANTITY' => $this->APP_QUANTITY,
            'CURRENT_BALANCE' => $this->CURRENT_BALANCE,
            'UNIT_PRICE' => $this->UNIT_PRICE,
            'CREATED_AT' => $this->CREATED_AT,
            'UPDATED_AT' => $this->UPDATED_AT,
            'CREATED_BY' => $this->CREATED_BY,
            'UPDATED_BY' => $this->UPDATED_BY,
            'DELETED' => $this->DELETED,
            'DELETED_AT' => $this->DELETED_AT,
        ]);

        return $dataProvider;
    }

}
