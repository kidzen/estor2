<?php

namespace common\models\maintenance;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "ALL_MAINTENANCE_CHECK".
 *
 * @property string $ID
 * @property string $TYPE
 * @property string $TABLE_NAME
 * @property string $COL_NAME
 * @property string $DATA_ID
 * @property string $FAULT_VALUE
 * @property string $TRUE_VALUE
 */
class AllMaintenanceCheck extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'CREATED_AT',
                'updatedAtAttribute' => 'UPDATED_AT',
                'value' => date('d-M-y h.i.s a'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CREATED_BY',
                'updatedByAttribute' => 'UPDATED_BY',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ALL_MAINTENANCE_CHECK';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'DATA_ID'], 'number'],
            [['TYPE'], 'string', 'max' => 11],
            [['TABLE_NAME'], 'string', 'max' => 19],
            [['COL_NAME'], 'string', 'max' => 17],
            [['FAULT_VALUE', 'TRUE_VALUE'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'TYPE' => Yii::t('app', 'Type'),
            'TABLE_NAME' => Yii::t('app', 'Table  Name'),
            'COL_NAME' => Yii::t('app', 'Col  Name'),
            'DATA_ID' => Yii::t('app', 'Data  ID'),
            'FAULT_VALUE' => Yii::t('app', 'Fault  Value'),
            'TRUE_VALUE' => Yii::t('app', 'True  Value'),
        ];
    }
    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
    }
    public function getUpdator() {
        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
    }
}
