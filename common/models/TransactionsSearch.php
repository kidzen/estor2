<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Transactions;

/**
 * TransactionsSearch represents the model behind the search form about `common\models\Transactions`.
 */
class TransactionsSearch extends Transactions {

    /**
     * @inheritdoc
     */
    public $DETAILS;

    public function rules() {
        return [
            [['ID', 'TYPE'], 'number'],
            [['CHECK_DATE', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
            [['CHECK_BY', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Transactions::find();
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['TRANSACTIONS.DELETED' => 0]);
        }

//        $query->where(['ID'=>'']);
//        $query->joinWith('order');
//        $query->joinWith('inventoriesCheckIn');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
//        var_dump($dataProvider->query);die();
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'TRANSACTIONS.ID' => $this->ID,
            'TRANSACTIONS.TYPE' => $this->TYPE,
            'TRANSACTIONS.CHECK_BY' => $this->CHECK_BY,
            'TRANSACTIONS.CREATED_AT' => $this->CREATED_AT,
            'TRANSACTIONS.UPDATED_AT' => $this->UPDATED_AT,
            'TRANSACTIONS.CREATED_BY' => $this->CREATED_BY,
            'TRANSACTIONS.UPDATED_BY' => $this->UPDATED_BY,
            'TRANSACTIONS.DELETED' => $this->DELETED,
            'TRANSACTIONS.DELETED_AT' => $this->DELETED_AT,
        ]);

        $query->andFilterWhere(['like', 'TRANSACTIONS.CHECK_DATE', $this->CHECK_DATE]);

        return $dataProvider;
    }

}
