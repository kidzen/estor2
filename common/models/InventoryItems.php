<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use \Exception;
use common\models\People;

/**
 * This is the model class for table "INVENTORY_ITEMS".
 *
 * @property string $ID
 * @property integer $INVENTORY_ID
 * @property integer $CHECKIN_TRANSACTION_ID
 * @property integer $CHECKOUT_TRANSACTION_ID
 * @property string $SKU
 * @property string $UNIT_PRICE
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class InventoryItems extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
        [
        'class' => TimestampBehavior::className(),
        'createdAtAttribute' => 'CREATED_AT',
        'updatedAtAttribute' => 'UPDATED_AT',
        'value' => date('d-M-y h.i.s a'),
        ],
        [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'CREATED_BY',
        'updatedByAttribute' => 'UPDATED_BY',
        'value' => Yii::$app->user->id,
        ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'INVENTORY_ITEMS';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
        [['UNIT_PRICE'], 'required', 'on' => 'entry-create'],
        [['ID', 'UNIT_PRICE'], 'number'],
        [['INVENTORY_ID', 'CHECKIN_TRANSACTION_ID', 'CHECKOUT_TRANSACTION_ID', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
        [['CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
        [['SKU'], 'string', 'max' => 50],
        [['ID', 'SKU'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
        'ID' => Yii::t('app', 'ID'),
        'INVENTORY_ID' => Yii::t('app', 'Inventory  ID'),
        'CHECKIN_TRANSACTION_ID' => Yii::t('app', 'ID Transaksi Masuk'),
        'CHECKOUT_TRANSACTION_ID' => Yii::t('app', 'ID Transaksi Keluar'),
        'SKU' => Yii::t('app', 'Sku'),
        'UNIT_PRICE' => Yii::t('app', 'Harga Seunit'),
        'CREATED_AT' => Yii::t('app', 'Tarikh Dijana'),
        'UPDATED_AT' => Yii::t('app', 'Tarikh Dikemaskini'),
        'CREATED_BY' => Yii::t('app', 'Dijana Oleh'),
        'UPDATED_BY' => Yii::t('app', 'Dikemaskini Oleh'),
        'DELETED' => Yii::t('app', 'Status'),
        'DELETED_AT' => Yii::t('app', 'Tarikh Kemaskini Status'),
        ];
    }

    /**
     * @inheritdoc
     */
//    public function getCheckInTransaction2() {
//        return $this->hasOne(Transactions::className(), ['ID' => 'CHECKIN_TRANSACTION_ID']);
//    }
    public function getCheckInTransaction() {
        return $this->hasOne(InventoriesCheckIn::className(), ['ID' => 'CHECKIN_TRANSACTION_ID']);
    }

    public function getCheckOutTransaction() {
        return $this->hasOne(OrderItems::className(), ['ID' => 'CHECKOUT_TRANSACTION_ID']);
    }

    public function getTransactionIn() {
        return $this->hasOne(Transactions::className(), ['ID' => 'TRANSACTION_ID'])
        ->via('checkInTransaction');
    }

    public function getTransactionOut() {
        return $this->hasOne(Transactions::className(), ['ID' => 'TRANSACTION_ID'])
        ->via('orders');
    }

    public function getInventory() {
        return $this->hasOne(Inventories::className(), ['ID' => 'INVENTORY_ID'])
        ->via('checkInTransaction');
    }

    public function getCategory() {
        return $this->hasOne(Categories::className(), ['ID' => 'CATEGORY_ID'])
        ->via('inventory');
    }

    public function getOrders() {
        return $this->hasOne(Orders::className(), ['ID' => 'ORDER_ID'])
        ->via('checkOutTransaction');
    }

    public function getRequestOrderItems() {
        return $this->hasOne(OrderItems::className(), ['INVENTORY_ID' => 'ID'])
        ->via('inventory');
    }

    public function getRequestInventory() {
        return $this->hasOne(Inventories::className(), ['INVENTORY_ID' => 'INVENTORY_ID'])
        ->via('orderItems');
    }

    public function getPeople($id) {
        return $this->hasOne(People::className(), ['ID' => $id]);
    }

    public function getCreator() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
    }

    public function getUpdator() {
        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
    }

    public function getSku() {
        return $this->SKU;
    }

    public static function findByInventoryId($id) {
        return static::find()->joinWith('inventory')->where(['INVENTORIES.ID' => $id])->all();
    }

    public static function findByOrderItemId($id) {
        return static::find()->joinWith('checkOutTransaction')->where(['CHECKOUT_TRANSACTION_ID' => $id])->all();
    }

    public static function findByIdStatusInStore($id) {
        return static::find($id)->where(['CHECKOUT_TRANSACTION_ID' => null])->one();
    }

    public static function checkoutFifo($ordersId) {
        $order = Orders::findOne($ordersId);
        $dbtransac = \Yii::$app->db->beginTransaction();
        try {
            // populate all items requested
            $orderItems = $order->items;
            foreach($orderItems as $orderItem){
                $inventoryItems = InventoryItems::find()
                ->joinWith('inventory')
                ->andWhere(['INVENTORIES.ID'=>$orderItem->inventory->ID])
                ->andWhere(['CHECKOUT_TRANSACTION_ID'=>null])
                ->andWhere(['INVENTORY_ITEMS.DELETED'=>0])
                ->orderBy(['INVENTORIES.ID'=>SORT_ASC])
                ->limit($orderItem->RQ_QUANTITY)
                ->all();
                foreach ($inventoryItems as $i => $inventoryItem) {
                    if ($orderItem->APP_QUANTITY < $orderItem->RQ_QUANTITY) {
                        $inventoryItem->CHECKOUT_TRANSACTION_ID = $orderItem->ID;
                        $inventoryItem->save();
                    } else {
                        throw new Exception(' Transaksi gagal. Transaksi secara manual diperlukan.');
                    }
                }
                var_dump(sizeof($inventoryItems));
                $orderItem->APP_QUANTITY = sizeof($inventoryItems);
                Inventories::updateQuantity($orderItem->inventory->ID);
                $orderItem->save();
            }
            $order->APPROVED = 1;
            $order->APPROVED_BY = Yii::$app->user->id;
            $order->APPROVED_AT = date('d-M-y h.i.s a');
            $order->save();
            $dbtransac->commit();
            \Yii::$app->notify->success('Proses berjaya');
        } catch (Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        }
    }

//    public static function printDetails($id) {
////        $sku = static::find()
////                ->select('INVENTORY_ITEMS.ID,INVENTORY_ITEMS.SKU,INVENTORIES.CODE_NO')
////                ->joinWith('inventory')
////                ->where(['INVENTORY_ITEMS.ID' => $id])
////                ->asArray()
////                ->all();
////        var_dump($sku);
//        $data = static::findOne($id);
////        var_dump($data->inventory->DESCRIPTION);die();
////        $pdf = new \kartik\mpdf\Pdf();
////        $mpdf = $pdf->getApi('utf-8', array(190,236));
//        $mpdf = new \mPDF('utf-8', array(50, 20), 0, '', 1, 1, 4, 1, 0.5, 0, 'P');
////        $mpdf = new \mPDF('utf-8', 'A4', 0, '', 1, 1, 4, 1, 0.5, 0, 'P');
////        $mpdf = new \mPDF('utf-8', 'A4');
////        $mpdf = new \mPDF('utf-8', 'A4', 0, '', 15, 15, 16, 16, 9, 9, 'L');
//        $mpdf->defaultheaderfontsize = 5;
//        $mpdf->defaultfooterfontsize = 5;
//        $mpdf->SetHeader('|© Majlis Perbandaran Seberang Perai|');
////        $mpdf->SetFooter('|© Majlis Perbandaran Seberang Perai|');
////        $mpdf->showWatermarkImage = true;
//        $mpdf->WriteHTML(''
//                . 'barcode {padding: 1.5mm;margin: 0;vertical-align: top;color: #000044;}'
//                . '.barcodecell {text-align: center;vertical-align: middle;}'
//                , 1);
//        $mpdf->WriteHTML(''
//                . '<div style="text-align:center;font-size:8px">' . $data->inventory->DESCRIPTION . '</div>'
//                . '<div class="barcodecell">'
//                . '<barcode class="barcode" code="' . $data->inventory->CODE_NO . '" type="C128A" size="0.5" height="0.5"/></div>'
//                . '<div style="text-align:center;font-size:6px;">CODE : ' . $data->inventory->CODE_NO . '</div>'
//                . '<div class="barcodecell">'
//                . '<barcode class="barcode" code="' . $data->SKU . '" type="C128A" size="0.5" height="0.5"/></div>'
//                . '<div style="text-align:center;font-size:6px">SKU : ' . $data->SKU . '</div>'
//                , 2);
//        $mpdf->AddPage();
//        $mpdf->WriteHTML(''
//                . '<div style="text-align:center;font-size:8px">' . $data->inventory->DESCRIPTION . '</div>'
//                . '<div style="color:white;text-align:center;padding:2px;">'
//                . '<barcode code="' . $data->inventory->CODE_NO . '" type="C128A" size="0.5" height="0.5"/></div>'
//                . '<div style="text-align:center;font-size:6px;">CODE : ' . $data->inventory->CODE_NO . '</div>'
//                . '<div style="color:white;text-align:center;padding:2px;">'
//                . '<barcode code="' . $data->SKU . '" type="C128A" size="0.5" height="0.5"/></div>'
//                . '<div style="text-align:center;font-size:6px">SKU : ' . $data->SKU . '</div>'
//        );
//        $mpdf->Output();
////        return $sku;
//    }


    public static function printSkuById($id, $printInventoryCode = 0) {
        $model = static::find()->where(['in', 'ID' ,$id])->with('inventory')->asArray()->all();
        $mpdf = static::generatePdfBarcode($model, $printInventoryCode);
        $mpdf->Output();
    }
    public static function printSku($sku, $printInventoryCode = 0) {
        $model = static::find()->where(['in', 'SKU' ,$sku])->with('inventory')->asArray()->all();
        $mpdf = static::generatePdfBarcode($model, $printInventoryCode);
        $mpdf->Output();
    }

    public static function generatePdfBarcode($model, $printInventoryCode) {
        $mpdf = new \mPDF('utf-8', array(50, 20), 0, '', 1, 1, 5, 1, 2, 0, 'P');
        $mpdf->defaultheaderfontsize = 5;
        $mpdf->defaultfooterfontsize = 5;
        $mpdf->SetHeader('|© Majlis Perbandaran Seberang Perai|');
//        $mpdf->SetFooter('|© Majlis Perbandaran Seberang Perai|');
//        $mpdf->showWatermarkImage = true;
        $css = ''
        . 'barcode {padding: 1.5mm;margin: 0;vertical-align: top;color: #000044;}'
        . '.barcodecell {text-align: center;vertical-align: middle;color:white;text-align:center;padding:2px;}'
        . '.barcodetext {text-align:center;font-size:6px;}'
        .'';
        $mpdf->WriteHTML($css, 1);
        foreach ($model as $data) {
            $mpdf->WriteHTML(''
                . '<div style="text-align:center;font-size:8px">' . $data['inventory']['DESCRIPTION'] . '</div>'
                . '<div class="barcodecell"><barcode class="barcode" code="' . $data['SKU'] . '" type="C128A" size="0.48" height="1.5"/></div>'
                . '<div class="barcodetext">SKU : ' . $data['SKU'] . '</div>'
                , 2);
            if ($printInventoryCode) {
                $mpdf->AddPage();
                $mpdf->WriteHTML(''
                    . '<div style="text-align:center;font-size:8px">' . $data['inventory']['DESCRIPTION'] . '</div>'
                    . '<div class="barcodecell"><barcode class="barcode" code="' . $data['inventory']['CODE_NO'] . '" type="C128A" size="0.5" height="1.5"/></div>'
                    . '<div class="barcodetext">CODE : ' . $data['inventory']['CODE_NO'] . '</div>'
                    , 2);
            }
        }
        return $mpdf;
    }

}
