<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "MPSP_STAFF".
 *
 * @property string $NO_PEKERJA
 * @property string $NAMA
 * @property string $JAWATAN
 * @property string $KETERANGAN
 */
class MpspStaff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'CREATED_AT',
                'updatedAtAttribute' => 'UPDATED_AT',
                'value' => date('d-M-y h.i.s a'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CREATED_BY',
                'updatedByAttribute' => 'UPDATED_BY',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MPSP_STAFF';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NO_PEKERJA'], 'string', 'max' => 20],
            [['NAMA', 'JAWATAN'], 'string', 'max' => 100],
            [['KETERANGAN'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'NO_PEKERJA' => Yii::t('app', 'No  Pekerja'),
            'NAMA' => Yii::t('app', 'Nama'),
            'JAWATAN' => Yii::t('app', 'Jawatan'),
            'KETERANGAN' => Yii::t('app', 'Keterangan'),
        ];
    }
    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
    }
    public function getUpdator() {
        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
    }
}
