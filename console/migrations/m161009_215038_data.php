<?php

use yii\db\Migration;

class m161009_215038_data extends Migration
{
    public function up() {
        $this->insertDefaultData();
        foreach (array_filter(array_map('trim', explode(';', $this->getSchemaSql()))) as $query) {
            var_dump($query);
            $this->execute($query);
        }
    }

    private function getSchemaSql() {
        // db oracle
        if(Yii::$app->db->driverName == 'oci'){
            $fileName = 'data-v3.sql';
            $myfile = fopen($fileName, "r") or die("Unable to open file!");
            $commandR = fread($myfile, filesize($fileName));
            fclose($myfile);

            $sql = $commandR;
            return "$sql";
        } else if(Yii::$app->db->driverName == 'mysql'){
            $fileName = 'data-mysql-v3.sql';
            $myfile = fopen($fileName, "r") or die("Unable to open file!");
            $commandR = fread($myfile, filesize($fileName));
            fclose($myfile);

            $sql = $commandR;
            return "$sql";
        } else {
            return 'fail';die;
        }

    }

    public function down() {
        // db oracle
        $this->insertDefaultData();

    }
    protected function insertDefaultData() {

        try {
            if(Yii::$app->db->driverName == 'oci'){
                $sqlObject = Yii::$app->db->createCommand("select 'truncate table ' || table_name || ' ;' as sql from user_tables")->queryAll();
                foreach ($sqlObject as $query) {
                    var_dump($query['SQL']);
                    $command = Yii::$app->db->createCommand($query['SQL']);
                    $command->execute();
                }

            } else if(Yii::$app->db->driverName == 'mysql'){
                $sqlObject = Yii::$app->db->createCommand("select 'truncate table ' || table_name || ' ;' as sql from information_schema.tables
                    WHERE table_schema = '".$this->schemaName."' and table_name != 'migration'")->queryAll();
                foreach ($sqlObject as $query) {
                    var_dump($query['SQL']);
                    $command = Yii::$app->db->createCommand($query['SQL']);
                    $command->execute();
                }
            }
            die;
            $i = 1;
            $this->batchInsert('{{%roles}}', ['id','name'],
                [
                [$i++,'Super Admin',],
                [$i++,'Admin Undang-Undang',],
                [$i++,'Admin Jabatan',],
                // [$i++,'User Undang-Undang',],
                [$i++,'User Jabatan',],
                // [$i++,'Kontraktor',],
                [$i++,'Common User',],
                ]
                );
            $i = 1;
            $this->batchInsert('{{%people}}', ['id','username','email','staff_no','password','role_id','auth_key'],
                [
                [$i++,'admin1','admin2@mail.com','10000',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                ]
                );

        } catch (\yii\db\Exception $e) {
            echo "No data";
        }

    }
}
