<?php include '_head.php'; 


?>
<!--<div class="container-fluid">-->

<div id="pageTitle" value="admin"></div>
<!-- ##########  EDIT DI KAWASAN BAWAH INI SAHAJA!  ########-->



<div class="row">
 <div class="col-xs-12 main">
<div class="box">
<div class="box-body">    
          


<!--<h3 class="sub-header">Dashboard Administrator</h3>  
     -->

  
       
      
      
<div class="row placeholders" style="margin-top: 30px;margin-bottom: -30px;">
    
               <div class="col-xs-6 col-sm-3 placeholder">
               <a href="admin_staff"><span style="font-size: 7em;color: #363636" class="glyphicon glyphicon-user" aria-hidden="true"></span></a>
               <h4>Staf</h4>
               <span class="text-muted">Senarai Penuh</span>
               </div>
    
                <div class="col-xs-6 col-sm-3 placeholder">
               <a href="admin_cuti"><span style="font-size: 7em;color: #ff0000" class="glyphicon glyphicon-calendar" aria-hidden="true"></span></a>
               <h4>Cuti</h4>
               <span class="text-muted">Mengikut Jabatan</span>
               </div>
    
                <div class="col-xs-6 col-sm-3 placeholder">
               <a href="admin_jabjawatan"><span style="font-size: 7em;color: #363636" class="glyphicon glyphicon-copy" aria-hidden="true"></span></a>
               <h4>Jabatan / Jawatan</h4>
               <span class="text-muted">Senarai Penuh</span>
               </div>
    
                <div class="col-xs-6 col-sm-3 placeholder">
               <a href="admin_settings"><span style="font-size: 7em;color: #363636" class="glyphicon glyphicon-cog" aria-hidden="true"></span></a>
               <h4>Tetapan</h4>
               <span class="text-muted">Senarai Tetapan Aplikasi</span>
               </div>
    
               

            
</div>





      
<h3 class="sub-header"></h3> 
      
      <div class="panel panel-default" style="width: 100%">
                        <div class="panel-heading">
                             Senarai Cuti Dipohon (Semua Jabatan)
                        </div>
                        <div class="panel-body">
      <div class="table-responsive"  >
      <p style="min-height: 45px"></p>
      
      
    <table class="table table-bordered table-striped  table-hover table-condensed " id="tableMain" >
        <thead class="theadx" >
        <th style="max-width: 40px;text-align: center;">Tarikh Pohon</th>              
        <th style="max-width: 40px;text-align: center;">Staf</th>
                        <th style="text-align: center;">Jabatan</th>
                        <th style="text-align: center;">Jawatan</th>
                        <th style="text-align: center;">Jenis Cuti</th>
                        <th style="text-align: center;">Mula</th>
                        <th style="text-align: center;">Akhir</th>
                        <th style="text-align: center;">Sebab Cuti</th>
                        <th style="text-align: center;">Pengganti</th>
                        <th style="text-align: center;">Status</th>
                     <th style="text-align: center;">Tindakan</th>
                    </thead>
                    <tbody>
                        
                        
       <?php
    


        foreach ($this->semuaCuti as $row)
        {
           
          
             
           echo'<tr >
               <td >'.$row['tarikhpohon'].'</td>
               <td >'.$row['cuti_staffno'].' : '.$row['staff_namapenuh'].'</td>
                      <td >'.$row['dept_name'].'</td>
                       <td >'.$row['p_name'].'</td>
                        
                        <td >'.$row['cuti_jeniscuti'].'</td>
                        <td >'.$row['tarikhmula'].'</td>
                        <td >'.$row['tarikhtamat'].'</td>
                        <td >'.$row['cuti_sebabcuti'].'</td>';
             if($row['cuti_pengganti'])
           {
            echo '<td style="text-align: left">'.$row['cuti_pengganti'].' : '.$row['ganti_namapenuh'].'</td>';   
           }else
           {
             echo '<td style="text-align: left">-Tiada-</td>';  
           }
                   
           switch ($row['cuti_status']) {
           case 'Menunggu': echo '<td ><span class="label label-warning col-md-12">'.$row['cuti_status'].'</span></td>';
           break;
           
           case 'Lulus': echo '<td><span class="label label-success col-md-12">'.$row['cuti_status'].'</span></td>';
           break;
       
            case 'Ditolak': echo '<td><span class="label label-danger col-md-12">'.$row['cuti_status'].'</span></td>';
           break;
       
       
           case 'Batal': echo '<td><span class="label label-default col-md-12">'.$row['cuti_status'].'</span></td>';
           break;
       
           default: echo '<td><span class="label label-info col-md-12">'.$row['cuti_status'].'</span></td>';
           break;
           }    
                
           
           echo'<td style="max-width:120px"><center>
         
                        <select id="'.$row['cuti_id'].'" name="respondCuti" class="respondCuti" title="Respond" >
                        <option></option> 
                          
                         
                         <option>Batal</option>
                         <option>Ditolak</option>
                         <option>Lulus</option>
                         <option>Menunggu</option>
                         
                         </select>
                    </center></td></tr>';
                echo'</tr>';
           
//            echo'<td style="max-width:120px"><center><div class="btn-group">'
//           . '<button title="Cetak/Download."  type="button" class="btn btn-default btn-sm cetak" value="'.$row['cuti_id'].'" ><span class="glyphicon glyphicon-print"></span></button>'
//           . '<button title="Kemaskini Permohonan."  type="button" class="btn btn-default btn-sm kemas" value="'.$row['cuti_id'].'" ><span class="glyphicon glyphicon-info-sign"></span></button>'
//           . '<button title="Batal Permohonan."  type="button" class="btn btn-default btn-sm batal" value="'.$row['cuti_id'].'" ><span class="glyphicon glyphicon-minus-sign"></span></button>'
//           . '<button title="Delete."  type="button" class="btn btn-default btn-sm hapus" value="'.$row['cuti_id'].'" ><span class="glyphicon glyphicon-remove-sign"></span></button></div></center></td></tr>';
//                echo'</tr>';
        }        
      ?>                
                    </tbody>
                    
    </table>    
                            
                            
      </div>                      
                       
                  
                        

<!-- ##########  EDIT DI KAWASAN ATAS INI SAHAJA!  ########-->
</div> </div>
   </div><!-- /.box-body -->
   </div><!-- /.box --> 
</div><!-- tutup Col --> 
</div><!-- tutup Row --> 



  <script type="text/javascript">
    $(document).ready(function ()
  { 
     
    $('.respondCuti').change(function()
    {
    
      var val =  $(this).val(); 
      var cutid =  $(this).attr('id');
     
 
     
        $.ajax({
                       
                       url: 'call/respondCuti',
                       data: {id:cutid,respond:val},
                       cache: false,
                       type: 'POST',
                       success: function(data){
                           
//                           $('#respondMod').append('<div style=\"min-height: 50px\" class=\"alert alert-success\">Kemaskini berjaya!</div>');
//                                        setTimeout(function() {
//                                        $('#respondMod').fadeTo(300, 0).slideUp(300, function(){
//                                            $(this).remove(); 
//                                        });
//                                  
//                                    }, 1100);
                            
                        //setTimeout(function() 
                       // {
                       //alert(data);
                         $(location).attr('href','admin_cuti');               
                        //}, 1100);
                       
                  
                       },
                        error: function(jqXHR, textStatus, errorThrown) {
                            
                            $('#respondMod').append('<div style=\"min-height: 50px\" class=\"alert alert-danger\">Ralat.Cuba sekali lagi</div>');
                                        setTimeout(function() {
                                        $('#respondMod').fadeTo(300, 0).slideUp(300, function(){
                                            $(this).remove(); 
                                        });
                                    }, 2000);
                        console.log(textStatus+" "+errorThrown);
                      }
                   });
     
     
     
     
     
     
     
    });
  

        
        
        
        
            
  });          
</script>
 



<?php include '_foot.php'; ?>
