<?php include '_head.php'; 


?>
<!--<div class="container-fluid">-->

<div id="pageTitle" value="hr"></div>
<!-- ##########  EDIT DI KAWASAN BAWAH INI SAHAJA!  ########-->



<div class="row">
 <div class="col-xs-12 main">
<div class="box">
<div class="box-body">    
          


<h3 class="sub-header">Dashboard | Sumber Manusia (HR)</h3>  
     

  
       
      
      






      

      
      <div class="panel panel-default" style="width: 100%">
                        <div class="panel-heading">
                          <ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#cutiS">Senarai Cuti Dipohon (Semua Jabatan)</a></li>
  <li><a data-toggle="tab" href="#profilS">Profil Staf (Semua Jabatan)</a></li>

</ul>     
                        </div>
                        <div class="panel-body">
      
                            
                            
                            
                            
                             
                            
                            
                            
                            
                            
  <div class="tab-content">                          
                            
 <div id="cutiS" class="tab-pane fade in active">
    <h3></h3>                           
      
    <p style="min-height: 20px"></p>    
      
        
    <table class="table table-bordered table-responsive table-striped  table-hover table-condensed " id="tableMain" >
        <thead class="theadx" >
        <th style="max-width: 40px;text-align: center;">Tarikh Pohon</th>              
        <th style="max-width: 40px;text-align: center;">Staf</th>
                        <th style="text-align: center;">Jabatan</th>
                        <th style="text-align: center;">Jawatan</th>
                        <th style="text-align: center;">Jenis Cuti</th>
                        <th style="text-align: center;">Mula</th>
                        <th style="text-align: center;">Akhir</th>
                        <th style="text-align: center;">Sebab Cuti</th>
                        <th style="text-align: center;">Pengganti</th>
                        <th style="text-align: center;">Status</th>
                    
                    </thead>
                    <tbody>
                        
                        
       <?php
    
        foreach ($this->semuaCuti as $row)
        {
           
          
             
           echo'<tr >
               <td >'.$row['tarikhpohon'].'</td>
               <td >'.$row['cuti_staffno'].' : '.$row['staff_namapenuh'].'</td>
                      <td >'.$row['dept_name'].'</td>
                       <td >'.$row['p_name'].'</td>
                        
                        <td >'.$row['cuti_jeniscuti'].'</td>
                        <td >'.$row['tarikhmula'].'</td>
                        <td >'.$row['tarikhtamat'].'</td>
                        <td >'.$row['cuti_sebabcuti'].'</td>';
             if($row['cuti_pengganti'])
           {
            echo '<td style="text-align: left">'.$row['cuti_pengganti'].' : '.$row['ganti_namapenuh'].'</td>';   
           }else
           {
             echo '<td style="text-align: left">-Tiada-</td>';  
           }
                   
           switch ($row['cuti_status']) {
           case 'Menunggu': echo '<td ><span class="label label-warning col-md-12">'.$row['cuti_status'].'</span></td>';
           break;
           
           case 'Lulus': echo '<td><span class="label label-success col-md-12">'.$row['cuti_status'].'</span></td>';
           break;
       
            case 'Ditolak': echo '<td><span class="label label-danger col-md-12">'.$row['cuti_status'].'</span></td>';
           break;
       
       
           case 'Batal': echo '<td><span class="label label-default col-md-12">'.$row['cuti_status'].'</span></td>';
           break;
       
           default: echo '<td><span class="label label-info col-md-12">'.$row['cuti_status'].'</span></td>';
           break;
           }    

                echo'</tr>';

        }        
      ?>                
                    </tbody>
                    
    </table>   
    
    
 </div>
      
      
      
      
   <div id="profilS" class="tab-pane fade">
    <h3></h3>                           
      
    <p style="min-height: 20px"></p>                              
     
    
    
    
    <table class="table table-bordered table-responsive table-striped  table-hover table-condensed " id="tableMain3" >
        <thead class="theadx" >
    
                        
                        <th style="text-align: center;">No. Staf</th>
                        <th style="text-align: center;">Nama Penuh</th>
                        <th style="text-align: center;">Jabatan</th>
                        <th style="text-align: center;">Jawatan</th>
                       <th style="text-align: center;">Kelayakan Asal(hari)</th>
                       <th style="text-align: center;">Baki Tahun <?php echo date('Y', strtotime('-1 years')) ?></th>
                       <th style="text-align: center;">Baki Tahun <?php echo date('Y', strtotime('0 years')) ?></th>
                        <th style="text-align: center;">Total Kelayakan (hari)</th>
                        <th style="text-align: center;">Status Akaun</th>
                    
                    </thead>
                    <tbody>
                        
                        
       <?php
    


        foreach ($this->semuaStaff as $row)
        {
           
          
             
           echo'<tr >
              
               <td> '.$row['staff_staffno'].'</td>
               <td> '.$row['staff_namapenuh'].'</td>
               <td >'.$row['dept_name'].'</td>
               <td >'.$row['p_name'].'</td>
               <td >'.$row['p_cuti_count'].'</td>
               <td >'.$row['bc_baki_tahun_lepas'].'</td>
               <td >'.$row['tahun_ini'].'</td>
                <td >'.$row['bc_baki'].'</td>';
                
           
                   
           switch ($row['staff_accstatus']) {
           case 'active': echo '<td ><span class="label label-success col-md-12">Aktif</span></td>';
           break;
           
           case 'blocked': echo '<td><span class="label label-warning col-md-12">Disekat</span></td>';
           break;
       
           case 'deleted': echo '<td><span class="label label-default col-md-12">Dibatalkan</span></td>';
           break;
       
           default:
           break;
           }    
                
           
        }        
      ?>                
                    </tbody>
                    
    </table>
    
    
    
    
    
    
    
    
                            
   </div>                  
                  
  </div>  
                            
                            
                            
                            
                            

<!-- ##########  EDIT DI KAWASAN ATAS INI SAHAJA!  ########-->
</div> </div>
   </div><!-- /.box-body -->
   </div><!-- /.box --> 
</div><!-- tutup Col --> 
</div><!-- tutup Row --> 



  
 



<?php include '_foot.php'; ?>
