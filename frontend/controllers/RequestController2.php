<?php

namespace frontend\controllers;

use Yii;
use yii\base\Model;
use common\models\Transactions;
use common\models\TransactionsSearch;
use common\models\RequestSearch;
use common\models\Orders;
use common\models\OrdersSearch;
use common\models\OrderItems;
use common\models\OrderItemsSearch;
use common\models\InventoryItems;
use common\models\InventoryItemsSearch;
use common\models\Inventories;
use common\models\InventoriesSearch;
use common\models\Vendors;
use common\models\VendorsSearch;
use common\models\Package;
use common\models\PackageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\components\AccessRule;
use kartik\widgets\Alert;
use kartik\widgets\Growl;
use yii\base\Exception;

/**
 * TransactionsController implements the CRUD actions for Transactions model.
 */
class RequestController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-multiple' => ['POST'],
                    'recover' => ['POST'],
                    'delete-permanent' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),],
                'only' => ['create', 'update', 'index', 'view',
                    'delete', 'delete-permanent', 'recover'],
                'rules' => [
                    [
                        'actions' => ['delete-permanent'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'recover'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Transactions models.
     * @return mixed
     */
//    public function actionIndex2() {
//        $searchModel = new TransactionsSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//                    'searchModel' => $searchModel,
//                    'dataProvider' => $dataProvider,
//        ]);
//    }
//    in development
//    public function actionItemList2($id) {
////        $searchModel = new RequestSearch();
////        $orderItems = OrderItems::find()->where(['ORDER_ID' => $id])->all();
////        $orderItems = [0 => ['ID' => 92], 1 => ['ID' => 93]];
////        foreach ($orderItems as $orderItem) {
////            $orderItemIds[] = $orderItem['ID'];
//////            $orderItemIds = $orderItem->ID;
////        }
//////        var_dump($orderItemIds);die();
//////        $searchModel->DELETED = 1;
////        $testArray = [93, 92];
////        $testArray = $orderItemIds;
////        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//////        $dataProvider->query->andWhere(['in', 'CHECKOUT_TRANSACTION_ID',$testArray]);
//////        $dataProvider->query->andWhere(['not','CHECKOUT_TRANSACTION_ID', null]);
//////        $dataProvider->query->andFilterWhere(['DELETED'=>1]);
//////        var_dump($dataProvider->models[0]->inventory);die();
//////        die();
//////        $data = Transactions::find()->all();
//////        $data = \common\models\Orders::find()->all();
//////        var_dump($data);die();
//        $ordersId = $id;
//        $searchModel = new \common\models\ApprovalSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider->query->where(['ID_ORDERS' => $ordersId]);
////        $dataProvider->query->andWhere(['INVENTORY_ITEMS.CHECKOUT_TRANSACTION_ID' => null]);
////        $dataProvider->query->andWhere(['not',['INVENTORY_ITEMS.CHECKOUT_TRANSACTION_ID' => null]]);
////        $dataProvider->query->andWhere(['not','INVENTORY_ITEMS.CHECKOUT_TRANSACTION_ID', null]);
//        var_dump($dataProvider->models);
//        die();
//        return $this->render('item-list', [
//                    'searchModel' => $searchModel,
//                    'dataProvider' => $dataProvider,
//        ]);
//    }

    public function actionItemList($id) {
//        $ordersId = $ordersId;
        $searchModel = new \common\models\InventoryItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider->query->where(['ORDERS.ID' => $ordersId]);
        $dataProvider->query->andWhere(['INVENTORY_ITEMS.CHECKOUT_TRANSACTION_ID' => $id]);
//        $dataProvider->query->andWhere(['INVENTORY_ITEMS.CHECKOUT_TRANSACTION_ID' => null]);
//        $dataProvider->query->andWhere(['not', ['INVENTORY_ITEMS.CHECKOUT_TRANSACTION_ID' => null]]);
//        var_dump($dataProvider->models);die();
        return $this->render('item-list-grid', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
//                    'ordersId' => $ordersId,
        ]);
    }

    public function actionIndex() {
        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndex2() {
        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index2', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

//    public function actionSearch() {
//        $searchModel = new RequestSearch();
//        return $this->renderPartial('_search', [
//                    'searchModel' => $searchModel,
//        ]);
//    }

    public function actionView($id) {
        $model = $this->findOrders($id);
//        $model = $this->findModel($id);
        $searchModel = new \common\models\FormKewps10Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->filterWhere(['ORDER_ID' => $id]);
        $items = $dataProvider->models;
//        var_dump($items[0]);die();
//        $dataProvider->query->andFilterWhere(['DELETED'=>1]);
        return $this->render('view', [
                    'items' => $items,
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

//    public function actionKew2() {
//        $searchModel = new RequestSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
////        $dataProvider->query->andFilterWhere(['DELETED'=>1]);
//        return $this->render('_kewps10', [
////        return $this->render('_view', [
//                    'searchModel' => $searchModel,
//                    'dataProvider' => $dataProvider,
//        ]);
//    }

    public function actionPrintKewps10($id) {
        // $id = 1032;
        // Orders::pdfKewps102($id);
        \common\models\KewpsForm::pdfKewps10($id);
    }
    public function actionKew($id) {
        $model = $this->findOrders($id);
//        $model = $this->findModel($id);
        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->filterWhere(['ORDER_ID' => $id]);
        $items = $dataProvider->models;
//        $dataProvider->query->andFilterWhere(['DELETED'=>1]);
        $content = $this->renderPartial('_kewps10', [
            'items' => $items,
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
//        var_dump($dataProvider);die();
//        $pdf = new \kartik\mpdf\Pdf([
//            'mode' => \kartik\mpdf\Pdf::MODE_CORE, // leaner size using standard fonts
//            'content' => $content,
//            'defaultFontSize' => $content,
////            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
////            'cssFile' => '@frontend/assets/dist/css/Pdf.css',
//            'format' => 'A4-L',
////            'format' => 'A4',
////            'defaultFont' => 'Times New Roman',
////            'orientation' => 'L',
////            'destination' => 'I',
////            'orientation' => \kartik\mpdf\Pdf::ORIENT_LANDSCAPE,
////            'orientation' => \kartik\mpdf\Pdf::ORIENT_PORTRAIT,
//            // any css to be embedded if required
////            'cssInline' => '.form-id,.form-lampiran{font-size:10px} .right-border-bold{border:10px solid #000}',
//            'filename' => 'KEW PS 10',
//            'options' => [
//                'title' => 'Borang Pemesanan Dan Pengeluaran',
////                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
//            ],
//            'methods' => [
//                'SetHeader' => ['Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran A'],
////                'SetFooter' => ['|Page {PAGENO}|'],
//            ]
////            'methods' => [
////                'SetHeader' => ['Generated By: MPSP Estor Application||Generated On: ' . date("r")],
//////                'SetFooter' => ['|Page {PAGENO}|'],
////            ]
//        ]);
        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE, // leaner size using standard fonts
            'content' => $content,
//            'defaultFontSize' => 30,
//            'defaultFont' => '',
//
            'marginLeft' => 13,
            'marginRight' => 13,
            'marginTop' => 10,
            'marginBottom' => 6,
            'marginHeader' => 5,
            'marginFooter' => 5,
//            'marginLeft' => 15,
//            'marginRight' => 15,
//            'marginTop' => 16,
//            'marginBottom' => 16,
//            'marginHeader' => 9,
//            'marginFooter' => 9,
            'cssFile' => '@frontend/assets/dist/css/Pdf.css',
            'format' => 'A4-L',
            'filename' => 'KEW PS 10.pdf',
            'options' => [
                'title' => 'Borang Pemesanan Dan Pengeluaran',
//                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'methods' => [
                'SetHeader' => ['Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran A'],
                'SetFooter' => ['|{PAGENO}|'],
            ]
        ]);
//        var_dump($pdf);
//        die();
//         $pdf = new \kartik\mpdf\Pdf([
//        // set to use core fonts only
//        'mode' => \kartik\mpdf\Pdf::MODE_CORE,
//        // A4 paper format
////        'format' => \kartik\mpdf\Pdf::FORMAT_A4,
//        // portrait orientation
//        'orientation' => \kartik\mpdf\Pdf::ORIENT_LANDSCAPE,
//        // stream to browser inline
//        'destination' => \kartik\mpdf\Pdf::DEST_BROWSER,
//        // your html content input
//        'content' => $content,
//        // format content from your own css file if needed or use the
//        // enhanced bootstrap css built by Krajee for mPDF formatting
//        'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
//        // any css to be embedded if required
//        'cssInline' => '.kv-heading-1{font-size:18px}',
//         // set mPDF properties on the fly
//        'options' => ['title' => 'Krajee Report Title'],
//         // call mPDF methods on the fly
//        'methods' => [
//            'SetHeader'=>['Krajee Report Header'],
//            'SetFooter'=>['{PAGENO}'],
//        ]
//    ]);
//        var_dump(Yii::$app->response->format);die();
//        $response = Yii::$app->response;
//        $response->format = \yii\web\Response::FORMAT_RAW;
//        $headers = Yii::$app->response->headers;
//        $headers->add('Content-Type', 'application/pdf');
//        var_dump($pdf);
//        die();

        $mpdf = new \mPDF('utf-8', 'A4-L');
//        $mpdf = new \mPDF('utf-8','A4');
//        $mpdf = new \mPDF('c','A4');
        $filename = '@frontend/assets/dist/css/Pdf.css';
        $filedir = Yii::getAlias($filename);
//        $filename = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
//        $stylesheet = file_get_contents('@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css');
        $stylesheet = file_get_contents($filedir);
//        var_dump($filedir);
//        die();
//        var_dump($stylesheet);die();
//        $mpdf->WriteHTML($stylesheet, 1);
//        var_dump($content);die();
//        $mpdf->WriteHTML($content, 2);
//        var_dump($content);die();

        $mpdf->WriteHTML($stylesheet, 1);
//        $mpdf->useDefaultCSS2 = true;
        $mpdf->WriteHTML($content, 2);
//        $response = Yii::$app->response;
//        $response->format = \yii\web\Response::FORMAT_RAW;
//        $headers = Yii::$app->response->headers;
//        $headers->add('Content-Type', 'application/pdf');

        $pdf->render();
//        $mpdf->Output();
        exit;

//        return $mpdf->Output();
//        return $pdf->render();
    }

//    public function actionApproval2($id) {
//        $orderItemsId = $id;
////        var_dump($orderItemsId);die();
////        $debug = InventoryItems::find()->joinWith('requestOrderItems')
//////                ->where(['ORDER_ITEMS.ID'=>$id])
////                        ->asArray()->all();
//
//        $searchModel = new \common\models\ApprovalItemsSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
////        $dataProvider->query->where(['ORDER_ITEMS.ORDER_ID' => $id]);
////        $dataProvider->query->andWhere(['CHECKOUT_TRANSACTION_ID' => null]);
////        var_dump($debug);
////        die();
////        var_dump($dataProvider->models);die();
////        var_dump($dataProvider->models);die();
////        $dataProvider->query->andFilterWhere(['DELETED'=>1]);
////        new test
////        $query = \common\models\InventoryItems::find();
////        $query->where(['ORDER_ID' => $id]);
////        $searchModel->leftJoin('INVENTORIES', ['INVENTORY_ID'=>1]);
////                ->rightJoin('ORDER_ITEMS', ['ID'=>$id]);
////        $dataProvider = new \yii\data\ActiveDataProvider(['query' => $query,]);
//
//        return $this->render('approval1', [
//                    'searchModel' => $searchModel,
//                    'dataProvider' => $dataProvider,
//                    'orderItemsId' => $orderItemsId,
//        ]);
//    }

    public function actionApproval($ordersId) {
        $searchModel = new \common\models\ApprovalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $ordersId);
        $dataProvider->query->orderBy('INVENTORY_ITEMS.SKU');
        if (\Yii::$app->request->post('smart-approve')) {
            $dbtransac = \Yii::$app->db->beginTransaction();
            try {
                if ($order && $orderItems && $inventory && $inventoryItems) {
                    //assign checkout id to items(link)
                    $inventoryItems->CHECKOUT_TRANSACTION_ID = $orderItemsId;
                    //remove 1 items from inventory
                    $inventory->QUANTITY = $inventory->QUANTITY - 1;
                    //add 1 item to approved quantity
                    $orderItems->APP_QUANTITY = $orderItems->APP_QUANTITY + 1;
                    //update total price of checkout items
                    $orderItems->UNIT_PRICE = $orderItems->UNIT_PRICE + $inventoryItems->UNIT_PRICE;
                    //save all model
                    if (!$inventoryItems->save(false))
                        Throw new Exception(' Data item inventori tidak dapat dikemaskini.');
                    if (!$inventory->save(false))
                        Throw new Exception(' Data inventori tidak dapat dikemaskini.');
                    if (!$orderItems->save(false))
                        Throw new Exception(' Data pesanan tidak dapat dikemaskini.');
                    $dbtransac->commit();
                    \Yii::$app->notify->success('Proses kemaskini data berjaya');
                } else {
                    throw new Exception(' Data tidak wujud.');
                }
            } catch (Exception $e) {
                \Yii::$app->notify->fail($e->getMessage());
                $dbtransac->rollBack();
            } catch (\yii\db\Exception $e) {
                \Yii::$app->notify->fail($e->getMessage());
                $dbtransac->rollBack();
            }
        }

        return $this->render('approval', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'ordersId' => $ordersId,
        ]);
    }

    public function actionApprove($id, $orderItemsId) {
        $inventoryItems = InventoryItems::findOne($id);
        $inventory = Inventories::findOne($inventoryItems->checkInTransaction->INVENTORY_ID);
        $orderItems = OrderItems::findOne($orderItemsId);
        $order = Orders::findOne($orderItems->ORDER_ID);

        $dbtransac = \Yii::$app->db->beginTransaction();
        try {
            if ($order && $orderItems && $inventory && $inventoryItems) {
                //assign checkout id to items(link)
                $inventoryItems->CHECKOUT_TRANSACTION_ID = $orderItemsId;
                //remove 1 items from inventory
                $inventory->QUANTITY = $inventory->QUANTITY - 1;
                //add 1 item to approved quantity
                $orderItems->APP_QUANTITY = $orderItems->APP_QUANTITY + 1;
                //update total price of checkout items
                $orderItems->UNIT_PRICE = $orderItems->UNIT_PRICE + $inventoryItems->UNIT_PRICE;
                //save all model
                if (!$inventoryItems->save(false))
                    Throw new Exception(' Data item inventori tidak dapat dikemaskini.');
                if (!$inventory->save(false))
                    Throw new Exception(' Data inventori tidak dapat dikemaskini.');
                if (!$orderItems->save(false))
                    Throw new Exception(' Data pesanan tidak dapat dikemaskini.');
                $dbtransac->commit();
                \Yii::$app->notify->success('Proses kemaskini data berjaya');
            } else {
                throw new Exception(' Data tidak wujud.');
            }
        } catch (Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionApproveMultiple($id, $orderItemsId) {
        $inventoryItems = InventoryItems::findOne($id);
        $inventory = Inventories::findOne($inventoryItems->checkInTransaction->INVENTORY_ID);
        $orderItems = OrderItems::findOne($orderItemsId);
        $order = Orders::findOne($orderItems->ORDER_ID);

        $inventoryItems->CHECKOUT_TRANSACTION_ID = $orderItemsId;
        $inventory->QUANTITY = $inventory->QUANTITY - 1;
        $orderItems->APP_QUANTITY = $orderItems->APP_QUANTITY + 1;
        $orderItems->UNIT_PRICE = $orderItems->UNIT_PRICE + $inventoryItems->UNIT_PRICE;

//        $inventoryItems->save();
//        $inventory->save();
//        $orderItems->save();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionApproveTransaction($ordersId) {
        $order = Orders::findOne($ordersId);
        //change order status to approved
        $order->APPROVED = 1;
        $order->APPROVED_BY = Yii::$app->user->id;
        $order->APPROVED_AT = date('d-M-y h.i.s a');

        $dbtransac = Yii::$app->db->beginTransaction();
        try {
            if (!$order->save(false))
                Throw new Exception(' Data tidak dapat dikemaskini.');
            $dbtransac->commit();
        } catch (Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        }

        return $this->redirect(['index']);
    }

//    public function actionCreate2() {
//        $transaction = new Transactions();
//        $inventory = new Inventories();
//        $order = new Orders();
//        $orderItem = new OrderItems();
//
//        $inventories = \common\models\Inventories::find()->where(['DELETED' => 0])->asArray()->all();
//        $inventoriesArray = \yii\helpers\ArrayHelper::map($inventories, 'ID', 'DESCRIPTION');
//        $vendors = \common\models\Vendors::find()->where(['DELETED' => 0])->asArray()->all();
//        $vendorsArray = \yii\helpers\ArrayHelper::map($vendors, 'ID', 'NAME');
//
//        if ($transaction->load(Yii::$app->request->post())) {
//            if ($transaction->save()) {
//                \Yii::$app->session->setFlash('success', [
//                    'type' => Growl::TYPE_SUCCESS,
//                    'duration' => 3000,
//                    'icon' => 'glyphicon glyphicon-ok-sign',
//                    'title' => ' Proses BERJAYA',
//                    'message' => ' Data berjaya ditambah.',
//                    'positonY' => 'top',
//                    'positonX' => 'right'
//                ]);
//            } else {
//                \Yii::$app->session->setFlash('error', [
//                    'type' => Growl::TYPE_DANGER,
//                    'duration' => 3000,
//                    'icon' => 'glyphicon glyphicon-remove-sign',
//                    'title' => ' Proses GAGAL.',
//                    'message' => ' Data tidak berjaya ditambah.',
//                    'positonY' => 'top',
//                    'positonX' => 'right'
//                ]);
//            }
//            return $this->redirect(['view', 'id' => $transaction->ID]);
////return $this->redirect(['index']);
//        } else {
//            return $this->render('create', [
//                        'transaction' => $transaction,
//                        'inventory' => $inventory,
//                        'order' => $order,
//                        'orderItem' => $orderItem,
//                        'inventoriesArray' => $inventoriesArray,
//                        'vendorsArray' => $vendorsArray,
//            ]);
//        }
//    }

    public function actionCreate() {
        //create list for inventory dropdown
        $inventories = \common\models\Inventories::find()->where(['DELETED' => 0])->andWhere(['not', ['QUANTITY' => 0]])->asArray()->all();
        $inventoriesArray = \yii\helpers\ArrayHelper::map($inventories, 'ID', function($model, $defaultValue) {
                    return $model['CODE_NO'] . ' - ' . $model['DESCRIPTION'] . ' {' . $model['QUANTITY'] . '}';
                });
        //create list for vendor dropdown
        $vendors = \common\models\Vendors::find()->where(['DELETED' => 0])->asArray()->all();
        $vendorsArray = \yii\helpers\ArrayHelper::map($vendors, 'ID', 'NAME');
        //create list for vehicle dropdown
        $vehicles = \common\models\VehicleList::find()->where(['DELETED' => 0])->asArray()->all();
        $vehiclesArray = \yii\helpers\ArrayHelper::map($vehicles, 'ID', ['REG_NO'], 'TYPE');
        //itterate bill no
        $bilStock = Orders::find()
                ->where(['EXTRACT(MONTH FROM CREATED_AT)' => date('m')])
                ->andWhere(['EXTRACT(YEAR FROM CREATED_AT)' => date('Y')])
                ->max('CAST(SUBSTR(ORDER_NO,9) AS INT)');
        $bilStock = $bilStock + 1;
        $inventory = new Inventories();
        $transaction = new Transactions();
        $order = new Orders(['scenario' => 'checkout']);
        $orderItems = [new OrderItems(['scenario' => 'checkout'])];
        //**need to check for orderItems to before proceed
        if ($order->load(Yii::$app->request->post())) {
            //set transaction detail for checkin
            $transaction->TYPE = 2;     //checkIn
            $transaction->CHECK_DATE = date('d-M-Y');
            $transaction->CHECK_BY = Yii::$app->user->id;
            //itterate bill no
            $bilStock = Orders::find()
                    ->where(['EXTRACT(MONTH FROM CREATED_AT)' => date('m')])
                    ->andWhere(['EXTRACT(YEAR FROM CREATED_AT)' => date('Y')])
                    ->max('CAST(SUBSTR(ORDER_NO,9) AS INT)');
            $bilStock = $bilStock + 1;
            //assign bill no to order no
            $order->ORDER_NO = date('Y/m') . '-' . $bilStock;
            //change order status to pending
            $order->APPROVED = 2;
            //create multiple order items model and load with data
            $orderItems = \common\models\Model::createMultiple(OrderItems::classname(['scenario' => 'checkout']));
//            foreach ($orderItems as $orderItem) {
//                $orderItem->scenario = 'checkout';
//            }
            Model::loadMultiple($orderItems, Yii::$app->request->post());
            //validate all model
            $valid = $transaction->validate();
            $valid = $order->validate();
            $valid = Model::validateMultiple($orderItems) && $valid;
            if ($valid) {
                $dbtransac = \Yii::$app->db->beginTransaction();

                try {
//                    var_dump($transaction->ID);
                    if (!$transaction->save(false)) {
                        Throw new Exception(' Detail transaksi tidak dapat disimpan.');
                    }
//                    var_dump($transaction->ID);
                    $order->TRANSACTION_ID = $transaction->ID;
//                    var_dump($order->ID);
                    if (!$order->save(false)) {
                        Throw new Exception(' Detail pesanan tidak dapat disimpan.');
                    }
//                    var_dump($order->ID);die();
                    foreach ($orderItems as $orderItem) {
                        $inventory = Inventories::findOne($orderItem->INVENTORY_ID);

                        $orderItem->ORDER_ID = $order->ID;
                        $orderItem->CURRENT_BALANCE = $inventory->QUANTITY;
                        if (!$orderItem->save(false))
                            Throw new Exception(' Detail item pesanan tidak dapat disimpan.');
                    }

                    $dbtransac->commit();
                    return $this->redirect(['index']);
                } catch (Exception $e) {
                    \Yii::$app->notify->fail($e->getMessage());
                    $dbtransac->rollBack();
                } catch (\yii\db\Exception $e) {
                    \Yii::$app->notify->fail($e->getMessage());
                    $dbtransac->rollBack();
                }
            } else {
                \Yii::$app->notify->fail(' Validasi data gagal. Sila pastikan data yang dimasukkan.');
            }
        }
        return $this->render('create', [
                    'transaction' => $transaction,
                    'vehiclesArray' => $vehiclesArray,
                    'inventoriesArray' => $inventoriesArray,
                    'order' => $order,
                    'bilStock' => $bilStock,
                    'orderItems' => (empty($orderItems)) ? [new OrderItems] : $orderItems,
        ]);
    }

    /**
     * Updates an existing Transactions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $orderItems = $this->findOrderItems($id);
        $order = $this->findOrders($orderItems->ORDER_ID);

        $inventories = \common\models\Inventories::find()->where(['DELETED' => 0])->asArray()->all();
        $inventoriesArray = \yii\helpers\ArrayHelper::map($inventories, 'ID', function($model, $defaultValue) {
                    return $model['CODE_NO'] . ' - ' . $model['DESCRIPTION'];
                });
        $vendors = \common\models\Vendors::find()->where(['DELETED' => 0])->asArray()->all();
        $vendorsArray = \yii\helpers\ArrayHelper::map($vendors, 'ID', 'NAME');
        $vehicles = \common\models\VehicleList::find()->where(['DELETED' => 0])->asArray()->all();
        $vehiclesArray = \yii\helpers\ArrayHelper::map($vehicles, 'ID', ['REG_NO'], 'TYPE');

        if ($order->load(Yii::$app->request->post())) {
            if ($order->save()) {
                \Yii::$app->notify->success(' Data berjaya dikemaskini.');
                return $this->redirect(['view', 'id' => $order->ID]);
            } else {
                \Yii::$app->notify->fail(' Data tidak berjaya dikemaskini.');
            }
        }
        return $this->render('update', [
                    'order' => $order,
                    'vehiclesArray' => $vehiclesArray,
                    'inventoriesArray' => $inventoriesArray,
        ]);
    }

    /**
     * Reject an existing Transactions model.
     * If rejection is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionReject($id) {
        $orderItem = $this->findOrderItems($id);
        $order = $this->findOrders($orderItem->ORDER_ID);
        $transaction = $this->findTransaction($order->TRANSACTION_ID);
        $orderItems = $this->findAllOrderItems($orderItem->ORDER_ID);
        $inventoryItems = InventoryItems::find()->where(['CHECKOUT_TRANSACTION_ID' => $orderItem->ID])->all();

        if (empty($order) || empty($transaction) || empty($orderItems)) {
            \Yii::$app->notify->fail(' Data tidak berjaya ditemui.');
            $this->redirect(Yii::$app->request->referrer);
        }
        $dbtransac = \Yii::$app->db->beginTransaction();

        try {
            $flag = true;
            foreach ($orderItems as $item) {
                if (!empty($inventoryItems)) {
                    $flag = InventoryItems::updateAll(['CHECKOUT_TRANSACTION_ID' => null], ['and',
                                ['DELETED' => 0],
                                ['CHECKOUT_TRANSACTION_ID' => $item->ID],
                    ]);
                }
                $item->APP_QUANTITY = null;
                $item->CURRENT_BALANCE = $item->CURRENT_BALANCE + $item->APP_QUANTITY;
                $item->UNIT_PRICE = null;
                if ($flag = $flag && $item->save(false)) {
                    Inventories::updateQuantity($item->INVENTORY_ID);
                } else {
                    Throw new Exception(' Data tidak dapat dikemaskini.');
                }
            }
            $order->ARAHAN_KERJA_ID = null;
            $order->APPROVED = 8;
            $order->APPROVED_AT = date('d-M-y h.i.s a');

            if (!$flag || !$order->save(false) || !$transaction->save(false))
                Throw new Exception(' Data tidak dapat dikemaskini.');
            $dbtransac->commit();
            \Yii::$app->notify->success(' Data berjaya dikemaskini.');
        } catch (Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Deletes an existing Transactions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $orderItem = $this->findOrderItems($id);
        $order = $this->findOrders($orderItem->ORDER_ID);
        $transaction = $this->findTransaction($order->TRANSACTION_ID);
        $orderItems = $this->findAllOrderItems($orderItem->ORDER_ID);
//        $inventoryItems = InventoryItems::find()->where(['CHECKOUT_TRANSACTION_ID' => $orderItem->ID])->all();
//        $orderItems = OrderItems::findAllByOrderItemId($id);
//       $order = $this->findOrders($orderItem->ORDER_ID);
//        $transaction = $this->findTransaction($order->TRANSACTION_ID);
        $inventoryItems = InventoryItems::findByOrderItemId($id);

        if (empty($order) || empty($transaction) || empty($orderItems)) {
            \Yii::$app->notify->fail(' Data tidak berjaya ditemui.');
            $this->redirect(Yii::$app->request->referrer);
        }

        $dbtransac = \Yii::$app->db->beginTransaction();
        try {
            $flag = true;
            foreach ($orderItems as $item) {
                if (!empty($inventoryItems)) {
                    $flag = InventoryItems::updateAll(['CHECKOUT_TRANSACTION_ID' => null], ['and',
                                ['DELETED' => 0],
                                ['CHECKOUT_TRANSACTION_ID' => $item->ID],
                    ]);
                }
                $item->APP_QUANTITY = null;
                $item->CURRENT_BALANCE = $item->CURRENT_BALANCE + $item->APP_QUANTITY;
                $item->DELETED = 1;
                $item->DELETED_AT = date('d-M-y h.i.s a');

                if (!$flag = $flag && $item->save(false))
                    Throw new Exception(' Data tidak berjaya dikemaskini.');
                Inventories::updateQuantity($item->INVENTORY_ID);
            }
            $order->ARAHAN_KERJA_ID = null;
            $order->DELETED = 1;
            $order->DELETED_AT = date('d-M-y h.i.s a');
            $transaction->DELETED = 1;
            $transaction->DELETED_AT = date('d-M-y h.i.s a');

            if (!$flag = $flag && $order->save(false) && $transaction->save(false))
                Throw new Exception(' Data tidak berjaya dikemaskini.');
            $dbtransac->commit();
            \Yii::$app->notify->success(' Data berjaya dikemaskini.');
        } catch (Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionRecover($id) {
//        var_dump(date('d-M-y h.i.s a'));
//        die();
        $orderItem = $this->findOrderItems($id);
        $order = $this->findOrders($orderItem->ORDER_ID);
        $transaction = $this->findTransaction($order->TRANSACTION_ID);
        $orderItems = $this->findAllOrderItems($orderItem->ORDER_ID);
//        $orderItems = OrderItems::findOne($id);
        $inventoryItems = InventoryItems::find()->where(['CHECKOUT_TRANSACTION_ID' => $orderItem->ID])->all();
        if (empty($order) || empty($transaction)) {
            \Yii::$app->notify->fail(' Data tidak berjaya ditemui.');
            $this->redirect(Yii::$app->request->referrer);
        }
        $dbtransac = \Yii::$app->db->beginTransaction();
        try {
            $flag = true;
            if (!empty($orderItems)) {
                foreach ($orderItems as $item) {
                    if (!empty($inventoryItems)) {
                        $flag = InventoryItems::updateAll(['CHECKOUT_TRANSACTION_ID' => null], ['and',
                                    ['DELETED' => 1],
                                    ['CHECKOUT_TRANSACTION_ID' => $item->ID],
                        ]);
                    }
                    $item->CURRENT_BALANCE = $item->CURRENT_BALANCE + $item->APP_QUANTITY;
                    $item->APP_QUANTITY = null;
                    $item->DELETED = 0;
                    $item->DELETED_AT = date('d-M-y h.i.s a');
                    if (!$flag = $flag && $item->save(false))
                        Throw new Exception(' Data tidak berjaya dikemaskini.');
                    Inventories::updateQuantity($item->INVENTORY_ID);
                }
            }
            $order->DELETED = 0;
            $order->DELETED_AT = date('d-M-y h.i.s a');
            $transaction->DELETED = 0;
            $transaction->DELETED_AT = date('d-M-y h.i.s a');
            $order->ARAHAN_KERJA_ID = '';
            if (!$flag = $flag && $order->save(false))
                Throw new Exception(' Data Permohonan tidak berjaya dikemaskini.');
            if (!$flag = $flag && $transaction->save(false))
                Throw new Exception(' Data Transaksi tidak berjaya dikemaskini.');
            $dbtransac->commit();
            \Yii::$app->notify->success(' Data berjaya dikemaskini.');
        } catch (Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionDeletePermanent($id) {
        $orderItem = $this->findOrderItems($id);
        $orderItems = OrderItems::find()->where(['ORDER_ID' => $orderItem->ORDER_ID])->all();
        $order = $this->findOrders($orderItem->ORDER_ID);
        $transaction = $this->findTransaction($order->TRANSACTION_ID);
        $dbtransac = Yii::$app->db->beginTransaction();
        try {
            $flag = true;
            if ($order->APPROVED == 8 || $order->DELETED == 1) {
                if (!empty($orderItems))
                    $flag = OrderItems::deleteAll(['ORDER_ID' => $orderItem->ORDER_ID], ['APP_QUANTITY' => 0]);
                if (!$flag || !$order->delete() || !$transaction->delete())
                    Throw new Exception(' Data tidak berjaya dipadam.');
            } else {
                Throw new Exception(' Data masih diperlukan. Hanya transaksi yang telah DIPADAM/DITOLAK sahaja boleh dipadam secara kekal.');
            }
            $dbtransac->commit();
            \Yii::$app->notify->success(' Data berjaya dikemaskini.');
        } catch (Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Transactions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Transactions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = OrderItems::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findOrderItems($id) {
        if (($model = OrderItems::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findAllOrderItems($orderId) {
        if (($model = OrderItems::find()->where(['ORDER_ID' => $orderId])->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findOrders($id) {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findTransaction($id) {
        if (($model = Transactions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
