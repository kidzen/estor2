<?php

namespace frontend\controllers;

use Yii;
use common\models\ActivityLogs;
use common\models\InventoriesCheckIn;
use common\models\EntrySearch;
use common\models\EntryTransactionSearch;
use common\models\InventoriesCheckInSearch;
use common\models\InventoryItems;
use common\models\InventoryItemsSearch;
use common\models\Inventories;
use common\models\InventoriesSearch;
use common\models\Vendors;
use common\models\VendorsSearch;
use common\models\Transactions;
use common\models\TransactionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\components\AccessRule;
use kartik\widgets\Alert;
use kartik\widgets\Growl;
use yii\db\Expression;
use yii\base\Exception;

/**
 * InventoryItemsController implements the CRUD actions for InventoryItems model.
 */
class EntryController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-multiple' => ['POST'],
                    'recover' => ['POST'],
                    'delete-permanent' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),],
                'only' => ['create', 'update', 'index', 'view',
                    'delete', 'delete-permanent', 'recover'],
                'rules' => [
                    [
                        'actions' => ['delete-permanent'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'recover'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all InventoryItems models.
     * @return mixed
     */
    public function actionIndex() {

        $searchModel = new EntryTransactionSearch();
//        $searchModel->DELETED = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $inventoryDetailFilter = Inventories::find()->where(['DELETED' => 0])->asArray()->all();
//        $dataProvider->query->andFilterWhere(['DELETED'=>1]);
//        var_dump($dataProvider->models[0]->inventory);die();
//        die();
//        $data = Transactions::find()->all();
//        $data = \common\models\Orders::find()->all();
//        var_dump($data);die();
       // die();
        return $this->render('index', [
                    'inventoryDetailFilter' => $inventoryDetailFilter,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionItemList($id) {

//        $searchModel = new EntrySearch();
//        $searchModel->DELETED = 1;
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider->query->andWhere(['CHECKIN_TRANSACTION_ID' => $id]);
//        var_dump($id);die();

        $searchModel = new InventoryItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere((['INVENTORIES_CHECKIN.ID' => $id]));

        return $this->render('item-list', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single InventoryItems model.
     * @param string $id
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findInventoriesCheckin($id),
        ]);
        //return $this->redirect(['index']);
    }

    /**
     * Creates a new InventoryItems model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
//        var_dump(Yii::$app->user->id);die();
        $date = date('my');
        $transaction = new Transactions();
        $checkIn = new InventoriesCheckIn(['scenario'=>'entry-create']);
        $inventory = new Inventories();
        $items = new InventoryItems(['scenario'=>'entry-create']);

        $inventories = \common\models\Inventories::find()->where(['DELETED' => 0])->asArray()->all();
        $inventoriesArray = \yii\helpers\ArrayHelper::map($inventories, 'ID', function($model, $defaultValue) {
                    return $model['CODE_NO'] . ' - ' . $model['DESCRIPTION'] . ' {' . $model['QUANTITY'] . '}';
                });
        $vendors = \common\models\Vendors::find()->where(['DELETED' => 0])->asArray()->all();
        $vendorsArray = \yii\helpers\ArrayHelper::map($vendors, 'ID', 'NAME');
        $vehicles = \common\models\VehicleList::find()->where(['DELETED' => 0])->asArray()->all();
        $vehiclesArray = \yii\helpers\ArrayHelper::map($vehicles, 'ID', ['REG_NO'], 'TYPE');

        if ($items->load(Yii::$app->request->post()) && $checkIn->load(Yii::$app->request->post())) {

            $transaction->TYPE = 1;     //checkIn
            $transaction->CHECK_DATE = date('d-M-Y');
            $transaction->CHECK_BY = Yii::$app->user->id;

            $checkIn->ITEMS_TOTAL_PRICE = $items->UNIT_PRICE * $checkIn->ITEMS_QUANTITY;
            $checkIn->CHECK_DATE = date('d-M-Y');
            $checkIn->CHECK_BY = Yii::$app->user->id;
            $checkIn->APPROVED = 1;
            $checkIn->APPROVED_AT = date('d-M-y h.i.s a');
            $checkIn->APPROVED_BY = Yii::$app->user->id;

            $valid = $checkIn->validate();
            $valid = $items->validate() && $valid;
            $valid = $transaction->validate() && $valid;

            if ($valid) {
                $modelInventory = Inventories::findOne([$checkIn->INVENTORY_ID]);
                $modelInventory->QUANTITY = Inventories::getQuantityInStore($checkIn->INVENTORY_ID) + $checkIn->ITEMS_QUANTITY;   //use update counter
                $checkIn->INVENTORY_ID = $modelInventory->ID;   //use link
                $modelItems = \common\models\Model::createMultipleGenerator(InventoryItems::classname(), '', $checkIn);
            } else {
                Yii::$app->notify->fail(' Proses menyimpan data GAGAL!');
                return $this->redirect(['create']);
            }
//            generate sku
            $skuCounter = InventoryItems::find()
                    ->select(["MAX(SUBSTR(sku, -6)) AS SKU"])               //  xxxx-xxxx-{SKU}
                    ->joinWith('inventory')
                    ->where(["TO_CHAR(INVENTORY_ITEMS.CREATED_AT, 'MMYY')" => $date])                //  xxxx-{MMYY}-xxxx
                    ->andWhere(['CODE_NO' => $modelInventory->CODE_NO])                //  xxxx-{MMYY}-xxxx
                    ->one();
            $skuCounter = isset($skuCounter->SKU) ? substr($skuCounter->SKU + 1, -6) : 0; //data-type : integer

            foreach ($modelItems as $i => $modelItem) {
//                mask counter to 6 byte length
                $skuCounter = str_pad($skuCounter, 6, STR_PAD_LEFT, 0);   //999,999 items limit added per month
                $modelItem->SKU = $modelInventory->CODE_NO . '-' . $date . '-' . $skuCounter;
                $modelItem->UNIT_PRICE = $items->UNIT_PRICE;
//                increase counter
                $skuCounter = $skuCounter + 1;
            }

            $valid = $modelInventory->validate() && $valid;
            $valid = \yii\base\Model::validateMultiple($modelItems) && $valid;
            if ($valid) {
                $dbTransaction = \Yii::$app->db->beginTransaction();
                try {
                    //do the save
                    if (!$transaction->save(false)) {
                        Throw new Exception(' Transaksi gagal disimpan.');
                    }
                    $checkIn->TRANSACTION_ID = $transaction->ID;    //use link
                    if (!$checkIn->ITEMS_QUANTITY) {
                        Throw new Exception(' Transaksi gagal disimpan.');
                    }    //use link
                    if (!$checkIn->save(false)) {
                        Throw new Exception(' Transaksi gagal disimpan.');
                    }    //use link
                    if (!$modelInventory->save(false)) {
                        Throw new Exception(' Data Inventori gagal dikemaskini.');
                    }
                    foreach ($modelItems as $i => $modelItem) {
                        $modelItem->CHECKIN_TRANSACTION_ID = $checkIn->ID;  //use link
                        if (!$modelItem->save(false)) {
                            Throw new Exception(' Item Inventori gagal disimpan.');
                        }
                    }
                    ActivityLogs::add(ActivityLogs::LOG_STATUS_SUCCESS, ['Permohonan Kemasukan Barang Berjaya']);
                    $dbTransaction->commit();
                    Yii::$app->notify->success(' ' . $i + 1 . ' item telah berjaya disimpan.');
                    return $this->redirect(['index']);
                } catch (\yii\base\ErrorException $e) {
                    $dbTransaction->rollBack();
                    ActivityLogs::add(ActivityLogs::LOG_STATUS_FAIL, ['Permohonan Kemasukan Barang', serialize($e->getMessage()),]);
                    Yii::$app->notify->fail($e->getMessage());
                }
            } else if (!$valid) {
                ActivityLogs::add(ActivityLogs::LOG_STATUS_WARNING, ['Permohonan Kemasukan Barang', 'Proses validasi data GAGAL!',]);
                Yii::$app->notify->info(' Sila pastikan data yang diisi menepati syarat.');
//                return $this->redirect(['create']);
            }
        }
        return $this->render('create', [
//                    'model' => $model,
                    'transaction' => $transaction,
                    'checkIn' => $checkIn,
                    'inventory' => $inventory,
                    'items' => $items,
                    'inventoriesArray' => $inventoriesArray,
                    'vendorsArray' => $vendorsArray,
        ]);
    }

    public function actionUpdate($id) {
        $date = date('my');
        $checkIn = InventoriesCheckIn::findOne($id);
        $transaction = $checkIn->transaction;
        $inventory = $checkIn->inventory;
        $items = $checkIn->items;

        $inventories = \common\models\Inventories::find()->where(['DELETED' => 0])->asArray()->all();
        $inventoriesArray = \yii\helpers\ArrayHelper::map($inventories, 'ID', function($model, $defaultValue) {
                    return $model['CODE_NO'] . ' - ' . $model['DESCRIPTION'] . ' {' . $model['QUANTITY'] . '}';
                });
        $vendors = Vendors::find()->asArray()->all();
        $vendorsArray = \yii\helpers\ArrayHelper::map($vendors, 'ID', 'NAME');

        if ($checkIn->load(Yii::$app->request->post())) {
            if ($checkIn->APPROVED == 2) {
                $oldIDs = ArrayHelper::map($items, 'ID', 'ID');
                $items = Model::createMultiple(StockItems::classname(), $items);
                Model::loadMultiple($items, Yii::$app->request->post());
                $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($items, 'ID', 'ID')));

                // validate all models
                $valid = $checkIn->validate();
                $valid = Model::validateMultiple($items) && $valid;

                if ($valid) {
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        if ($flag = $checkIn->save(false)) {
                            if (!empty($deletedIDs)) {
                                StockItems::deleteAll(['ID' => $deletedIDs]);
                            }
                            foreach ($items as $item) {
                                $item->CHECKIN_TRANSACTION_ID = $checkIn->ID;

                                if (!($flag = $item->save(false))) {
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                        if ($flag) {
                            $transaction->commit();
                            return $this->redirect(['index']);
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }
            } else {
                if ($checkIn->save()) {
                    Yii::$app->notify->success(' Data berjaya disimpan.');
                } else {
                    Yii::$app->notify->fail(' Data tidak berjaya disimpan.');
                }
            }
        }
        return $this->render('update', [
//                    'model' => $model,
                    'transaction' => $transaction,
                    'checkIn' => $checkIn,
                    'inventory' => $inventory,
                    'items' => (empty($items)) ? [new InventoryItems] : $items,
                    'inventoriesArray' => $inventoriesArray,
                    'vendorsArray' => $vendorsArray,
        ]);
    }

    /**
     * Deletes an existing InventoryItems model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findInventoriesCheckin($id);
        $modelTransaction = $this->findTransactions($model->TRANSACTION_ID);
        $modelItems = InventoryItems::find()->where(['CHECKIN_TRANSACTION_ID' => $model->ID])->all();

//        check if items is already check out or not
        $checkoutedItems[] = '';
        foreach ($modelItems as $item) {
            $checkoutedItems = \common\models\OrderItems::findOne($item->CHECKOUT_TRANSACTION_ID);
        }
        if (!empty($checkoutedItems)) {
            Yii::$app->notify->fail(' Item dalam transaksi ini telah/pernah dikeluarkan dari stor.');
            return $this->redirect(['index']);
        }

        $model->DELETED = '1';
        $model->DELETED_AT = date('d-M-y h.i.s a');
        $modelTransaction->DELETED = '1';
        $modelTransaction->DELETED_AT = date('d-M-y h.i.s a');

        $dbtransac = Yii::$app->db->beginTransaction();
        try {
            foreach ($modelItems as $item) {
                $item->DELETED = '1';
                $item->DELETED_AT = date('d-M-y h.i.s a');
                if (!$item->save()) {
                    Throw new Exception(' Data berjaya dikemaskini');
                }
            }

            Inventories::updateQuantity($model->INVENTORY_ID);
            if (!$modelTransaction->save()) {
                Throw new Exception(' Data berjaya dikemaskini');
            }

            if (!$model->save()) {
                Throw new Exception(' Data tidak berjaya dihapuskan.');
            }
            Yii::$app->notify->success(' Data berjaya dihapuskan.');
            $dbtransac->commit();
        } catch (Exception $e) {
            $dbtransac->rollBack();
            Yii::$app->notify->fail(' Data tidak berjaya dihapuskan.');
        }

        return $this->redirect(['index']);
    }

    public function actionRecover($id) {
        $model = $this->findInventoriesCheckin($id);
        $modelTransaction = $this->findTransactions($model->TRANSACTION_ID);
        $modelItems = InventoryItems::find()->where(['CHECKIN_TRANSACTION_ID' => $model->ID])->all();

        $model->DELETED = '0';
        $model->DELETED_AT = date('d-M-y h.i.s a');
        $modelTransaction->DELETED = '0';
        $modelTransaction->DELETED_AT = date('d-M-y h.i.s a');

        $dbtransac = Yii::$app->db->beginTransaction();
        try {
            foreach ($modelItems as $item) {
                $item->DELETED = '0';
                $item->DELETED_AT = date('d-M-y h.i.s a');
                $item->save();
            }

            Inventories::updateQuantity($model->INVENTORY_ID);
            $modelTransaction->save();

            if ($model->save()) {
                Throw new Exception(' Data tidak berjaya dikembalikan.');
            }
            $dbtransac->commit();
        } catch (\Exception $e) {
            $dbtransac->rollBack();
            Yii::$app->notify->fail(' Data tidak berjaya dikembalikan.');
        }
        return $this->redirect(['index']);
    }

    public function actionDeletePermanent($id) {
        $model = $this->findInventoriesCheckin($id);
        $modelTransaction = $this->findTransactions($model->TRANSACTION_ID);
        $modelItems = InventoryItems::find()
                        ->select('ID')
                        ->where(['CHECKIN_TRANSACTION_ID' => $model->ID])
                        ->asArray()->all();
        $checkoutedItems = InventoryItems::find()
                        ->select('ID')
                        ->where(['CHECKIN_TRANSACTION_ID' => $model->ID])
                        ->andWhere(['not', ['CHECKOUT_TRANSACTION_ID' => null]])
                        ->asArray()->all();
//        check if items is already check out or not
        if (sizeof($checkoutedItems) !== 0) {
            Yii::$app->notify->fail(' Item dalam transaksi ini telah/pernah dikeluarkan dari stor.');
            return $this->redirect(Yii::$app->request->referrer);
        }

        $dbtransac = \Yii::$app->db->beginTransaction();
        try {
            $flag = $model->delete(false);
            if ($flag = $flag && $modelTransaction->delete(false)) {
                $flag = $flag && InventoryItems::deleteAll(['in', 'ID', $modelItems]);
            }
            if (!$flag) {
                throw new Exception("Data does not exist");
            }

            Yii::$app->notify->success(' Data berjaya dihapuskan.');
            $dbtransac->commit();
            return $this->redirect(['index']);
        } catch (yii\db\Exception $e) {
            $dbtransac->rollBack();
            Yii::$app->notify->fail(' Data tidak berjaya dihapuskan.');
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the InventoryItems model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return InventoryItems the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findInventoryItems($id) {
        if (($model = InventoryItems::findOne($id)) !== null) {
            return $model;
        } else {
            Yii::$app->notify->fail(' Data tidak dijumpai.');
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    protected function findInventoriesCheckin($id) {
        if (($model = InventoriesCheckIn::findOne($id)) !== null) {
            return $model;
        } else {
            Yii::$app->notify->fail(' Data tidak dijumpai.');
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    protected function findTransactions($id) {
        if (($model = Transactions::findOne($id)) !== null) {
            return $model;
        } else {
            Yii::$app->notify->fail(' Data tidak dijumpai.');
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

//    protected function findInventory($id) {
//        if (($model = Inventories::findOne($id)) !== null) {
//            return $model;
//        } else {
//            Yii::$app->notify->fail(' Data tidak dijumpai.');
//            return $this->redirect(Yii::$app->request->referrer);
//        }
//    }

}
