<?php

namespace frontend\controllers;

use Yii;
use common\models\FormGenerator;
use common\models\Inventories;
use common\models\KewpsForm;
use common\models\Orders;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;

class OfficialFormController extends \yii\web\Controller {

	public function actionIndex() {
		return $this->render('index');
	}

	public function actionFormGenerator() {
		$model = new FormGenerator();
		$year = \common\models\TransactionsAll::find()
		->select('EXTRACT(YEAR FROM CHECK_DATE) as YEAR')
		->distinct()->indexBy('YEAR')->asArray()->all();
		$yearList = ArrayHelper::map($year,'YEAR','YEAR');
		$monthList = [
		1 => 'Januari',
		2 => 'Febuari',
		3 => 'Mac',
		4 => 'April',
		5 => 'Mei',
		6 => 'Jun',
		7 => 'Julai',
		8 => 'Ogos',
		9 => 'September',
		10 => 'Oktober',
		11 => 'November',
		12 => 'Disember',
		];
		$formId = [
		// 3 => 'KEW.PS-3 ()',
		4 => 'KEW.PS-4 (KAD PETAK)',
		5 => 'KEW.PS-5 (SENARAI DAFTAR KAD KAWALAN STOK)',
		7 => 'KEW.PS-7 (PENENTUAN KUMPULAN STOK)',
		8 => 'KEW.PS-8 (LABEL FIFO)',
		9 => 'KEW.PS-9 (SENARAI STOK BERTARIKH LUPUT)',
		10 => 'KEW.PS-10 (BORANG PESANAN DAN PENGELUARAN STOK)',
		11 => 'KEW.PS-11 (BORANG PEMESANAN STOK)',
		13 => 'KEW.PS-13 (LAPORAN KEDUDUKAN STOK)',
		14 => 'KEW.PS-14 (LAPORAN PEMERIKSAAN / VERIFIKASI STOR)',
		17 => 'KEW.PS-17 (PENYATA PELARASAN STOK)',
		18 => 'KEW.PS-18 (PERAKUAN AMBIL ALIH)',
		];
		$inventoryList = Inventories::find()->andFilterWhere(['DELETED' => 0])->asArray()->all();
		$inventoriesArray = ArrayHelper::map($inventoryList, 'ID', function($model) {return $model['CODE_NO'] . ' - ' .$model['DESCRIPTION'] ;});
		$orderList = Orders::find()
		->andFilterWhere(['DELETED' => 0])
		// ->andFilterWhere(['APPROVED' => 1])
		// ->orderBy(['ID'=>SORT_ASC])
		// ->orderBy(['to_number(substr([[ORDER_NO]],9))'=>SORT_ASC])
		->orderBy(['ID'=>SORT_DESC])
		->asArray()->all();
		$ordersArray = ArrayHelper::map($orderList, 'ID', 'ORDER_NO');
		$formName = new FormGenerator();
//        var_dump($model);die();
		if ($model->load(Yii::$app->request->post())) {
			// var_dump(Yii::$app->request->post());die();
			if ($model->FORM_ID == 3) {
				// KewpsForm::pdfKewps4(
				//     $id = $model->INVENTORY_ID,
				//     $year = $model->YEAR,
				//     $month = $model->MONTH
				//     );
			} else if ($model->FORM_ID == 4) {
				KewpsForm::pdfKewps4(
					$id = $model->INVENTORY_ID,
					$year = $model->YEAR,
					$month = $model->MONTH,
					// $printAll = $model->printAll
					$printAll = 0
					);
			} else if ($model->FORM_ID == 5) {
				KewpsForm::pdfKewps5(
					$id = $model->INVENTORY_LIST
					);
			} else if ($model->FORM_ID == 7) {
				KewpsForm::pdfKewps7(
					$id = $model->INVENTORY_LIST,
					$year = $model->YEAR
					);
			} else if ($model->FORM_ID == 8) {
				KewpsForm::pdfKewps8(
					$id = $model->INVENTORY_LIST
					);
			} else if ($model->FORM_ID == 9) {
				KewpsForm::pdfKewps9(
					$id = $model->INVENTORY_LIST
					);
			} else if ($model->FORM_ID == 10) {
				KewpsForm::pdfKewps10(
					$id = $model->ORDER_ID
					);

			} else if ($model->FORM_ID == 11) {
				KewpsForm::pdfKewps11(
					$id = $model->ORDER_ID
					);
			} else if ($model->FORM_ID == 13) {
				KewpsForm::pdfKewps13(
					$year = $model->YEAR
					);
			} else if ($model->FORM_ID == 14) {
				KewpsForm::pdfKewps14(
					$id = $model->INVENTORY_LIST,
					$year = $model->YEAR
					);
			} else if ($model->FORM_ID == 17) {
				KewpsForm::pdfKewps17(
					$id = $model->INVENTORY_LIST
					);
			} else if ($model->FORM_ID == 18) {
				KewpsForm::pdfKewps18(
					$id = $model->INVENTORY_LIST
					);
			}
		}

		return $this->render('form-generator', [
			'model' => $model,
			'formId' => $formId,
			'yearList' => $yearList,
			'monthList' => $monthList,
			'ordersArray' => $ordersArray,
			'inventoriesArray' => $inventoriesArray,
			]);
	}
	public function actionFormOptions() {
		$out = [];
		if (isset($_POST['depdrop_parents'])) {
			$parents = $_POST['depdrop_parents'];
			if ($parents != null) {
				$form_id = $parents[0];
				if ($form_id == 3) {
					$out = \common\models\Inventories::find()
					// ->select('ID,CODE_NO,CARD_NO,DESCRIPTION,QUANTITY,LOCATION')
					->select('ID as id,DESCRIPTION as name')
					->asArray()->all();
					// $out  = ArrayHelper::index($out,'ID');

				}
				if ($form_id == 4) {
					$out = \common\models\Orders::find()
					->select('ID as id,ORDER_NO as name')
					// ->select('ORDERS.ID,ORDER_NO,ARAHAN_KERJA_ID,APPROVED,VEHICLE_ID')
					// ->with('arahanKerja','vehicle')
					->asArray()->all();
				}
				echo Json::encode(['output' => $out, 'selected' => '']);
				return;
			}
		}
		echo Json::encode(['output' => '', 'selected' => '']);
	}

	public function actionPdfKewps3($id = null) {
		var_dump('note created yet');die();
	}
	public function actionPdfKewps4($id = null) {
		// var_dump($id);die();
		KewpsForm::pdfKewps4($id);
	}

	public function actionPdfKewps5($id = null) {
		KewpsForm::pdfKewps5($id);
	}

	public function actionPdfKewps7($id = [145, 144]) {
		KewpsForm::pdfKewps7($id);
	}

	public function actionPdfKewps8($id = null) {
		KewpsForm::pdfKewps8($id);
	}

	public function actionPdfKewps82($id = null) {
		KewpsForm::pdfKewps82($id);
	}
	public function actionPdfKewps9($id = null) {
		KewpsForm::pdfKewps9($id);
	}

	public function actionPdfKewps10($id = null) {
		KewpsForm::pdfKewps10($id);
	}
	public function actionPdfKewps11($id = null) {
		KewpsForm::pdfKewps11($id);
	}

	public function actionPdfKewps13($id = null, $year = null) {
		KewpsForm::pdfKewps13($id);
	}

	public function actionPdfKewps14($id = null) {
		KewpsForm::pdfKewps14($id);
	}

	public function actionPdfKewps17($id = null) {
		KewpsForm::pdfKewps17($id);
	}

	public function actionPdfKewps18($id = null) {
		KewpsForm::pdfKewps18($id);
	}

}
