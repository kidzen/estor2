<?php

namespace frontend\controllers;

use Yii;
use common\models\InventoryItems;
use common\models\InventoriesCheckIn;
use common\models\Transactions;
use common\models\Inventories;
use common\models\InventoryItemsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\components\AccessRule;
use kartik\widgets\Alert;
use kartik\widgets\Growl;
use yii\db\Expression;
use yii\db\Exception;

/**
 * InventoryItemsController implements the CRUD actions for InventoryItems model.
 */
class InventoryItemsController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'recover' => ['POST'],
                    'delete-multiple' => ['POST'],
                    'delete-permanent' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),],
                'only' => ['create', 'update', 'index', 'view',
                    'delete', 'delete-permanent', 'recover'],
                'rules' => [
                    [
                        'actions' => ['delete-permanent'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'recover'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                        // 'roles' => ['?','@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all InventoryItems models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new InventoryItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['CHECKOUT_TRANSACTION_ID' => null]);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
//                    'data' => $data,
        ]);
    }

    public function actionPrintInventoryCode() {
        InventoryItems::printSku([34246,34242]);
    }
    public function actionPrintSkuById($id,$printInventoryCode) {
        // var_dump($printInventoryCode);die();
        InventoryItems::printSkuById($id,$printInventoryCode);
    }
    public function actionPrintSku($sku,$printInventoryCode = 0) {
        InventoryItems::printSku($sku,$printInventoryCode = 0);
    }

    public function actionIndex2() {
        $searchModel = new InventoryItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index2', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single InventoryItems model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
//return $this->redirect(['index']);
    }

    /**
     * Creates a new InventoryItems model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new InventoryItems();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', [
                    'type' => Growl::TYPE_SUCCESS,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-ok-sign',
                    'title' => ' Proses BERJAYA',
                    'message' => ' Data berjaya ditambah.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            } else {
                \Yii::$app->session->setFlash('error', [
                    'type' => Growl::TYPE_DANGER,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => ' Proses GAGAL.',
                    'message' => ' Data tidak berjaya ditambah.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['view', 'id' => $model->ID]);
//return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing InventoryItems model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', [
                    'type' => Growl::TYPE_SUCCESS,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-ok-sign',
                    'title' => ' Proses BERJAYA',
                    'message' => ' Data berjaya dikemaskini.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            } else {
                \Yii::$app->session->setFlash('error', [
                    'type' => Growl::TYPE_DANGER,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => ' Proses GAGAL.',
                    'message' => ' Data tidak berjaya dikemaskini.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['view', 'id' => $model->ID]);
//return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing InventoryItems model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
//            start operation
        $dbtransac = Yii::$app->db->beginTransaction();
        try {
            $model = $this->findInventoryItemInStore($id);
//        check if items still in store, otherwise abort process
            if (empty($model)) {
                Throw new Exception(' Item telah dikeluarkan dari stor / Item tiada dalam stor.');
            }
            $model->DELETED = '1';
//                error if item cannot be update
            if (!$model->save()) {
                Throw new Exception(' Data tidak berjaya dihapuskan.');
            }
//                error if related inventory quantity cannot be update
            if (!Inventories::updateQuantity($model->inventory->ID)) {
                Throw new Exception('');
            }
//                error if related checkin transaction quantity and total price cannot be update
            if (!InventoriesCheckIn::updateQuantityAndTotalPriceCheckIn($model->CHECKIN_TRANSACTION_ID)) {
                Throw new Exception('');
            }
//                commit operation
            $dbtransac->commit();
            Yii::$app->notify->success(' Data berjaya dihapuskan.');
        } catch (\Exception $e) {
//                rollback operation on failure
            $dbtransac->rollback();
            Yii::$app->notify->fail($e->getMessage());
        }
//        redirect to previous page
        return $this->redirect(\Yii::$app->request->referrer);
    }

    public function actionRecover($id) {
        $dbtransac = Yii::$app->db->beginTransaction();
        try {
            $model = $this->findInventoryItemInStore($id);
            if (empty($model)) {
                Throw new Exception(' Item telah dikeluarkan dari stor / Item tiada dalam stor.');
            }
            $model->DELETED = '0';
//                error if item cannot be update
            if (!$model->save()) {
                Throw new Exception(' Data tidak berjaya dikembalikan.');
            }
//                error if related inventory quantity cannot be update
            if (!Inventories::updateQuantity($model->inventory->ID)) {
                Throw new Exception('');
            }
//                error if related checkin transaction quantity and total price cannot be update
            if (!InventoriesCheckIn::updateQuantityAndTotalPriceCheckIn($model->CHECKIN_TRANSACTION_ID)) {
                Throw new Exception('');
            }
//                commit operation
            $dbtransac->commit();
            Yii::$app->notify->success(' Data berjaya dikembalikan.');
        } catch (\Exception $e) {
//                rollback operation on failure
            $dbtransac->rollback();
            Yii::$app->notify->fail($e->getMessage());
        }
//        redirect to previous page
        return $this->redirect(\Yii::$app->request->referrer);
    }

    public function actionDeletePermanent($id) {
        $dbtransac = Yii::$app->db->beginTransaction();
        try {
            $model = $this->findInventoryItemInStore($id);
            if (empty($model)) {
                Throw new Exception('  Data tidak berjaya dihapuskan. Item telah dikeluarkan dari stor.');
            }
//                error if item cannot be update
            if (!$model->delete()) {
                Throw new Exception(' Data tidak berjaya dihapuskan.');
            }
//            skip update on inventory and checkin model if data is already in deleted state
            if ($model->DELETED == 1) {
//                error if related inventory quantity cannot be update
                if (!Inventories::updateQuantity($model->inventory->ID)) {
                    Throw new Exception('');
                }
//                error if related checkin transaction quantity and total price cannot be update
                if (!InventoriesCheckIn::updateQuantityAndTotalPriceCheckIn($model->CHECKIN_TRANSACTION_ID)) {
                    Throw new Exception('');
                }
            }
//            remove checkin transaction and transaction if no items left to delete
//                commit operation
            $dbtransac->commit();
            Yii::$app->notify->success(' Data berjaya dihapuskan.');
        } catch (\Exception $e) {
//                rollback operation on failure
            $dbtransac->rollback();
            Yii::$app->notify->fail($e->getMessage());
        }
//        redirect to index page
//        return $this->redirect(['index']);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the InventoryItems model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return InventoryItems the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = InventoryItems::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findInventoryItemInStore($id) {
        if (($model = InventoryItems::findOne(['ID' => $id, 'CHECKOUT_TRANSACTION_ID' => null])) !== null) {
            return $model;
        } else {
            return null;
        }
    }

    protected function findInventoriesCheckin($id) {
        if (($model = InventoriesCheckIn::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findTransactions($id) {
        if (($model = Transactions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findInventory($id) {
        if (($model = Inventories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetQuantity($id) {
//        if (($model = Inventories::findOne($id)) !== null) {
//            return $model->QUANTITY;
//        } else {
//            throw new NotFoundHttpException('The requested page does not exist.');
//        }
        return $this->getInventoryQuantity($id);
    }

    protected function getInventoryQuantity($id) {
        $quantity = InventoryItems::find()
                ->joinWith('inventory')
                ->where(['INVENTORIES.ID' => $id])
                ->andWhere(['INVENTORY_ITEMS.DELETED' => 0])
                ->asArray()
                ->count();
        return $quantity;
    }

//    update checkin transaction data : under development
//    protected function getInventoriesCheckinQuantity($id) {
//        $quantity = InventoryItems::find()
//                ->joinWith('inventory')
//                ->where(['INVENTORIES_CHECKIN.ID' => $id])
//                ->andWhere(['INVENTORY_ITEMS.DELETED' => 0])
//                ->asArray()
//                ->count();
//        $price = InventoryItems::find()
////                ->select('INVENTORY_ITEMS.UNIT_PRICE')
//                ->joinWith('inventory')
//                ->where(['INVENTORY_ITEMS.CHECKIN_TRANSACTION_ID' => $id])
//                ->andWhere(['INVENTORY_ITEMS.DELETED' => 0])
//                ->asArray()
////                ->all();
//                ->sum('INVENTORY_ITEMS.UNIT_PRICE');
//        $params = ['price'=>$price,'quantity'=>$quantity];
//        var_dump($price);die();
//        var_dump($params);die();
//        return $params;
//    }

    protected function updateInventoryQuantity($id) {
        if (($model = Inventories::findOne($id)) !== null) {
            $quantity = $this->getInventoryQuantity($id);
            $model->QUANTITY = $quantity;
            $model->UPDATED_AT = date('d-M-y h.i.s a');
            $model->save(false);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function setNotification($type, $message) {
        if ($type == 'error') {
            $class = Growl::TYPE_DANGER;
            $icon = 'glyphicon glyphicon-remove-sign';
        } else if ($type == 'success') {
            $class = Growl::TYPE_SUCCESS;
            $icon = 'glyphicon glyphicon-ok-sign';
        }
        return \Yii::$app->session->setFlash($type, [
                    'type' => $class,
                    'duration' => 3000,
                    'icon' => $icon,
                    'title' => $title,
                    'message' => $message,
                    'positonY' => 'top',
                    'positonX' => 'right'
        ]);
    }

}
