<?php

namespace frontend\controllers;

use Yii;
use yii\base\Model;
use common\models\LaporanPenyelengaraanSisken;
use common\models\LaporanPenyelengaraanSiskenSearch;
use common\models\Transactions;
use common\models\TransactionsSearch;
use common\models\RequestSearch;
use common\models\Orders;
use common\models\OrdersSearch;
use common\models\OrderItems;
use common\models\OrderItemsSearch;
use common\models\InventoryItems;
use common\models\InventoryItemsSearch;
use common\models\Inventories;
use common\models\InventoriesSearch;
use common\models\Vendors;
use common\models\VendorsSearch;
use common\models\Package;
use common\models\PackageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\components\AccessRule;
use kartik\widgets\Alert;
use kartik\widgets\Growl;

/**
 * LaporanPenyelengaraanSiskenController implements the CRUD actions for LaporanPenyelengaraanSisken model.
 */
class LaporanPenyelengaraanSiskenController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-multiple' => ['POST'],
                    'recover' => ['POST'],
                    'delete-permanent' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),],
                'only' => ['create', 'update', 'index', 'view',
                    'delete', 'delete-permanent', 'recover'],
                'rules' => [
                    [
                        'actions' => ['delete-permanent'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'recover'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all LaporanPenyelengaraanSisken models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new LaporanPenyelengaraanSiskenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAttend($id) {
        $arahanKerja = $this->findModel($id);

        $inventories = \common\models\Inventories::find()->where(['DELETED' => 0])->andWhere(['not', ['QUANTITY' => 0]])->asArray()->all();
        $inventoriesArray = \yii\helpers\ArrayHelper::map($inventories, 'ID', function($model, $defaultValue) {
                    return $model['CODE_NO'] . ' - ' . $model['DESCRIPTION'] . ' {' . $model['QUANTITY'] . '}';
                });
        $vendors = \common\models\Vendors::find()->where(['DELETED' => 0])->asArray()->all();
        $vendorsArray = \yii\helpers\ArrayHelper::map($vendors, 'ID', 'NAME');
        $vehicles = \common\models\VehicleList::find()->where(['DELETED' => 0])->asArray()->all();
        $vehiclesArray = \yii\helpers\ArrayHelper::map($vehicles, 'ID', ['REG_NO'], 'TYPE');
//        $vehiclesArray = \yii\helpers\ArrayHelper::map($vehicles, 'REG_NO', 'REG_NO');
//        var_dump($vehiclesArray);die();
//        $usageList = ArrayHelper::map(VehicleList::find()->all(), 'REG_NO', ['REG_NO'], 'TYPE');
        $bilStock = Orders::find()
                ->where(['EXTRACT(MONTH FROM CREATED_AT)' => date('m')])
                ->andWhere(['EXTRACT(YEAR FROM CREATED_AT)' => date('Y')])
                ->max('CAST(SUBSTR(ORDER_NO,9) AS INT)');
        $bilStock = $bilStock + 1;
//        $model = new Stocks();
//        $modelItems = [new StockItems];

        $inventory = new Inventories();
        $transaction = new Transactions();
        $order = new Orders();
//        $order->VEHICLE_ID = $arahanKerja->ID_KENDERAAN;
        $orderItems = [new OrderItems];
//        var_dump($arahanKerja->ID_KENDERAAN);
//        die();

        if ($order->load(Yii::$app->request->post())) {
//        if ($order->load(Yii::$app->request->post()) && $orderItems->load(Yii::$app->request->post())) {
//            var_dump(Yii::$app->request->post());
//            die();

            $transaction->TYPE = 2;     //checkIn
            $transaction->CHECK_DATE = date('d-M-Y');
            $transaction->CHECK_BY = Yii::$app->user->id;
            // order link to trans
            $bilStock = Orders::find()
                    ->where(['EXTRACT(MONTH FROM CREATED_AT)' => date('m')])
                    ->andWhere(['EXTRACT(YEAR FROM CREATED_AT)' => date('Y')])
                    ->max('CAST(SUBSTR(ORDER_NO,9) AS INT)');
            $bilStock = $bilStock + 1;
            $order->ORDER_NO = date('Y/m') . '-' . $bilStock;
            $order->APPROVED = 2;
            $order->ARAHAN_KERJA_ID = $id;
//            $orderItems = Inventories::find()->
            $orderItems = \common\models\Model::createMultiple(OrderItems::classname());
            Model::loadMultiple($orderItems, Yii::$app->request->post());

            // validate all models
//            $valid = $transaction->validate();
            $valid = $order->validate();
            $valid = Model::validateMultiple($orderItems) && $valid;

            if ($valid) {
                $dbTransaction = \Yii::$app->db->beginTransaction();

                try {
                    if ($flag = $transaction->save(false)) {
                        $order->TRANSACTION_ID = $transaction->ID;
                        $order->save(false);
//                        if (!($flag = $order->save(false))) {
//                            $dbTransaction->rollBack();
//                            break;
//                        }
                        foreach ($orderItems as $orderItem) {
                            // orderItems link to order
                            // orderItems current bal

                            $inventory = Inventories::findOne($orderItem->INVENTORY_ID);

                            $orderItem->ORDER_ID = $order->ID;
                            $orderItem->CURRENT_BALANCE = $inventory->QUANTITY;
//                            var_dump($order->ID);die();
//                            $balance = EstorInventories::find()->where(['ID' => $modelItem->INVENTORY_ID])
//                                            ->select('QUANTITY')->asArray()->one();
//                            $modelItem->CURRENT_BALANCE = $balance['QUANTITY'];
                            if (!($flag = $orderItem->save(false))) {
                                $dbTransaction->rollBack();
                                break;
                            }
                        }
                    }

                    if ($flag) {
                        $dbTransaction->commit();
                        return $this->redirect(['index']);
                    }
                } catch (Exception $e) {
                    $dbTransaction->rollBack();
                }
            }
        }
        return $this->render('attend', [
                    'arahanKerja' => $this->findModel($id),
                    'transaction' => $transaction,
                    'vehiclesArray' => $vehiclesArray,
                    'inventoriesArray' => $inventoriesArray,
                    'order' => $order,
                    'bilStock' => $bilStock,
                    'orderItems' => (empty($orderItems)) ? [new OrderItems] : $orderItems,
        ]);
    }

    /**
     * Displays a single LaporanPenyelengaraanSisken model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
        //return $this->redirect(['index']);
    }
    public function actionView2($inst_no) {
        return $this->render('view', [
                    'model' => $this->findModelByNo($inst_no),
        ]);
        //return $this->redirect(['index']);
    }

    /**
     * Creates a new LaporanPenyelengaraanSisken model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new LaporanPenyelengaraanSisken();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', [
                    'type' => Growl::TYPE_SUCCESS,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-ok-sign',
                    'title' => ' Proses BERJAYA',
                    'message' => ' Data berjaya ditambah.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            } else {
                \Yii::$app->session->setFlash('error', [
                    'type' => Growl::TYPE_DANGER,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => ' Proses GAGAL.',
                    'message' => ' Data tidak berjaya ditambah.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['view', 'id' => $model->ID]);
            //return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LaporanPenyelengaraanSisken model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', [
                    'type' => Growl::TYPE_SUCCESS,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-ok-sign',
                    'title' => ' Proses BERJAYA',
                    'message' => ' Data berjaya dikemaskini.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            } else {
                \Yii::$app->session->setFlash('error', [
                    'type' => Growl::TYPE_DANGER,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => ' Proses GAGAL.',
                    'message' => ' Data tidak berjaya dikemaskini.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['view', 'id' => $model->ID]);
            //return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LaporanPenyelengaraanSisken model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->DELETED = '1';
        if ($model->save()) {
            \Yii::$app->session->setFlash('success', [
                'type' => Growl::TYPE_SUCCESS,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-ok-sign',
                'title' => ' Proses BERJAYA',
                'message' => ' Data berjaya dihapuskan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        } else {
            \Yii::$app->session->setFlash('error', [
                'type' => Growl::TYPE_DANGER,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-remove-sign',
                'title' => ' Proses GAGAL.',
                'message' => ' Data tidak berjaya dihapuskan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        }

        return $this->redirect(['index']);
    }

    public function actionRecover($id) {
        $model = $this->findModel($id);
        $model->DELETED = '0';
        if ($model->save()) {
            \Yii::$app->session->setFlash('success', [
                'type' => Growl::TYPE_SUCCESS,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-ok-sign',
                'title' => ' Proses BERJAYA',
                'message' => ' Data berjaya dikembalikan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        } else {
            \Yii::$app->session->setFlash('error', [
                'type' => Growl::TYPE_DANGER,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-remove-sign',
                'title' => ' Proses GAGAL.',
                'message' => ' Data tidak berjaya dikembalikan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        }
        return $this->redirect(['index']);
    }

    public function actionDeletePermanent($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LaporanPenyelengaraanSisken model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return LaporanPenyelengaraanSisken the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = LaporanPenyelengaraanSisken::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelByNo($inst_no) {
        if (($model = LaporanPenyelengaraanSisken::find()->where(['NO_KERJA' => $inst_no])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
