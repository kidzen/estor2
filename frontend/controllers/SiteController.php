<?php

namespace frontend\controllers;

use Yii;
use common\models\Inventories;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
//use yii\web;
use kartik\widgets\Alert;
use kartik\widgets\Growl;
use common\components\AccessRule;
use kartik\mpdf\Pdf;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),],
                    'only' => ['logout', 'login', 'signup', 'create','index'],
                    'rules' => [
                        [
                            'actions' => ['login', 'signup'],
                            'allow' => true,
                            'roles' => ['?'],
                        ],
                        [
                            'actions' => ['logout','index'],
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                        [
                            'actions' => ['create'],
                            'allow' => true,
                            'roles' => ['Administrator'],
//                        'roles' => ['?'],
                        ],
                    ],
                ],
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
//                    'logout' => ['post'],
                    ],
                ],
            ];
        }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex2() {

//migration
        $commandR = '';
        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach (array_filter(array_map('trim', explode(';', $commandR))) as $query) {
                $command = Yii::$app->db->createCommand($query);
                $command->execute();
            }

            $transaction->commit();
            unset($command);
            \Yii::$app->session->setFlash('succes', [
                'type' => \kartik\widgets\Growl::TYPE_SUCCESS,
                'duration' => 5000,
                'icon' => 'glyphicon glyphicon-ok-sign',
                'title' => 'Berjaya',
                'message' => " Proses migrasi berjaya!",
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        } catch (\yii\db\Exception $e) {
            $transaction->commit();
            throw new \yii\web\HttpException(500, $e->getMessage());
            \Yii::$app->session->setFlash('fail', [
                'type' => \kartik\widgets\Growl::TYPE_DANGER,
                'duration' => 5000,
                'icon' => 'glyphicon glyphicon-remove-sign',
                'title' => 'Gagal',
                'message' => " Proses migrasi gagal!" . '<br>Error Code: <br>' . $e->getMessage(),
//                'message' => " Proses migrasi gagal!".'<br>Error Code: <br>'.$e->errorInfo(),
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
//            var_dump($e->getCode().'<br>'.$e->getMessage());
//            die();
        }
//endmigration

        $estorTables = \common\models\EstorTables::find()->select('TABLE_NAME');
        $estorViews = \common\models\EstorViews::find()->select('VIEW_NAME');
        $estorTriggers = \common\models\EstorTriggers::find()->select('TRIGGER_NAME');

// add conditions that should always apply here

        $dataTables = new ActiveDataProvider([
            'query' => $estorTables,
        ]);

        $dataViews = new ActiveDataProvider([
            'query' => $estorViews,
        ]);
        $dataTriggers = new ActiveDataProvider([
            'query' => $estorTriggers,
        ]);


        return $this->render('index', [
            'dataTables' => $dataTables,
            'dataViews' => $dataViews,
            'dataTriggers' => $dataTriggers,
        ]);
    }

    public function actionMaintenance() {
        // echo '<pre>';
        $data = \common\models\maintenance\AllMaintenanceCheck::find()->all();

        if (empty($data)) {
            Yii::$app->notify->success(' Sistem dalam keadaan baik.');
            return $this->redirect(Yii::$app->request->referrer);
        }
        try {
            $dbtransac = Yii::$app->db->beginTransaction();
            foreach ($data as $i => $dat) {
                var_dump($dat->attributes) . '<br>';
                if ($dat->TYPE == 'text') {
                    $updateStatement = Yii::$app->db->createCommand('UPDATE ' . $dat->TABLE_NAME . ' SET ' . $dat->COL_NAME . ' = \'' . $dat->TRUE_VALUE . '\' WHERE ID = ' . $dat->DATA_ID);
                } else {
                    $updateStatement = Yii::$app->db->createCommand('UPDATE ' . $dat->TABLE_NAME . ' SET ' . $dat->COL_NAME . ' = ' . $dat->TRUE_VALUE . ' WHERE ID = ' . $dat->DATA_ID);
                }
                $success = $updateStatement->execute();
                if (!$success) {
                    throw new Exception(' Sistem gagal dikemaskini.');
                }
//                 Yii::$app->session->setFlash($dat->ID, [
//                     'type' => 'success',
//                     'duration' => 5000,
//                     'icon' => 'glyphicon glyphicon-check-sign',
//                     'title' => ' BERJAYA.',
// //                    'message' => ' Kuantiti permohonan yang disahkan telah diselenggara. <br>' . count($orderItems) . ' data pesanan bermasalah berjaya dikemaskini.',
//                     'message' => $dat->TABLE_NAME . ' berjaya dikemaskini. ' . $dat->COL_NAME . ' dikemaskini dari ' . $dat->FAULT_VALUE . ' ke ' . $dat->TRUE_VALUE,
//                 ]);

                \Yii::$app->notify->success($dat->TABLE_NAME . ' berjaya dikemaskini. ' . $dat->COL_NAME . ' dikemaskini dari ' . $dat->FAULT_VALUE . ' ke ' . $dat->TRUE_VALUE);
                $dbtransac->commit();
            }
        } catch (\Exception $e) {
            // die();
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionMaintenance2() {

//check orderItems app_quantity ::already done in model,call models static function
        $orderItems = \common\models\OrderItemsAppQuantityCheck::find()->all();
//check inventory quantity
        $inventories = \common\models\InventoryQuantityCheck::find()->all();
//check orders
        $orders = \common\models\Orders::find()->select('ID,ARAHAN_KERJA_ID,APPROVED,DELETED')
        ->where(['not', ['ARAHAN_KERJA_ID' => null]])
        ->andWhere("DELETED = 1 or APPROVED = 8")
        ->asArray()->all();
//check order_no
        if (Yii::$app->db->driverName == 'oci') {
            $check_order_no = Yii::$app->db->createCommand(
                "select count(*) count from ("
                . "select "
                . "id,concat(to_char(created_at,'yyyy/mm'),concat('-',rank() OVER (partition by to_char(created_at,'yyyy/mm') ORDER BY created_at))) new_order_no"
                . "    ,order_no"
                . "  from orders t1"
                . ") where order_no != new_order_no"
            )->queryOne();
        } else {
            // $check_order_no = Yii::$app->db->createCommand(
            //     "select count(*) count from ("
            //     . "select "
            //     . "id,concat(to_char(created_at,'yyyy/mm'),concat('-',rank() OVER (partition by to_char(created_at,'yyyy/mm') ORDER BY created_at))) new_order_no"
            //     . "    ,order_no"
            //     . "  from orders t1"
            //     . ") where order_no != new_order_no"
            //     )->queryOne();
            $check_order_no = 0;
        }
//check maintenace check
        $mc_check = Yii::$app->db->createCommand(
            "select count(*) count from maintenance_check"
        )->queryOne();
//if all no errors
        If (empty($orderItems) && empty($inventories) && empty($orders) && $check_order_no['COUNT'] == 0) {
            Yii::$app->notify->success(' Sistem dalam keadaan baik.');
            return $this->redirect(Yii::$app->request->referrer);
        }
        try {
            if (Yii::$app->db->driverName == 'oci') {

                $flag = true;
//fix orderItem app_quantity
                if ($orderItems) {
                    $dbtransac = Yii::$app->db->beginTransaction();
                    foreach ($orderItems as $i => $orderItem) {
                        $flag = $flag && \common\models\OrderItems::updateAppQuantity($orderItem->ID);
                        if (!$flag) {
                            throw new Exception(' Proses selenggara sistem gagal. <br> Kuantiti permohonan yang disahkan gagal dikemaskini.');
                        }
                    }
                    $dbtransac->commit();
                    Yii::$app->session->setFlash('orderItems', [
                        'type' => 'success',
                        'duration' => 5000,
                        'icon' => 'glyphicon glyphicon-check-sign',
                        'title' => ' BERJAYA.',
                        'message' => ' Kuantiti permohonan yang disahkan telah diselenggara. <br>' . count($orderItems) . ' data pesanan bermasalah berjaya dikemaskini.',
                    ]);
                }
//fix inventories quantity
                if ($inventories) {
                    $dbtransac = Yii::$app->db->beginTransaction();
                    foreach ($inventories as $j => $inventory) {
                        $flag = $flag && Inventories::updateQuantity($inventory->ID);
                    }
                    if (!$flag) {
                        throw new Exception(' Proses selenggara sistem gagal. <br> Kuantiti inventori gagal dikemaskini.');
                    } else {
                        Yii::$app->session->setFlash('inventories', [
                            'type' => 'success',
                            'duration' => 5000,
                            'icon' => 'glyphicon glyphicon-check-sign',
                            'title' => ' BERJAYA.',
                            'message' => ' Kuantiti inventori telah diselenggara. <br>' . count($inventories) . ' data inventori bermasalah berjaya dikemaskini.',
                        ]);
                        $dbtransac->commit();
                    }
                }
//fix orders arahan_kerja
                if ($orders) {
                    $dbtransac = Yii::$app->db->beginTransaction();
                    foreach ($orders as $k => $order) {
                        $modelOrder = \common\models\Orders::findOne($order['ID']);
                        $modelOrder->ARAHAN_KERJA_ID = null;
                        $flag = $flag && $modelOrder->save(false);
                    }
                    if (!$flag) {
                        throw new Exception(' Proses selenggara sistem gagal. <br> Arahan kerja gagal dikemaskini.');
                    } else {
                        Yii::$app->session->setFlash('arahanKerja', [
                            'type' => 'success',
                            'duration' => 5000,
                            'icon' => 'glyphicon glyphicon-check-sign',
                            'title' => ' BERJAYA.',
                            'message' => ' Arahan kerja telah diselenggara. <br>' . count($orders) . ' data arahan kerja bermasalah berjaya dikemaskini.',
                        ]);
                        $dbtransac->commit();
                    }
                }
//fix orders order_no
                if ($check_order_no['COUNT'] > 0) {
                    $dbtransac = Yii::$app->db->beginTransaction();
                    $update_order_no = Yii::$app->db->createCommand(
                        "update orders o1 set order_no = ("
                        . "select new_order_no from ("
                        . "select id,concat(to_char(created_at,'yyyy/mm'),concat('-',rank() OVER (partition by to_char(created_at,'yyyy/mm') ORDER BY created_at))) new_order_no from orders o2"
                        . ") o3 "
                        . "where o3.id = o1.id"
                        . ")"
                    );
                    $flag = $update_order_no->execute();

                    if (!$flag) {
                        throw new Exception(' Proses selenggara sistem gagal. <br> No Order Permohonan Keluar gagal dikemaskini.');
                    } else {
                        Yii::$app->session->setFlash('ordersNoOrder', [
                            'type' => 'success',
                            'duration' => 5000,
                            'icon' => 'glyphicon glyphicon-check-sign',
                            'title' => ' BERJAYA.',
                            'message' => ' No Order Permohonan Keluar telah diselenggara. <br>' . $check_order_no['COUNT'] . ' data Permohonan Keluar bermasalah berjaya dikemaskini.',
                        ]);
                        $dbtransac->commit();
                    }
                }
//fix order_items unit_price
                if ($mc_check['COUNT'] > 0) {
                    $dbtransac = Yii::$app->db->beginTransaction();
                    $update_oi_unit_price = Yii::$app->db->createCommand(
                        "update order_items oi set UNIT_PRICE = ("
                        . " select distinct ii_batch_price from maintenance_check mc where mc.oi_id = oi.id "
                        . " )"
                        . " where oi.id in (select distinct oi_id from maintenance_check)"
                    );
                    $flag = $update_oi_unit_price->execute();
                    if (!$flag) {
                        throw new Exception(' Proses selenggara sistem gagal. <br> Jumlah Harga Permohonan Keluar gagal dikemaskini.');
                    } else {
                        Yii::$app->session->setFlash('ordersUnitPrice', [
                            'type' => 'success',
                            'duration' => 5000,
                            'icon' => 'glyphicon glyphicon-check-sign',
                            'title' => ' BERJAYA.',
                            'message' => ' Jumlah Harga Permohonan Keluar telah diselenggara. <br>' . $mc_check['COUNT'] . ' Data Permohonan Keluar bermasalah berjaya dikemaskini.',
                        ]);
                        $dbtransac->commit();
                    }
                }
            }
        } catch (Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionClearCache() {
        \Yii::$app->cache->flush();
        return $this->redirect('index');
    }

    public function actionTutor() {
//        return $this->renderAjax('_tutor');
        return $this->renderPartial('_tutor');
//        return $this->render('tutor');
    }

//    public function actionSync() {
//        Yii::$app->cache->flush();
//        return $this->redirect('index');
//    }

    public function actionTutorPdf() {
        $content = $this->renderPartial('_tutor');
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'content' => $content,
            'options' => [
                'title' => 'MPSP Estor Tutuorial',
//                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'methods' => [
                'SetHeader' => ['Generated By: MPSP Estor Application||Generated On: ' . date("r")],
                'SetFooter' => ['|Page {PAGENO}|'],
            ]
        ]);
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        return $pdf->render();
    }

    public function actionIndex() {
        if (!\Yii::$app->user->isGuest) {
            if (Yii::$app->user->Name == null) {
                \Yii::$app->session->setFlash('profile update', [
                    'type' => Growl::TYPE_DANGER,
                    'duration' => 5000,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => 'Profil',
                    'message' => " Sila kemaskini profil anda di " . \yii\helpers\Html::a('sini', ['people/update', 'id' => Yii::$app->user->id]),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
        }
        $limit = Inventories::find()
        ->where('QUANTITY < MIN_STOCK')
        ->andWhere('DELETED = 0')
        ->count();
        $warning = Inventories::find()
        ->where('QUANTITY <= MIN_STOCK + 5')
        ->andWhere('DELETED = 0')
        ->count();

        \Yii::$app->notify->fail(" $limit stok berada dibawah paras minimum.");
        \Yii::$app->notify->warning(" $warning stok menghampiri paras minimum.");
        // var_dump($limit);
        // var_dump($warning);
        // die;

        $limitList = Inventories::find()
        ->where('QUANTITY < MIN_STOCK')
        ->andWhere('DELETED = 0')
        ->asArray()->all();
        // foreach ($limitList as $key => $value) {
        //     $hasItems = \common\models\InventoryItems::find()
        //         ->joinWith('inventory')->where(['INVENTORIES.ID' => $value['ID']])
        //         ->count();

        //     var_dump($hasItems);
        // }
        // die;
        // var_dump($limitList);die;
        $warningList = Inventories::find()
        ->where('QUANTITY <= MIN_STOCK + 5')
        ->andwhere('QUANTITY >= MIN_STOCK')
        ->asArray()->all();
//        $currentYear = date('Y');
        $currentMonth = date('m-y');
//        var_dump($currentMonth);die();
        if (Yii::$app->db->driverName == 'oci') {
            $quantityStockIn = \common\models\InventoryItems::find()->where(['TO_CHAR(created_at,\'mm-YY\')' => $currentMonth])->andWhere(['DELETED' => 0])->count();
            $quantityStockOut = \common\models\InventoryItems::find()->where(['TO_CHAR(updated_at,\'mm-YY\')' => $currentMonth])->andWhere(['not', ['CHECKOUT_TRANSACTION_ID' => null]])->andWhere(['DELETED' => 0])->count();
            ;
            $quantityStockRequest = \common\models\Orders::find()->where(['TO_CHAR(created_at,\'mm-YY\')' => $currentMonth])->andWhere(['DELETED' => 0])->count();
            $quantityInventoriesCardRegistered = \common\models\Inventories::find()->where(['TO_CHAR(created_at,\'mm-YY\')' => $currentMonth])->andWhere(['DELETED' => 0])->count();

            $latestOrder = new \yii\data\ActiveDataProvider([
                'query' => \common\models\OrderItems::find()->limit(5)->orderBy(['ORDER_ITEMS.CREATED_AT' => SORT_DESC]),
                'pagination' => false,
            ]);
            $latestStockIn = new \yii\data\ActiveDataProvider([
                'query' => \common\models\InventoriesCheckIn::find()->limit(5)->orderBy(['CREATED_AT' => SORT_DESC]),
                'pagination' => false,
            ]);
            $pendingApproval = new \yii\data\ActiveDataProvider([
                'query' => \common\models\OrderItems::find()->joinWith('order')->where(['ORDERS.APPROVED' => 2,'ORDERS.DELETED'=>0])->limit(5)->orderBy(['ORDERS.APPROVED_AT' => SORT_DESC]),
                'pagination' => false,
            ]);
            // var_dump($pendingApproval->models);die();
        } else {
            $quantityStockIn = \common\models\InventoryItems::find()->where(['date_format(created_at,\'%m-%y\')' => $currentMonth])->andWhere(['DELETED' => 0])->count();
            $quantityStockOut = \common\models\InventoryItems::find()->where(['date_format(updated_at,\'%m-%y\')' => $currentMonth])->andWhere(['not', ['CHECKOUT_TRANSACTION_ID' => null]])->andWhere(['DELETED' => 0])->count();
            ;
            $quantityStockRequest = \common\models\Orders::find()->where(['date_format(created_at,\'%m-%y\')' => $currentMonth])->andWhere(['DELETED' => 0])->count();
            $quantityInventoriesCardRegistered = \common\models\Inventories::find()->where(['date_format(created_at,\'%m-%y\')' => $currentMonth])->andWhere(['DELETED' => 0])->count();

            $latestOrder = new \yii\data\ActiveDataProvider([
                'query' => \common\models\OrderItems::find()->limit(5)
                ->orderBy(['ORDER_ITEMS.CREATED_AT' => SORT_DESC]),
                'pagination' => false,
            ]);
            $latestStockIn = new \yii\data\ActiveDataProvider([
                'query' => \common\models\InventoriesCheckIn::find()->limit(5)
                ->orderBy(['CREATED_AT' => SORT_DESC]),
                'pagination' => false,
            ]);
            $pendingApproval = new \yii\data\ActiveDataProvider([
                'query' => \common\models\OrderItems::find()->where(['order.APPROVED' => 2])->limit(5)->orderBy(['CREATED_AT' => SORT_DESC]),
                'pagination' => false,
            ]);
        }
//        var_dump($latestStockIn->models);die();
//        var_dump($latestStockIn->models);die();
//        $quantityStockIn = \common\models\EstorItems::find()
//                        ->where(['extract(month from CHECKIN_DATE)' => date('m')])
//                        ->asArray()->count();
//        $quantityStockOut = \common\models\EstorItems::find()
//                        ->where(['extract(month from CHECKOUT_DATE)' => date('m')])
//                        ->asArray()->count();
//        $quantityStockRequest = \common\models\StockItems::find()
//                        ->where(['extract(month from CREATED_DATE)' => date('m')])
//                        ->asArray()->count();
//        $quantityInventoriesCardRegistered = \common\models\EstorInventories::find()
//                        ->where(['extract(month from APPROVED_DATE)' => date('m')])
//                        ->asArray()->count();
//        $latestOrder = new \yii\data\ActiveDataProvider([
//            'query' => \common\models\Stocks::find()->limit(5),
//            'pagination' => false,
////            'pagination' => ['pageSize'=>2],
//        ]);
//        $latestStockIn = new \yii\data\ActiveDataProvider([
//            'query' => \common\models\EstorItems::find()->limit(4),
//            'pagination' => false,
//        ]);
        return $this->render('index', [
            'limitList' => $limitList,
            'warningList' => $warningList,
            'quantityStockIn' => $quantityStockIn,
            'quantityStockOut' => $quantityStockOut,
            'quantityStockRequest' => $quantityStockRequest,
            'quantityInventoriesCardRegistered' => $quantityInventoriesCardRegistered,
            'latestOrder' => $latestOrder,
            'latestStockIn' => $latestStockIn,
            'pendingApproval' => $pendingApproval,
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin() {
        $this->layout = '//main-login';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm(['scenario' => LoginForm::SCENARIO_LOGIN_WITH_STAFF_NO]);
//        $model->login_with_username = 0;
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
//            \common\models\ActivityLogs::add('Berjaya', ['LOG MASUK.']);
            return $this->goBack();
        } else if ($model->load(Yii::$app->request->post()) && !$model->login()) {
//            \common\models\ActivityLogs::add('Amaran', ['LOG MASUK', serialize($model->errors),]);
            return $this->render('login', [
                'model' => $model,
            ]);
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLoginUsername() {
        $this->layout = '//main-login';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm(['scenario' => LoginForm::SCENARIO_LOGIN_WITH_USERNAME]);
//        $model->login_with_username = 1;
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            \common\models\ActivityLogs::add('Berjaya', ['LOG MASUK.']);
            return $this->goBack();
        } else if ($model->load(Yii::$app->request->post()) && !$model->login()) {
            \common\models\ActivityLogs::add('Amaran', ['LOG MASUK', serialize($model->errors),]);
            return $this->render('login', [
                'model' => $model,
            ]);
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout() {
        \common\models\ActivityLogs::add('Berjaya', ['LOG KELUAR.']);
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout() {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup() {
        $this->layout = '//main-login';
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {

                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionCreate() {
        $model = new People();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset() {
        $this->layout = '//main-login';
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
//            if (1) {
                \Yii::$app->session->setFlash('success', [
                    'type' => Growl::TYPE_SUCCESS,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => ' Permohonan penukaran kata laluan BERJAYA.',
                    'message' => ' Sila periksa email anda.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
//                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                \Yii::$app->session->setFlash('error', [
                    'type' => Growl::TYPE_DANGER,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => ' Permohonan penukaran kata laluan GAGAL.',
                    'message' => ' Kata laluan tidak berjaya dihantar.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
//                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
//            Yii::$app->session->setFlash('success', 'New password was saved.');
            \Yii::$app->session->setFlash('success', [
                'type' => Growl::TYPE_SUCCESS,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-remove-sign',
                'title' => ' Kata laluan BERJAYA diubah.',
                'message' => ' Sila periksa email anda.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionPatch($type, $version) {
        if ($type == 'schema' || $type == 'data' && $version) {
            $fileName = $type . "-" . $version . ".sql";
            $myfile = fopen($fileName, "r") or die("Unable to open file!");
            $commandR = fread($myfile, filesize($fileName));
            fclose($myfile);
        } else {
            return $this->redirect('index');
        }
        $sql = array_filter(array_map('trim', explode(';', $commandR)));
//        for ($i = 0; $i < 2; $i++) {
        foreach ($sql as $i => $sqls) {
//            $command = Yii::$app->db->createCommand(@$sql[$i]);
            $command = Yii::$app->db->createCommand(@$sqls[$i]);
            var_dump($sql[$i]);
            if ($command->execute()) {
                echo 'success';
            } else {
                echo 'fails';
            }
        }
        die();
        return $this->redirect('index');
//        $transaction = Yii::$app->db->beginTransaction();
//        try {
////            var_dump(array_filter(array_map('trim', explode(';', $commandR))));die();
////            foreach (array_filter(array_map('trim', explode(';', $commandR))) as $query) {
//////                $command = Yii::$app->db->createCommand($commandR);
////                $command = Yii::$app->db->createCommand($query);
////                $command->execute();
////            }
//            $sql = array_filter(array_map('trim', explode(';', $commandR)));
////            var_dump($sql);
////            die();
//            for ($i = 1; $i < 75; $i++) {
//                $command = Yii::$app->db->createCommand($sql[$i]);
////                var_dump($sql[$i]);
//                $command->execute();
//            }
//            $transaction->commit();
//            unset($command);
////            die();
//            \Yii::$app->session->setFlash('succes', [
//                'type' => \kartik\widgets\Growl::TYPE_SUCCESS,
//                'duration' => 5000,
//                'icon' => 'glyphicon glyphicon-ok-sign',
//                'title' => 'Berjaya',
//                'message' => " Proses migrasi berjaya!",
//                'positonY' => 'top',
//                'positonX' => 'right'
//            ]);
//            return $this->redirect('index');
//        } catch (\yii\db\Exception $e) {
//            $transaction->rollback();
//            throw new \yii\web\HttpException(500, $e->getMessage());
//            \Yii::$app->session->setFlash('fail', [
//                'type' => \kartik\widgets\Growl::TYPE_DANGER,
//                'duration' => 5000,
//                'icon' => 'glyphicon glyphicon-remove-sign',
//                'title' => 'Gagal',
//                'message' => " Proses migrasi gagal!" . '<br>Error Code: <br>' . $e->getMessage(),
////                'message' => " Proses migrasi gagal!".'<br>Error Code: <br>'.$e->errorInfo(),
//                'positonY' => 'top',
//                'positonX' => 'right'
//            ]);
//            return $this->redirect('index');
//
////            var_dump($e->getCode().'<br>'.$e->getMessage());
////            die();
//        }

        $estorTables = \common\models\EstorTables::find()->select('TABLE_NAME')->where(['TABLE_NAME' => 'migration']);
        $estorViews = \common\models\EstorViews::find()->select('VIEW_NAME');
        $estorTriggers = \common\models\EstorTriggers::find()->select('TRIGGER_NAME');

// add conditions that should always apply here

        $dataTables = new ActiveDataProvider([
            'query' => $estorTables,
        ]);

        $dataViews = new ActiveDataProvider([
            'query' => $estorViews,
        ]);
        $dataTriggers = new ActiveDataProvider([
            'query' => $estorTriggers,
        ]);


        return $this->render('migrate', [
            'dataTables' => $dataTables,
            'dataViews' => $dataViews,
            'dataTriggers' => $dataTriggers,
        ]);

        return $this->render('patch', [
            'model' => $model,
        ]);
    }

//    public function actionCheckPatch($pass) {
    public function actionCheckPatch() {

        $estorTables = \common\models\EstorTables::find()->select('TABLE_NAME');
        $estorViews = \common\models\EstorViews::find()->select('VIEW_NAME');
        $estorTriggers = \common\models\EstorTriggers::find()->select('TRIGGER_NAME');

// add conditions that should always apply here

        $dataTables = new ActiveDataProvider([
            'query' => $estorTables,
        ]);

        $dataViews = new ActiveDataProvider([
            'query' => $estorViews,
        ]);
        $dataTriggers = new ActiveDataProvider([
            'query' => $estorTriggers,
        ]);


        return $this->render('check-patch', [
            'dataTables' => $dataTables,
            'dataViews' => $dataViews,
            'dataTriggers' => $dataTriggers,
        ]);
//        } else {
//            return $this->redirect(['index']);
//        }
    }

    protected function getInventoryQuantity($id) {
        $quantity = \common\models\InventoryItems::find()
        ->joinWith('inventory')
        ->where(['INVENTORIES.ID' => $id])
        ->andWhere(['CHECKOUT_TRANSACTION_ID' => null])
        ->andWhere(['INVENTORY_ITEMS.DELETED' => 0])
        ->asArray()
        ->count();
        return $quantity;
    }

    protected function updateInventoryQuantity($id) {
        if (($model = Inventories::findOne($id)) !== null) {
            $quantity = $this->getInventoryQuantity($id);
            $model->QUANTITY = $quantity;
            if ($model->save(false))
                return true;
            else {
                Yii::$app->notify->fail(' Kuantiti inventori tidak berjaya dikemaskini.');
            }
        }
    }

}
