<?php

namespace frontend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use common\models\Categories;
use common\models\CategoriesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\components\AccessRule;

class DatabaseController extends \yii\web\Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'recover' => ['POST'],
                    'delete-permanent' => ['POST'],
                    'delete-mutliple' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),],
                'only' => ['create', 'update', 'index', 'view',
                    'delete', 'delete-permanent', 'recover', 'db-version'],
                'rules' => [
                    [
                        'actions' => ['index', 'db-version'],
                        'allow' => true,
                        'roles' => ['@'],
//                        'roles' => ['Administrator'],
                    ],
                    [
                        'actions' => ['delete-permanent'],
                        'allow' => true,
                        'roles' => ['Administrator'],
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'recover'],
                        'allow' => true,
                        'roles' => ['Pegawai Stor', 'Administrator'],
                    ],
                ],
            ],
        ];
    }

    public function actionTest() {

        $commandR = '
            select
            cons.CONSTRAINT_NAME, cons.CONSTRAINT_TYPE, cons.TABLE_NAME, cons.STATUS, cons.INDEX_NAME
            from all_constraints cons
            where cons.owner = \'ESTOR5\'
            and cons.INDEX_NAME like \'%PK\'

        ';
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => $commandR,
        ]);
        var_dump($dataProvider->getModels());
        die();
    }

    public function actionIndex() {

        $estorTables = \common\models\EstorTables::find()->select('TABLE_NAME');
        $estorViews = \common\models\EstorViews::find()->select('VIEW_NAME');
        $estorTriggers = \common\models\EstorTriggers::find()->select('TRIGGER_NAME');

// add conditions that should always apply here

        $dataTables = new ActiveDataProvider([
            'query' => $estorTables,
        ]);

        $dataViews = new ActiveDataProvider([
            'query' => $estorViews,
        ]);
        $dataTriggers = new ActiveDataProvider([
            'query' => $estorTriggers,
        ]);


        return $this->render('index', [
                    'dataTables' => $dataTables,
                    'dataViews' => $dataViews,
                    'dataTriggers' => $dataTriggers,
        ]);
    }

    public function actionIndex2() {

//migration
        $commandR = <<<SQL

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."KEWPS_10" ("ORDER_ID", "ORDER_REQUIRED_DATE", "ORDER_ITEM_ID", "ORDER_NO", "ORDERED_BY", "ORDER_DATE", "TRANSACTION_ID", "CARD_NO", "CODE_NO", "DESCRIPTION", "RQ_QUANTITY", "APP_QUANTITY", "CURRENT_BALANCE", "UNIT_PRICE", "AVERAGE_UNIT_PRICE", "BATCH_TOTAL_PRICE", "CREATED_DATE", "CREATED_BY", "APPROVED_BY", "APPROVED_DATE") AS
  with data1 as (
select distinct
--select
--  rownum id
  o.id order_id
  ,ii.id
  ,o.required_date order_required_date
  ,oi.id order_item_id
  , o.order_no
  , o.ORDERED_BY
  , o.ORDER_DATE
  ,ta.id transaction_id
  , i.card_no
  , i.code_no
  , i.description
  , oi.rq_quantity
--  , count(*) over (partition by oi.id,ii.unit_price) app_quantity
  , oi.CURRENT_BALANCE
  , ii.UNIT_PRICE unit_price
--  , avg(ii.UNIT_PRICE) over (partition by oi.id,ii.unit_price) average_unit_price
--  , sum(ii.UNIT_PRICE) over (partition by oi.id,ii.unit_price) batch_total_price
  , trunc(oi.CREATED_AT) created_date
  , oi.CREATED_BY
  , o.approved_BY
  , trunc(o.approved_at) approved_date
from INVENTORY_ITEMS ii
left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
left join INVENTORIES i on i.id = ic.INVENTORY_ID
left join ORDER_ITEMS oi on oi.id = ii.checkout_transaction_id
left join ORDERS o on o.id = oi.order_ID
left join transactions t on t.id = o.TRANSACTION_ID
left join transactions_all ta on ta.id = t.id
where ii.checkout_transaction_id  is not null
-- and order_id = 1174
)
select distinct
  order_id
  , order_required_date
  , order_item_id
  , order_no
  , ORDERED_BY
  , ORDER_DATE
  , transaction_id
  , card_no
  , code_no
  , description
  , rq_quantity
  , count(*) over (partition by order_item_id,unit_price) app_quantity
  , CURRENT_BALANCE
  , unit_price
  , avg(UNIT_PRICE) over (partition by order_item_id,unit_price) average_unit_price
  , sum(UNIT_PRICE) over (partition by order_item_id,unit_price) batch_total_price
  , created_date
  , CREATED_BY
  , approved_BY
  , approved_date
from data1;
SQL;
        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach (array_filter(array_map('trim', explode(';', $commandR))) as $query) {
//                $command = Yii::$app->db->createCommand($commandR);
                $command = Yii::$app->db->createCommand($query);
                $command->execute();
            }
//            $command = Yii::$app->db->createCommand("@".'"BEGIN '.$commandR.'END;"');
//$commandR2 = '  ';
//            $command = Yii::$app->db->createCommand($commandR2);
//            $commandR = Yii::$app->db->createCommand($commandR);
//            $command = Yii::$app->db->createCommand(@"
//                begin
//                DROP TABLE ACTIVITY_LOGS cascade constraints;
//                DROP TABLE AIRCOND_LIST cascade constraints;end;");
//            $command->execute();

            $transaction->commit();
            unset($command);
            \Yii::$app->session->setFlash('succes', [
                'type' => \kartik\widgets\Growl::TYPE_SUCCESS,
                'duration' => 5000,
                'icon' => 'glyphicon glyphicon-ok-sign',
                'title' => 'Berjaya',
                'message' => " Proses migrasi berjaya!",
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        } catch (\yii\db\Exception $e) {
            $transaction->commit();
            throw new \yii\web\HttpException(500, $e->getMessage());
            \Yii::$app->session->setFlash('fail', [
                'type' => \kartik\widgets\Growl::TYPE_DANGER,
                'duration' => 5000,
                'icon' => 'glyphicon glyphicon-remove-sign',
                'title' => 'Gagal',
                'message' => " Proses migrasi gagal!" . '<br>Error Code: <br>' . $e->getMessage(),
//                'message' => " Proses migrasi gagal!".'<br>Error Code: <br>'.$e->errorInfo(),
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
//            var_dump($e->getCode().'<br>'.$e->getMessage());
            die();
        }
//endmigration

        $estorTables = \common\models\EstorTables::find()->select('TABLE_NAME');
        $estorViews = \common\models\EstorViews::find()->select('VIEW_NAME');
        $estorTriggers = \common\models\EstorTriggers::find()->select('TRIGGER_NAME');

// add conditions that should always apply here

        $dataTables = new ActiveDataProvider([
            'query' => $estorTables,
        ]);

        $dataViews = new ActiveDataProvider([
            'query' => $estorViews,
        ]);
        $dataTriggers = new ActiveDataProvider([
            'query' => $estorTriggers,
        ]);


        return $this->render('index', [
                    'dataTables' => $dataTables,
                    'dataViews' => $dataViews,
                    'dataTriggers' => $dataTriggers,
        ]);
    }

    public function actionDbVersion() {
        $sql = 'SELECT * FROM V$VERSION ';
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => $sql,
        ]);

        var_dump($dataProvider->models);
        die();
    }

    public function actionMigrate() {


//        $fileName = "drop.sql";
//        $fileName = "init.sql";
//        $fileName = "views.sql";
//        $myfile = fopen($fileName, "r") or die("Unable to open file!");
//        $commandR = fread($myfile, filesize($fileName));
//        fclose($myfile);
//        var_dump($commandR);die();
        $commandR = '
  CREATE OR REPLACE VIEW "ESTOR"."REPORT_IN_QUARTER" ("YEAR", "QUARTER", "COUNT", "TOTAL_PRICE") AS
  WITH DATA1 AS (select
  ESTOR_ITEMS.SKU,
  ESTOR_ITEMS.UNIT_PRICE,
  ESTOR_ITEMS.INVENTORY_ID,
  CHECKIN_DATE, extract(year from CHECKIN_DATE) year,
  to_char(CHECKIN_DATE,\'Q\') quarter
from ESTOR_ITEMS
WHERE CHECKIN_DATE IS NOT NULL and deleted = 0)

SELECT
  DISTINCT YEAR,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER;
--------------------------------------------------------
--  DDL for View REPORT_OUT_QUARTER
--------------------------------------------------------

  CREATE OR REPLACE VIEW "ESTOR"."REPORT_OUT_QUARTER" ("YEAR", "QUARTER", "COUNT", "TOTAL_PRICE") AS
  WITH DATA1 AS (select
  ESTOR_ITEMS.SKU,
  ESTOR_ITEMS.UNIT_PRICE,
  ESTOR_ITEMS.INVENTORY_ID,
  CHECKOUT_DATE, extract(year from CHECKOUT_DATE) year,
  to_char(CHECKOUT_DATE,\'Q\') quarter
from ESTOR_ITEMS
WHERE CHECKOUT_DATE IS NOT NULL and deleted = 0)

SELECT
  DISTINCT YEAR,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER;
--------------------------------------------------------
--  DDL for View LIST_YEAR
--------------------------------------------------------

  CREATE OR REPLACE VIEW "ESTOR"."LIST_YEAR" ("YEAR", "QUARTER") AS
  select year,quarter from REPORT_IN_QUARTER union
select year,quarter from REPORT_OUT_QUARTER;
--------------------------------------------------------
--  DDL for View REPORT_CURRENT
--------------------------------------------------------

  CREATE OR REPLACE VIEW "ESTOR"."REPORT_CURRENT" ("YEAR", "QUARTER", "COUNT", "TOTAL_PRICE") AS
  WITH DATA1 AS (select
  ESTOR_ITEMS.SKU,
  ESTOR_ITEMS.UNIT_PRICE,
  ESTOR_ITEMS.INVENTORY_ID,
  CHECKIN_DATE, extract(year from CHECKIN_DATE) year,
  to_char(CHECKIN_DATE,\'Q\') quarter
from ESTOR_ITEMS
WHERE CHECKIN_DATE IS NOT NULL and CHECKOUT_DATE IS NULL or CHECKOUT_DATE IS NULL or to_char(CHECKOUT_DATE,\'q\') < 4)

SELECT
  DISTINCT YEAR,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER;
--------------------------------------------------------
--  DDL for View REPORT_PREVIOUS_Q1
--------------------------------------------------------

  CREATE OR REPLACE VIEW "ESTOR"."REPORT_PREVIOUS_Q1" ("QUARTER", "QUANTITY", "TOTAL_PRICE") AS
  SELECT
  \'1\' QUARTER,
  COUNT ("ESTOR_ITEMS".ID) QUANTITY,
  SUM ("ESTOR_ITEMS".UNIT_PRICE) TOTAL_PRICE
FROM "ESTOR_ITEMS"
WHERE "ESTOR_ITEMS".CHECKOUT_DATE IS NULL
AND ESTOR_ITEMS.DELETED = 0;
--------------------------------------------------------
--  DDL for View REPORT_ALL
--------------------------------------------------------

  CREATE OR REPLACE VIEW "ESTOR"."REPORT_ALL" ("YEAR", "QUARTER", "COUNT_IN", "PRICE_IN", "COUNT_OUT", "PRICE_OUT", "COUNT_CURRENT", "PRICE_CURRENT") AS
  select
    list_year.YEAR,
    list_year.QUARTER,
    NVL(REPORT_IN_QUARTER.COUNT, \'0\') count_in,
    NVL(REPORT_IN_QUARTER.TOTAL_PRICE, \'0\') price_in,
    NVL(REPORT_out_QUARTER.COUNT, \'0\') count_out,
    NVL(REPORT_out_QUARTER.TOTAL_PRICE, \'0\') price_out,

      sum(NVL(REPORT_IN_QUARTER.COUNT, \'0\')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING) -
      sum(NVL(REPORT_OUT_QUARTER.COUNT, \'0\')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING)
    count_current,
      sum(NVL(REPORT_in_QUARTER.TOTAL_PRICE, \'0\')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING) -
      sum(NVL(REPORT_out_QUARTER.TOTAL_PRICE, \'0\')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING)
    price_current

from list_year
left join REPORT_IN_QUARTER
on (REPORT_IN_QUARTER.YEAR = list_year.YEAR and list_year.QUARTER = REPORT_IN_QUARTER.QUARTER)
left join REPORT_OUT_QUARTER
on (list_year.YEAR = REPORT_OUT_QUARTER.YEAR and list_year.QUARTER = REPORT_OUT_QUARTER.QUARTER)
order by list_year.YEAR, list_year.QUARTER;
--------------------------------------------------------
--  DDL for View REPORT_ALL2
--------------------------------------------------------

  CREATE OR REPLACE VIEW "ESTOR"."REPORT_ALL2" ("YEAR", "QUARTER", "COUNT_CURRENT", "PRICE_CURRENT", "COUNT_IN", "PRICE_IN", "COUNT_OUT", "PRICE_OUT") AS
  select
    REPORT_IN_QUARTER.YEAR,
    REPORT_IN_QUARTER.QUARTER,
    NVL(REPORT_CURRENT.COUNT, \'0\') count_CURRENT,
    NVL(REPORT_CURRENT.TOTAL_PRICE, \'0\') price_CURRENT,
    NVL(REPORT_IN_QUARTER.COUNT, \'0\') count_in,
    NVL(REPORT_IN_QUARTER.TOTAL_PRICE, \'0\') price_in,
    NVL(REPORT_out_QUARTER.COUNT, \'0\') count_out,
    NVL(REPORT_out_QUARTER.TOTAL_PRICE, \'0\') price_out
from REPORT_IN_QUARTER
left join REPORT_OUT_QUARTER
on (REPORT_IN_QUARTER.YEAR = REPORT_OUT_QUARTER.YEAR and REPORT_IN_QUARTER.QUARTER = REPORT_OUT_QUARTER.QUARTER)
left join REPORT_CURRENT
on (REPORT_IN_QUARTER.YEAR = REPORT_CURRENT.YEAR and REPORT_IN_QUARTER.QUARTER = REPORT_CURRENT.QUARTER)
order by REPORT_IN_QUARTER.YEAR, REPORT_IN_QUARTER.QUARTER;
';
        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach (array_filter(array_map('trim', explode(';', $commandR))) as $query) {
//                $command = Yii::$app->db->createCommand($commandR);
                $command = Yii::$app->db->createCommand($query);
                $command->execute();
            }
//            $command = Yii::$app->db->createCommand("@".'"BEGIN '.$commandR.'END;"');
            $commandR2 = '  CREATE OR REPLACE VIEW "ESTOR"."REPORT_IN_QUARTER" ("YEAR", "QUARTER", "COUNT", "TOTAL_PRICE") AS
  WITH DATA1 AS (select
  ESTOR_ITEMS.SKU,
  ESTOR_ITEMS.UNIT_PRICE,
  ESTOR_ITEMS.INVENTORY_ID,
  CHECKIN_DATE, extract(year from CHECKIN_DATE) year,
  to_char(CHECKIN_DATE,\'Q\') quarter
from ESTOR_ITEMS
WHERE CHECKIN_DATE IS NOT NULL and deleted = 0)

SELECT
  DISTINCT YEAR,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER';

//            $command = Yii::$app->db->createCommand($commandR2);
//            $commandR = Yii::$app->db->createCommand($commandR);
//            $command = Yii::$app->db->createCommand(@"
//                begin
//                DROP TABLE ACTIVITY_LOGS cascade constraints;
//                DROP TABLE AIRCOND_LIST cascade constraints;end;");
//            $command->execute();

            $transaction->commit();
            unset($command);
            \Yii::$app->session->setFlash('succes', [
                'type' => \kartik\widgets\Growl::TYPE_SUCCESS,
                'duration' => 5000,
                'icon' => 'glyphicon glyphicon-ok-sign',
                'title' => 'Berjaya',
                'message' => " Proses migrasi berjaya!",
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        } catch (\yii\db\Exception $e) {
            $transaction->commit();
            throw new \yii\web\HttpException(500, $e->getMessage());
            \Yii::$app->session->setFlash('fail', [
                'type' => \kartik\widgets\Growl::TYPE_DANGER,
                'duration' => 5000,
                'icon' => 'glyphicon glyphicon-remove-sign',
                'title' => 'Gagal',
                'message' => " Proses migrasi gagal!" . '<br>Error Code: <br>' . $e->getMessage(),
//                'message' => " Proses migrasi gagal!".'<br>Error Code: <br>'.$e->errorInfo(),
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
//            var_dump($e->getCode().'<br>'.$e->getMessage());
            die();
        }

        $estorTables = \common\models\EstorTables::find()->select('TABLE_NAME')->where(['TABLE_NAME' => 'migration']);
        $estorViews = \common\models\EstorViews::find()->select('VIEW_NAME');
        $estorTriggers = \common\models\EstorTriggers::find()->select('TRIGGER_NAME');

// add conditions that should always apply here

        $dataTables = new ActiveDataProvider([
            'query' => $estorTables,
        ]);

        $dataViews = new ActiveDataProvider([
            'query' => $estorViews,
        ]);
        $dataTriggers = new ActiveDataProvider([
            'query' => $estorTriggers,
        ]);


        return $this->render('migrate', [
                    'dataTables' => $dataTables,
                    'dataViews' => $dataViews,
                    'dataTriggers' => $dataTriggers,
        ]);
    }

    public function actionMigrationTest() {

        $transaction = Yii::$app->db->beginTransaction();


//        $commandR is the Sql command being excute
//        $tableName is the table name use for testing

        $tableName = '';
        $tableName = '{{%migration}}';
        $tableNameQuery = '{{%migration}}';
//        create table $tableName
//        $commandR = Yii::$app->db->createCommand()->createTable($tableName, [
//                    'version' => 'varchar(180) NOT NULL PRIMARY KEY',
//                    'apply_time' => 'integer',
//                ])->execute();

        $commandR = Yii::$app->db->createCommand()->dropTable($tableName)->execute();

        $transaction->commit();
        $estorTables = \common\models\EstorTables::find()->select('TABLE_NAME')->where(['TABLE_NAME' => $tableNameQuery]);
        $estorViews = \common\models\EstorViews::find()->select('VIEW_NAME');
        $estorTriggers = \common\models\EstorTriggers::find()->select('TRIGGER_NAME');

// add conditions that should always apply here

        $dataTables = new ActiveDataProvider([
            'query' => $estorTables,
        ]);

        $dataViews = new ActiveDataProvider([
            'query' => $estorViews,
        ]);
        $dataTriggers = new ActiveDataProvider([
            'query' => $estorTriggers,
        ]);


        return $this->render('migrate', [
                    'dataTables' => $dataTables,
                    'dataViews' => $dataViews,
                    'dataTriggers' => $dataTriggers,
        ]);
    }

}
