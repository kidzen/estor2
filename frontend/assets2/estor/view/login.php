<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="mmh2050">
<!--    <link rel="icon" href="../../favicon.ico">-->

    <title>eCuti Management System</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASEDIR; ?>engine/view/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo BASEDIR; ?>engine/view/plugin/bootstrap/css/bootstrap-theme.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo BASEDIR; ?>engine/view/css/signin.css" rel="stylesheet">

    <script src="<?php echo BASEDIR; ?>engine/view/plugin/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo BASEDIR; ?>engine/view/js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script type="text/javascript"> 
        $(document).ready(function(){   
             // $('#respond').hide();
               $('#reseter').click(function()
               {
                    $('#respond').hide();
                   $('#staffno').val('');
                   $('#staffpwd').val('');
                   
                   
                   
                
               });//return form value to null
                
              });//main document ready
       </script>  
        
        

   
  </head>
  <body>

    <div class="container">
<div class="panel panel-default" >
    <div class="panel-heading "><a href="../index.php">
            <img style="margin-top: 20px;" src ="<?php echo BASEDIR; ?>engine/view/img/header.png" class="img-responsive" draggable="false"></a>
   

</div><div class="panel-body">
    <div  id="respond" ></div><!--alert-->
     
    
 
           
   
    <div class="list-group-item" style="margin-top: 1px">
   <h4  style="margin: 10px auto 50px;"> Sistem E-Cuti | Log Masuk </h4> 
     
        <form class="form-signin " method="post" action="<?php echo BASEDIR; ?>auth/login"  >
            
     
        <label for="text" class="sr-only">No. Staf</label>
        <div class="input-group"> <span class="input-group-addon" ><span class="glyphicon glyphicon-credit-card"></span></span>
        <input type="text" name="staffno" id="staffno" class="form-control " placeholder="No. Staf" required autofocus>
        </div>
        
        
        <label for="inputPassword" class="sr-only">Katalaluan</label>
        <div class="input-group"> <span class="input-group-addon" ><span class="glyphicon glyphicon-lock"></span></span>
        <input type="password" name="staffpwd" id="staffpwd" class="form-control" placeholder="Katalaluan" required>
        </div>
                     
        <div class="btn-group btn-group-sm" style="padding-left: 0px;padding-top:1px">
        <button type="submit" name="submitx" id="submit"  class="btn btn-default"><span class="glyphicon glyphicon-log-in"></span>  Masuk </button>
        <button  type="button" id="reseter" value="Reset" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span>  Reset </button>
        </div>
        
   <hr />
           <p ><a href="../daftar" ><span class="glyphicon glyphicon-link"></span> Daftar Akaun Kali Pertama</a></p>
		<p ><a href="http://picoms.edu.my/cms2/images/gallery/download/CARA%20PENGGUNAAN%20SISTEM%20ECUTI.pdf"><span class="glyphicon glyphicon-link"></span> Panduan Penggunaan</a></p>
		
        </form></div>
    
</div></div><!-- /panel-->


<center><br/>Copyright &copy; <?php echo date('Y'); ?> Hak Cipta Milik (IT PICOMS).</center><!-- ni footer tu...ejas la...kalau xnk buang-->
<center> All Rights Reserved.</center><br/>
				


    </div> <!-- /container -->


    
  </body>
</html>



