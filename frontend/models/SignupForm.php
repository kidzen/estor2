<?php

namespace frontend\models;

use common\models\People;
use common\models\MpspStaff;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model {

    public $USERNAME;
    public $EMAIL;
    public $PASSWORD;
    public $ROLE_ID;
    public $STAFF_NO;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['USERNAME', 'filter', 'filter' => 'trim'],
            ['USERNAME', 'required'],
            ['USERNAME', 'unique', 'targetClass' => '\common\models\People', 'message' => 'This username has already been taken.'],
            ['USERNAME', 'string', 'min' => 2, 'max' => 255],

            ['STAFF_NO', 'filter', 'filter' => 'trim'],
            ['STAFF_NO', 'required'],
            ['STAFF_NO', 'unique', 'targetClass' => '\common\models\People', 'message' => 'This staff_no has already been taken.'],
            ['STAFF_NO', 'exist', 'targetClass' => '\common\models\MpspStaff', 'targetAttribute' => 'NO_PEKERJA', 'message' => 'This staff no does not exist as mpsp staff.'],
            ['STAFF_NO', 'integer'],

            ['EMAIL', 'filter', 'filter' => 'trim'],
            ['EMAIL', 'required'],
            ['EMAIL', 'email'],
            ['EMAIL', 'string', 'max' => 255],
            ['EMAIL', 'unique', 'targetClass' => '\common\models\People', 'message' => 'This email address has already been taken.'],
            ['ROLE_ID', 'filter', 'filter' => 'trim'],
            ['PASSWORD', 'required'],
            ['PASSWORD', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup() {

        if (!$this->validate()) {
            return null;
        }
        // $test = MpspStaff::find()->asArray()->all();
        // var_dump($test);
        // die;
        $user = new People();
        $user->USERNAME = $this->USERNAME;
        $user->STAFF_NO = $this->STAFF_NO;
        $user->EMAIL = $this->EMAIL;
        $user->ROLE_ID = 2;
        $user->DELETED = 1;
        $user->setPassword($this->PASSWORD);
        $user->generateAuthKey();
        $user->validate();
        // var_dump($user->validate());
//        $user->save();
       // var_dump($user->getErrors());die();
       // var_dump($user->save());die();

//        var_dump($user);die();
//        $auth = Yii::$app->authManager;
//        $authorRole = $auth->getRole('author');
//        $auth->assign($authorRole, $user->getId());
//        var_dump($user->save());
//        var_dump($user->getErrors());
//        die();
        return $user->save() ? $user : null;
    }

}
