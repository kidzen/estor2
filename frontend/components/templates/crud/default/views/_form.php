<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">

    <?= "<?php " ?>$form = ActiveForm::begin(); ?>
    <div class="panel-body">
        <div class="row">
            <?php
            foreach ($generator->getColumnNames() as $attribute) {
                if ($attribute != 'ID' && $attribute != 'APPROVED_AT' && $attribute != 'APPROVED_BY' && $attribute != 'DELETED' && $attribute != 'DELETED_AT' 
                        && $attribute != 'CREATED_AT' && $attribute != 'CREATED_BY' && $attribute != 'UPDATED_AT' && $attribute != 'UPDATED_BY' && $attribute != 'CHECK_DATE' 
                        && $attribute != 'CHECK_BY') {
                    if (in_array($attribute, $safeAttributes)) {
                        echo "            <div class=\"col-md-4\">\n";
                        echo "                <?= " . $generator->generateActiveField($attribute) . " ?>\n";
                        echo "            </div>\n";
                    }
                }
            }
            ?>

        </div>
        <div>
            <?= "<?= " ?>Html::a(<?= $generator->generateString('Cancel') ?> , Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
            <?= "<?= " ?>Html::submitButton($model->isNewRecord ? <?= $generator->generateString('Create') ?> : <?= $generator->generateString('Update') ?>, ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?= "<?php " ?>ActiveForm::end(); ?>

</div>
