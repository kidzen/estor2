<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $checkIn common\models\InventoryItems */
//die();
$this->title = Yii::t('app', 'Update {modelClass}: ', [
            'modelClass' => 'Check In Items',
        ]) . $checkIn->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inventory Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $checkIn->ID, 'url' => ['view', 'id' => $checkIn->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Kemaskini');
?>
<div class="inventory-items-update">

    <?=
    $this->render('_form-update', [
        'transaction' => $transaction,
        'checkIn' => $checkIn,
        'inventory' => $inventory,
        'items' => $items,
        'inventoriesArray' => $inventoriesArray,
        'vendorsArray' => $vendorsArray,
    ])
    ?>

</div>
