

<tfoot>
    <tr>
        <td class="foot-head-col-1 foot-head-row-1" style="white-space:nowrap;" colspan="2">Pegawai Pemesan :</td>
        <td class="foot-head-col-2 foot-head-row-1" style="white-space:nowrap;" colspan="2">Pegawai Penerima :</td>
        <td class="foot-head-col-3 foot-head-row-1" style="white-space:nowrap;" colspan="5">Tarikh Diluluskan dan Direkodkan Oleh :</td>
        <td class="foot-head-col-4 foot-head-row-1" style="white-space:nowrap;" colspan="2">Dikeluarkan dan Direkodkan Oleh :</td>
    </tr>
    <tr>
        <td class="foot-col-1 foot-row-2"><strong>Nama :</strong></td>
        <td class="foot-col-2 foot-row-2"><?= mb_strimwidth($items[0]['order']['ORDERED_BY'],0,20,"...",'UTF-8') ?></td>
        <td class="foot-col-3 foot-row-2"><strong>Nama :</strong></td>
        <td class="foot-col-4 foot-row-2"> </td>
        <td class="foot-col-5 foot-row-2"><strong>Nama :</strong></td>
        <td class="foot-col-6 foot-row-2" colspan="4"><?= mb_strimwidth($items[0]['order']['approvedBy']['mpspProfile']['NAMA'],0,40,"...",'UTF-8') ?></td>
        <td class="foot-col-7 foot-row-2"><strong>Nama :</strong></td>
        <td class="foot-col-8 foot-row-2"> </td>
    </tr>
    <tr>
        <td class="foot-col-1 foot-row-3"><strong>Jawatan :</strong></td>
        <td class="foot-col-2 foot-row-3"> </td>
        <td class="foot-col-3 foot-row-3"><strong>Jawatan :</strong></td>
        <td class="foot-col-4 foot-row-3"> </td>
        <td class="foot-col-5 foot-row-3"><strong>Jawatan :</strong></td>
        <td class="foot-col-6 foot-row-3" colspan="4"><?= mb_strimwidth($items[0]['order']['approvedBy']['mpspProfile']['JAWATAN'],0,40,"...",'UTF-8') ?></td>
        <td class="foot-col-7 foot-row-3"><strong>Jawatan :</strong></td>
        <td class="foot-col-8 foot-row-3"> </td>
    </tr>
    <tr>
        <td class="foot-col-1 foot-row-4"><strong>Jabatan :</strong></td>
        <td class="foot-col-2 foot-row-4"> </td>
        <td class="foot-col-3 foot-row-4"><strong>Jabatan :</strong></td>
        <td class="foot-col-4 foot-row-4"> </td>
        <td class="foot-col-5 foot-row-4"><strong>Jabatan :</strong></td>
        <td class="foot-col-6 foot-row-4" colspan="4"><?= mb_strimwidth($items[0]['order']['approvedBy']['mpspProfile']['KETERANGAN'],0,40,"...",'UTF-8') ?></td>
        <td class="foot-col-7 foot-row-4"><strong>Jabatan :</strong></td>
        <td class="foot-col-8 foot-row-4"> </td>
    </tr>
    <tr>
        <td class="foot-col-1 foot-row-5"><strong>Tarikh :</strong></td>
        <td class="foot-col-2 foot-row-5"><?= mb_strimwidth($items[0]['order']['ORDER_DATE'],0,40,"...",'UTF-8') ?></td>
        <td class="foot-col-3 foot-row-5"><strong>Tarikh :</strong></td>
        <td class="foot-col-4 foot-row-5"> </td>
        <td class="foot-col-5 foot-row-5"><strong>Tarikh :</strong></td>
        <td class="foot-col-6 foot-row-5" colspan="4"><?= mb_strimwidth($items[0]['order']['APPROVED_AT'],0,40,"...",'UTF-8') ?></td>
        <td class="foot-col-7 foot-row-5"><strong>Tarikh :</strong></td>
        <td class="foot-col-8 foot-row-5"> </td>
    </tr>
    <tr>
        <td class="foot-note-col-1 foot-note-row-6" colspan="4" rowspan="8">
            <br><br>		
            <p>Nota</p>		
            <p>Salinan 1 - Disimpan oleh pemesan</p>
            <p>Salinan 2 - Bahagian Bekalan, Kawalan Dan Akaun</p>
            <p>Salinan 3 - Bahagian Simpanan</p>
            <p>Salinan 4 - Bahagian Bungkusan Dan Penghantaran</p>
            <p>Salinan 5 – Disimpan oleh  pemesan setelah stok diterima</p>
        </td>
        <td class="foot-col-2 foot-row-6 foot-deliver-head text-center info" colspan="7"><strong>BAHAGIAN BUNGKUSAN DAN,PENGHANTARAN</strong></td>
    </tr>
    <tr>
        <td class="foot-head-col-1 foot-head-row-7" colspan="2" rowspan="7"><strong>Butir-Butir Bungkusan</strong></td>
        <td class="foot-head-col-2 foot-head-row-7" colspan="3" rowspan="7"><strong>Butir-Butir Penghantaran</strong></td>
        <td class="foot-head-col-3 foot-head-row-7" colspan="2">Telah dibungkus dan dihantar oleh :</td>
    </tr>
    <tr>
        <td class="foot-col-1 foot-row-8"><strong>Nama : </strong></td>
        <td class="foot-col-2 foot-row-8"> </td>
    </tr>
    <tr>
        <td class="foot-col-1 foot-row-9"><strong>Jawatan : </strong></td>
        <td class="foot-col-2 foot-row-9"> </td>
    </tr>
    <tr>
        <td class="foot-col-1 foot-row-10"><strong>Jabatan : </strong></td>
        <td class="foot-col-2 foot-row-10"> </td>
    </tr>
    <tr>
        <td class="foot-col-1 foot-row-11"><strong>Tarikh : </strong></td>
        <td class="foot-col-2 foot-row-11"> </td>
    </tr>
</tfoot>
</table>

</div>
</div>
