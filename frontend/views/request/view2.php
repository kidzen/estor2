<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info orders-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>orders : <?= Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a(Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-default']) ?>
            <?= Html::a(Yii::t('app', 'Kemaskini'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </p>
        <div class="order-items-index">

            <?php Pjax::begin(); ?>            
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                'pjax' => true, // pjax is set to always true for this demo
                // set your toolbar
                'toolbar' => [
                    ['content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title' => Yii::t('app', 'Create order-items'),])//. ' ' .
                    //                Html::a(Yii::t('app', 'Create Categories'), ['create'], ['class' => 'btn btn-success']) //. ' ' .
                    //                Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type' => 'button', 'title' => Yii::t('app', 'Add Book'), 'class' => 'btn btn-success', 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' ' .
                    ],
                    '{export}',
                    '{toggleData}',
                ],
                // set export properties
//                'exportConfig' => [
//                    'pdf' => $pdf,
//                    'csv' => '{csv}',
//                    'xls' => '{xls}',
//                ],
                'export' => [
                    'fontAwesome' => true,
                    'target' => '_self',
                ],
                // parameters from the demo form
                'bordered' => true,
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'hover' => true,
                //        'showPageSummary' => true,
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => $this->title,
                ],
                'responsiveWrap' => false,
                'persistResize' => false,
                //        'exportConfig' => $exportConfig,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'ID',
                        'hAlign' => 'center', 'vAlign' => 'middle',
                    ],
                    [

                        'attribute' => 'order.ORDER_NO',
                        'hAlign' => 'center', 'vAlign' => 'middle',
                        'group' => true
                    ],
                    [

                        'attribute' => 'order.ARAHAN_KERJA_ID',
                        'hAlign' => 'center', 'vAlign' => 'middle',
                        'group' => true, 'subGroupOf' => 2,
                    ],
                    [
                        'label' => 'Inventory Details',
                        'attribute' => 'INVENTORY_DETAILS',
                        'format' => 'raw',
                        'value' => function($model) {
                            $card = 'Card No : ' . $model->inventory->CARD_NO;
                            $code = 'Code No : ' . $model->inventory->CODE_NO;
                            $description = 'Description : <br>' . $model->inventory->DESCRIPTION;
                            return Html::tag('span', $card . '<br>' . $code . '<br>' . $description);
                        },
                        'hAlign' => 'left', 'vAlign' => 'middle',
                    ],
////            [
////                'attribute' => 'CURRENT_BALANCE',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
////            [
////                'attribute' => 'transaction.CHECK_BY',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
//            [
//                'attribute' => 'transaction.CHECK_DATE',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
                    [
                        'attribute' => 'order.ORDER_DATE',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::tag('span', 'Date : ' . $model->order->ORDER_DATE . '<br>' . 'Order by : ' . $model->order->ORDERED_BY);
                        },
                        'hAlign' => 'center', 'vAlign' => 'middle',
                        'group' => true, 'subGroupOf' => 2,
                    ],
                    [
                        'attribute' => 'order.REQUIRED_DATE',
                        'hAlign' => 'center', 'vAlign' => 'middle',
                        'group' => true, 'subGroupOf' => 2,
                    ],
//            [
//                'attribute' => 'order.ORDERED_BY',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
                    [
                        'label' => 'Inventory Quantity',
                        'attribute' => 'inventory.QUANTITY',
                        'hAlign' => 'center', 'vAlign' => 'middle',
                    ],
//            [
//                'attribute' => 'RQ_QUANTITY',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
                    [
                        'attribute' => 'QUANTITY',
                        'format' => 'raw',
                        'value' => function ($model) {
                            if ($model->APP_QUANTITY) {
                                return Html::tag('span', 'Requested : ' . $model->RQ_QUANTITY . '<br>' . 'Approved : ' . $model->APP_QUANTITY);
                            } else {
                                return Html::tag('span', 'Requested : ' . $model->RQ_QUANTITY . '<br>' . 'Approved : ' . 0);
                            }
                        },
                        'hAlign' => 'center', 'vAlign' => 'middle',
                    ],
//            [
//
//                'attribute' => 'items',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//                'format' => 'raw',
//                'value' => function($model) {
//                    $data = \yii\helpers\ArrayHelper::map($model->items, 'ID', 'SKU');
//                    return implode("<br>", $data);
//                },
//            ],
                    [
                        'label' => 'Total Price',
                        'attribute' => 'UNIT_PRICE',
                        'hAlign' => 'center', 'vAlign' => 'middle',
                    ],
                    [
                        'label' => 'Usage',
                        'attribute' => 'USAGE_DETAIL',
                        'format' => 'raw',
                        'value' => function($model) {
                            $type = 'ID : ' . $model['vehicle']['ID'];
                            $regNo = 'Reg No : ' . $model['vehicle']['REG_NO'];
                            $description = 'Model : ' . $model['vehicle']['MODEL'];
                            return Html::tag('span', $type . '<br>' . $regNo . '<br>' . $description);
                        },
                        'hAlign' => 'left', 'vAlign' => 'middle',
                        'group' => true, 'subGroupOf' => 2,
                    ],
//            [
//                'attribute' => 'order.CHECKOUT_DATE',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
//            [
//                'attribute' => 'order.CHECKOUT_BY',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
////            [
////                'attribute' => 'CREATED_AT',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
////            [
////                'attribute' => 'UPDATED_AT',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
////            [
////                'attribute' => 'CREATED_BY',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
////            [
////                'attribute' => 'UPDATED_BY',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
////            [
////                'attribute' => 'DELETED',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
////            [
////                'attribute' => 'DELETED_AT',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
                    [
                        'attribute' => 'order.APPROVED',
                        'format' => 'raw',
                        'value' => function($model) {
                            if ($model['order']['APPROVED'] == 2) {
                                return Html::label('PENDING', null, ['class' => 'label label-warning']);
                            } else if ($model['order']['APPROVED'] == 1) {
                                return Html::label('APPROVED', null, ['class' => 'label label-success']);
                            } else if ($model['order']['APPROVED'] == 8) {
                                return Html::label('REJECTED', null, ['class' => 'label label-danger']);
                            } else {
                                return Html::label('UNDEFINED', null, ['class' => 'label label-primary']);
                            }
                        },
                                'hAlign' => 'center', 'vAlign' => 'middle',
                                'group' => true, 'subGroupOf' => 2,
                            ],
////            [
////                'attribute' => 'order.APPROVED_BY',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
////            [
////                'attribute' => 'order.APPROVED_AT',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
                            [
                                'class' => 'kartik\grid\ActionColumn',
                                'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isPegawaiStor,
//                        'group' => true, 
//                        'subGroupOf' => 2,
                                'template' => '{approval} {reject} {item-list} {view}{update}{delete}{recover}',
                                'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
                                'updateOptions' => ['title' => 'Update', 'data-toggle' => 'tooltip'],
//                        'group' => true, 'subGroupOf' => 2,
                                'buttons' => [
                                    'approval' => function ($url, $model) {
                                        if ($model['order']['APPROVED'] === 2) {
                                            return Html::a('<span class="glyphicon glyphicon-check" style="color:green;"></span>', ['approval', 'ordersId' => $model->order->ID], ['title' => Yii::t('yii', 'Approval'), 'data-toggle' => 'tooltip']);
                                        }
                                    },
                                            'reject' => function ($url, $model) {
                                        if ($model['order']['APPROVED'] === 2) {
//                                    return Html::a('<span class="glyphicon glyphicon-remove" style="color:red;"></span>', ['reject', 'id' => $model->order->ID], ['title' => Yii::t('yii', 'Reject'), 'data-toggle' => 'tooltip']);
                                            return Html::a('<span class="glyphicon glyphicon-remove" style="color:red;"></span>', ['reject', 'id' => $model->ID], ['title' => Yii::t('yii', 'Reject'), 'data-toggle' => 'tooltip']);
                                        }
                                    },
//                                    'item-list' => function ($url, $model) {
//                                return Html::a('<span class="glyphicon glyphicon-list"></span>', $url, ['title' => Yii::t('yii', 'Item List'), 'data-toggle' => 'tooltip']);
//                            },
                                            'recover' => function ($url, $model) {
                                        if ($model->DELETED === 1) {
                                            return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, ['title' => Yii::t('yii', 'Recover'), 'data-toggle' => 'tooltip','data-method' => 'POST']);
                                        }
                                    },
                                            'delete' => function ($url, $model) {
                                        if ($model->DELETED === 0) {
                                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'), 'data-toggle' => 'tooltip',
                                                        'data-method' => 'post']);
                                        }
                                    },
                                        ],
                                    ],
                                    [
                                        'header' => 'Permanent Delete',
                                        'class' => 'kartik\grid\ActionColumn',
                                        'template' => '{delete-permanent}',
                                        'buttons' => [
                                            'delete-permanent' => function ($url, $model) {
                                                return Html::a('<span class="glyphicon glyphicon-trash" style="color:red;"></span>', $url, [
                                                            'title' => Yii::t('yii', 'Permanent Delete'), 'data-toggle' => 'tooltip','data-method' => 'POST'
                                                ]);
                                            }
                                                ],
                                                'visible' => Yii::$app->user->isAdmin,
                                            ],
                                        ],
                                    ]);
                                    ?>
                                    <?php Pjax::end(); ?></div>
    </div>
