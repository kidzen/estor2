<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */

$this->title = $model->ID . ' : BORANG PESANAN DAN PENGELUARAN STOK';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info orders-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i><?= Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">



        <div class="box-body table-responsive">
            <div>
                <!--<p><span>Pekeliling Perbendaharaan Malaysia</span><span class="pull-right">AM 6.5 Lampiran A</span></p>-->
                <!--<p class="pull-right form-lampiran"><strong>LAMPIRAN A</strong></p>-->
                <!--<div class="clearfix"></div>-->
                <!--<p class="pull-right form-id"><strong>KEW.PS-10</strong></p>-->
                <!--<div class="clearfix"></div>-->
                <!--<p class="text-center form-name"><strong>BORANG PESANAN DAN PENGELUARAN STOK</strong></p>-->
                <table class="kv-grid-table table table-hover table-bordered table-striped table-condensed kv-table-wrap">
                <!--<table bordered>-->
                    <thead>
                        <tr>
                            <th class="col-1 row-1" colspan="4"><span class="row-1-label">Daripada :</span><br><br></th>
                            <th class="col-2 row-1" colspan="7"><span class="row-1-label">Kepada :</span><br><br></th>
                        </tr>
                        <tr>
                            <th class="text-center info col-1 row-2" colspan="4">Dilengkapkan Oleh Stor Pesanan</th>
                            <th class=" right-border-bold col-2 row-2 text-center info" colspan="7">Dilengkapkan Oleh Stor Pengeluar</th>
                        </tr>
                        <tr>
                            <th class="col-1 row-3" colspan="4">No Pemesanan : <?= $items[0]->ORDER_NO?></th>
                            <th class="col-2 row-3 right-border-bold" colspan="7">No Pengeluaran : <?= $items[0]->ORDER_NO?></th>
                        </tr>
                        <tr>
                            <th class="col-1 row-4" colspan="4">Tarikh Bekalan Dikehendaki : <?= $items[0]->ORDER_REQUIRED_DATE ?></th>
                            <th class="col-2 row-4 text-center info right-border-bold" colspan="5">BAHAGIAN BEKALAN,KAWALAN DAN AKAUN</th>
                            <th class="col-3 row-4 text-center info" colspan="2">BAHAGIAN SIMPANAN</th>
                        </tr>
                        <tr>
                            <th class="col-1 row-5 text-center info" rowspan="2">No. Kod</th>
                            <th class="col-2 row-5 text-center info" colspan="2" rowspan="2">Perihal Stok</th>
                            <th class="col-3 row-5 text-center info" rowspan="2">Kuantiti</th>
                            <th class="col-4 row-5 text-center info" colspan="2">Kad Kawalan Stok</th>
                            <th class="col-5 row-5 text-center info" rowspan="2">Kuantiti <br>Diluluskan</th>
                            <th class="col-6 row-5 text-center info" colspan="2">Harga (RM)</th>
                            <th class="col-7 row-5 text-center info" rowspan="2">Kuantiti <br>Dikeluarkan</th>
                            <th class="col-8 row-5 text-center info" rowspan="2">Catatan</th>
                        </tr>
                        <tr>
                            <th class="col-1 row-6 text-center info">No. Kad</th>
                            <th class="col-2 row-6 text-center info">Baki Sedia Ada</th>
                            <th class="col-3 row-6 text-center info">Seunit</th>
                            <th class="col-4 row-6 text-center info">Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($items as $item) { ?>
                            <tr>
                                <td class="col-1 row-1 text-center"><?= $item->CODE_NO ?></td>
                                <td class="col-2 row-1" colspan="2"><?= $item->DESCRIPTION ?></td>
                                <td class="col-3 row-1 text-center"><?= $item->RQ_QUANTITY ?></td>
                                <td class="col-4 row-1 text-center"><?= $item->CARD_NO ?></td>
                                <td class="col-5 row-1 text-center"><?= $item->CURRENT_BALANCE ?></td>
                                <td class="col-6 row-1 text-center"><?= $item->APP_QUANTITY ?></td>
                                <td class="col-7 row-1 text-right"><?= number_format($item->UNIT_PRICE, 2, '.', ' '); ?></td>
                                <td class="col-8 row-1 text-right"><?= number_format($item->BATCH_TOTAL_PRICE, 2, '.', ' ') ?></td>
                                <td class="col-9 row-1 text-center"><?= $item->APP_QUANTITY ?></td>
                                <td class="col-10 row-1"></td>
                            </tr>
                        <?php } ?>
                    <tbody>
                    <tfoot>
                        <tr>
                            <td class="foot-head-col-1 foot-head-row-1" colspan="2">Pegawai Pemesan :</td>
                            <td class="foot-head-col-2 foot-head-row-1" colspan="2">Pegawai Penerima :</td>
                            <td class="foot-head-col-3 foot-head-row-1" colspan="5">Tarikh Diluluskan dan Direkodkan Oleh :</td>
                            <td class="foot-head-col-4 foot-head-row-1" colspan="2">Dikeluarkan dan Direkodkan Oleh :</td>
                        </tr>
                        <tr>
                            <td class="foot-col-1 foot-row-2"><strong>Nama :</strong></td>
                            <td class="foot-col-2 foot-row-2"><?= $items[0]->ORDERED_BY ?></td>
                            <td class="foot-col-3 foot-row-2"><strong>Nama :</strong></td>
                            <td class="foot-col-4 foot-row-2" style="width: 200px;"> </td>
                            <td class="foot-col-5 foot-row-2"><strong>Nama :</strong></td>
                            <td class="foot-col-6 foot-row-2" colspan="4"><?= $items[0]->approvedBy->NAMA ?></td>
                            <td class="foot-col-7 foot-row-2"><strong>Nama :</strong></td>
                            <td class="foot-col-8 foot-row-2"><?= $items[0]->createdBy->NAMA ?></td>
                        </tr>
                        <tr>
                            <td class="foot-col-1 foot-row-3"><strong>Jawatan :</strong></td>
                            <td class="foot-col-2 foot-row-3"></td>
                            <td class="foot-col-3 foot-row-3"><strong>Jawatan :</strong></td>
                            <td class="foot-col-4 foot-row-3"> </td>
                            <td class="foot-col-5 foot-row-3"><strong>Jawatan :</strong></td>
                            <td class="foot-col-6 foot-row-3" colspan="4"><?= $items[0]->approvedBy->JAWATAN ?></td>
                            <td class="foot-col-7 foot-row-3"><strong>Jawatan :</strong></td>
                            <td class="foot-col-8 foot-row-3"><?= $items[0]->approvedBy->JAWATAN ?></td>
                        </tr>
                        <tr>
                            <td class="foot-col-1 foot-row-4"><strong>Jabatan :</strong></td>
                            <td class="foot-col-2 foot-row-4"></td>
                            <td class="foot-col-3 foot-row-4"><strong>Jabatan :</strong></td>
                            <td class="foot-col-4 foot-row-4"> </td>
                            <td class="foot-col-5 foot-row-4"><strong>Jabatan :</strong></td>
                            <td class="foot-col-6 foot-row-4" colspan="4"><?= $items[0]->approvedBy->KETERANGAN ?></td>
                            <td class="foot-col-7 foot-row-4"><strong>Jabatan :</strong></td>
                            <td class="foot-col-8 foot-row-4"><?= $items[0]->approvedBy->KETERANGAN ?></td>
                        </tr>
                        <tr>
                            <td class="foot-col-1 foot-row-5"><strong>Tarikh :</strong></td>
                            <td class="foot-col-2 foot-row-5"><?= $items[0]->ORDER_DATE ?></td>
                            <td class="foot-col-3 foot-row-5"><strong>Tarikh :</strong></td>
                            <td class="foot-col-4 foot-row-5"> </td>
                            <td class="foot-col-5 foot-row-5"><strong>Tarikh :</strong></td>
                            <td class="foot-col-6 foot-row-5" colspan="4"><?= $items[0]->APPROVED_DATE ?></td>
                            <td class="foot-col-7 foot-row-5"><strong>Tarikh :</strong></td>
                            <td class="foot-col-8 foot-row-5"><?= $items[0]->APPROVED_DATE ?></td>
                        </tr>
                        <tr>
                            <td class="foot-note-col-1 foot-note-row-6" colspan="4" rowspan="8">
                                <br><br>
                                <p>Nota</p>
                                <p>Salinan 1 - Disimpan oleh pemesan</p>
                                <p>Salinan 2 - Bahagian Bekalan, Kawalan Dan Akaun</p>
                                <p>Salinan 3 - Bahagian Simpanan</p>
                                <p>Salinan 4 - Bahagian Bungkusan Dan Penghantaran</p>
                                <p>Salinan 5 – Disimpan oleh  pemesan setelah stok diterima</p>
                            </td>
                            <td class="foot-col-2 foot-row-6 foot-deliver-head text-center info" colspan="7"><strong>BAHAGIAN BUNGKUSAN DAN,PENGHANTARAN</strong></td>
                        </tr>
                        <tr>
                            <td class="foot-head-col-1 foot-head-row-7" colspan="2" rowspan="7"><strong>Butir-Butir Bungkusan</strong></td>
                            <td class="foot-head-col-2 foot-head-row-7" colspan="3" rowspan="7"><strong>Butir-Butir Penghantaran</strong></td>
                            <td class="foot-head-col-3 foot-head-row-7" colspan="2">Telah dibungkus dan dihantar oleh :</td>
                        </tr>
                        <tr>
                            <td class="foot-col-1 foot-row-8"><strong>Nama : </strong></td>
                            <td class="foot-col-2 foot-row-8"> </td>
                        </tr>
                        <tr>
                            <td class="foot-col-1 foot-row-9"><strong>Jawatan : </strong></td>
                            <td class="foot-col-2 foot-row-9"> </td>
                        </tr>
                        <tr>
                            <td class="foot-col-1 foot-row-10"><strong>Jabatan : </strong></td>
                            <td class="foot-col-2 foot-row-10"> </td>
                        </tr>
                        <tr>
                            <td class="foot-col-1 foot-row-11"><strong>Tarikh : </strong></td>
                            <td class="foot-col-2 foot-row-11"> </td>
                        </tr>
                    </tfoot>
                </table>

            </div>
        </div>

    </div>
    <div class="box-footer">
        <span><?= Html::a('<i class="glyphicon glyphicon-print"></i> Cetak (KEWPS 10)', ['kewps10', 'id' => $model->ID], ['class' => 'pull-right btn btn-info', 'style'=>'margin-left: 10px', 'target' => '_blank']); ?>
        </span>
        <span><?= Html::a('<i class="glyphicon glyphicon-print"></i> Cetak (KEWPS 11)', ['kewps11', 'id' => $model->ID], ['class' => 'pull-right btn btn-info', 'target' => '_blank']); ?>
        </span>
        <!--footer-->
        <!--/.row-->
    </div>
</div>
