<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->items,
        'key' => 'ID'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'ID', 'visible' => false],
        ['attribute' => 'inventory.CARD_NO', 'vAlign' => 'middle','hAlign'=>'center'],
        ['attribute' => 'inventory.CODE_NO', 'vAlign' => 'middle','hAlign'=>'center'],
        'inventory.DESCRIPTION',
        ['attribute' => 'RQ_QUANTITY', 'vAlign' => 'middle','hAlign'=>'right'],
        ['attribute' => 'APP_QUANTITY', 'vAlign' => 'middle','hAlign'=>'right'],
        ['label'=>'Kuantiti Sedia Ada','attribute' => 'inventory.QUANTITY', 'vAlign' => 'middle','hAlign'=>'center'],
        // [
        //         'attribute' => 'department.name',
        //         'label' => 'Department'
        //     ],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'order-items'
        ],
    ];

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'responsiveWrap' => false,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
