<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TransactionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transactions-search">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Carian</h4>
            </div>
            <div class="modal-body">
                <?php
                $form = ActiveForm::begin(['action' => ['index'], 'method' => 'get',]);
                ?>

                <h3>Perincian Pesanan</h3>
                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($searchModel, 'order.ORDER_NO') ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($searchModel, 'arahanKerja.NO_KERJA') ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($searchModel, 'order.ORDER_DATE') ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($searchModel, 'order.REQUIRED_DATE') ?>
                    </div>
                </div>
                <h3>Perincian Inventori</h3>
                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($searchModel, 'inventory.CARD_NO') ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($searchModel, 'inventory.CODE_NO') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($searchModel, 'inventory.DESCRIPTION') ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'inventory.QUANTITY') ?>
                    </div>
                </div>
                <h3> Perincian Maklumat Pesanan</h3>
                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($searchModel, 'RQ_QUANTITY') ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($searchModel, 'APP_QUANTITY') ?>
                    </div>
                    <div class="col-md-3">
                        <?php echo $form->field($searchModel, 'CURRENT_BALANCE') ?>
                    </div>
                    <div class="col-md-3">
                        <?php echo $form->field($searchModel, 'UNIT_PRICE') ?>
                    </div>
                </div>


                <?php if (Yii::$app->user->isAdmin) { ?>
                    <h3> Perincian Data</h3>
                    <div class="row">
                        <div class="col-md-4">
                            <?php echo $form->field($searchModel, 'CREATED_BY') ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo $form->field($searchModel, 'CREATED_AT') ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo $form->field($searchModel, 'UPDATED_BY') ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo $form->field($searchModel, 'UPDATED_AT') ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo $form->field($searchModel, 'DELETED') ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo $form->field($searchModel, 'DELETED_AT') ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>



