<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Transactions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transactions-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel-body">
        <div class="row">
            <!--            <div class="col-md-4">
            <?= $form->field($model, 'TYPE')->textInput(['maxlength' => true]) ?>
                        </div>-->
            <div class="col-md-4">
                <?=
                $form->field($model, 'INVENTORY_ID')->widget(kartik\select2\Select2::className(), [
                    'data' => $inventoriesArray,
//                    'language' => 'de',
                    'theme' => 'default',
                    'options' => ['placeholder' => 'Select an inventory ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]);
                ?>
            </div>
            <div class="col-md-4">
                <?=
                $form->field($model, 'VENDOR_ID')->widget(kartik\select2\Select2::className(), [
                    'data' => $vendorsArray,
//                    'language' => 'de',
                    'theme' => 'default',
                    'options' => ['placeholder' => 'Select a vendor ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]);
                ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'ITEMS_QUANTITY')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'ITEMS_TOTAL_PRICE')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?=
                $form->field($model, 'CHECK_DATE')->widget(kartik\widgets\DatePicker::className(), [
                    'options' => ['placeholder' => 'Select issue date ...'],
//                    'type' => 5,
                    'layout' => '{picker}{input}',
                    'pluginOptions' => [
                        'clearBtn' => true,
//                        'assumeNearbyYear' => true,
//                        'minViewMode' => 1,
                        'startDate' => 'today',
////                        'defaultViewDate' => 'year',
//                        'daysOfWeekDisabled' => ['0','6'],
////                        'layout' => '{picker}{input}',
                        'format' => 'd-M-yy',
                        'todayHighlight' => true,
                    ],
                ])
                ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'CHECK_BY')->textInput() ?>
            </div>

        </div>
        <div>
            <?= Html::a(Yii::t('app', 'Batal'), Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Kemaskini'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
