<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Transactions */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Transactions',
]) . $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Transactions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Kemaskini');
?>
<div class="transactions-update">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>

    </div>
</div>
