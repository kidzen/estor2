<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\LaporanPenyelengaraanSisken */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laporan Penyelengaraan Siskens'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info laporan-penyelengaraan-sisken-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>laporan-penyelengaraan-sisken : <?=  Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
    <p>
        <?= Html::a(Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('app', 'Kemaskini'), ['update-attend', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
        'method' => 'post',
        ],
        ]) ?>
    </p>
    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'ID',
            'TAHUN',
            'BULAN',
            'NO_KERJA',
            'ARAHAN_KERJA',
            'JK_JG',
            'BENGKEL_PANEL_ID',
            'NAMA_PANEL',
            'KATEGORI_PANEL',
            'SELENGGARA_ID',
            'TEMPOH_SELENGGARA',
            'ALASAN_ID',
            'ALASAN',
            'TARIKH_ARAHAN',
            'NO_SEBUTHARGA',
            'TARIKH_KENDERAAN_TIBA',
            'TARIKH_JANGKA_SIAP',
            'TARIKH_SEBUTHARGA',
            'STATUS_SEBUTHARGA',
            'NO_DO',
            'TARIKH_SIAP',
            'TEMPOH_JAMINAN',
            'TARIKH_DO',
            'TARIKH_CETAK',
            'STATUS',
            'CATATAN_SEBUTHARGA',
            'CATATAN',
            'NAMA_PIC',
            'TARIKH_PERMOHONAN',
            'ID_KENDERAAN',
            'KATEGORI_KEROSAKAN_ID',
            'KATEGORI_KEROSAKAN',
            'KATEGORI',
            'ISSERVICE',
            'ISPANCIT',
            'ODOMETER_TERKINI',
            'NO_PLAT',
            'MODEL',
            'KOD_JABATAN',
            'NAMA_JABATAN',
            'JUMLAH_KOS',
    ],
    ]) ?>

</div>
