<?php

//use Yii;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\People */

$this->title = Yii::t('app', 'Kemaskini {modelClass} : ', [
            'modelClass' => 'Profil',
        ]) . $model->ID;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Peoples'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
//$this->params['breadcrumbs'][] = Yii::t('app', 'Kemaskini');
?>
<div class="people-update">
    <div class="box box-info people-view">
        <div class="box-header with-border">
            <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>Profil : <?= Html::encode($model->USERNAME) ?></strong></h3>
        </div>
        <div class="box-body">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title"><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
                </div>

                <?=
                $this->render('_form', [
                    'model' => $model, 'rolesArray' => $rolesArray
                ])
                ?>

            </div>
<!--            <p>
            <?=
            Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
            </p>-->
            <?php Pjax::begin(['id' => 'people']) ?>
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'ID',
                    'USERNAME',
                    'EMAIL:email',
                    'mpspProfile.NAMA',
                    'mpspProfile.JAWATAN',
                    'mpspProfile.KETERANGAN',
                    [
                        'attribute' => 'PROFILE_PIC',
                        'format' => ['image', ['width' => '100', 'height' => '100']],
                        'value' => isset($model->PROFILE_PIC) ? Yii::getAlias('@web/') .$model->PROFILE_PIC : null,
                    ],
                    [
                        'label' => 'Url Gambar',
                        'attribute' => 'PROFILE_PIC',
                    ],
                    [
                        'label' => 'Roles',
                        'attribute' => 'roles.NAME',
                        'vAlign' => 'middle',
                    ],
                    [
                        'attribute' => 'DELETED',
                        'vAlign' => 'middle',
                        'value' => $model->DELETED == 1 ? 'Deleted' : 'Active',
                    ],
//                    'DELETED_AT',
//                    'CREATED_AT',
//                    'UPDATED_AT',
//                    'PASSWORD',
//                    'PASSWORD_RESET_TOKEN',
//                    'AUTH_KEY',
                ],
            ])
            ?>
            <?php Pjax::end() ?>

        </div>

    </div>

    <?php
    $this->registerJs(
            '$("document").ready(function(){
        $("#update_people").on("pjax:end", function() {
            $.pjax.reload({container:"#people"});  //Reload GridView
        });
    });'
    );
    ?>

