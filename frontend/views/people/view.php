<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\People */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Peoples'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info people-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>people : <?= Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a(Yii::t('app', 'Kembali'), Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
            <?= Html::a(Yii::t('app', 'Kemaskini'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </p>
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'ID',
                'USERNAME',
                'EMAIL:email',
                'STAFF_NO',
                'mpspProfile.NAMA',
                'mpspProfile.JAWATAN',
                'mpspProfile.KETERANGAN',
                [
                    'attribute' => 'PROFILE_PIC',
                    'format' => ['image', ['width' => '100', 'height' => '100']],
                    'value' => isset($model->PROFILE_PIC) ? Yii::getAlias('@web/') . $model->PROFILE_PIC : null,
                ],
                [
                    'label' => 'Url Gambar',
                    'attribute' => 'PROFILE_PIC',
                ],
                [
                    'label' => 'Roles',
                    'attribute' => 'roles.NAME',
                    'vAlign' => 'middle',
                ],
                [
                    'attribute' => 'DELETED',
                    'vAlign' => 'middle',
                    'value' => $model->DELETED == 1 ? 'Deleted' : 'Active',
                ],
                'DELETED_AT',
                'CREATED_AT',
                'UPDATED_AT',
                'PASSWORD_RESET_TOKEN',
                'AUTH_KEY',
            ],
        ])
        ?>

    </div>
