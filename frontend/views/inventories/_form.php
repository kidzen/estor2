<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Inventories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inventories-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel-body">
        <div class="row">
                        <div class="col-md-4">
                <?= $form->field($model, 'CATEGORY_ID')->dropDownList($categoryArray,['prompt'=> ' -- select --']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'CARD_NO')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'CODE_NO')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'DESCRIPTION')->textInput(['maxlength' => true]) ?>
            </div>
<!--            <div class="col-md-4">
                <?= $form->field($model, 'QUANTITY')->textInput(['maxlength' => true]) ?>
            </div>-->
            <div class="col-md-4">
                <?= $form->field($model, 'MIN_STOCK')->textInput() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'LOCATION')->textInput(['maxlength' => true]) ?>
            </div>
<!--            <div class="col-md-4">
                <?= $form->field($model, 'APPROVED')->textInput() ?>
            </div>-->

        </div>
        <div>
            <?= Html::a(Yii::t('app', 'Batal') , Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Kemaskini'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
