<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\EstorInventories */

//$this->title = $inventory->ID;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Estor Inventories'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
//var_dump($order);
//var_dump($listItem[0]);die();
//die();
?>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Kad Petak</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <!--                    <div class="btn-group">
                                            <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-wrench"></i></button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>-->
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!--/.box-header-->
            <div class="box-body table-responsive">
                <div>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td colspan="2" style="width:25%">Rujuk Kawalan Stok :</th>
                                <td colspan="5" style="width:40%"><?= $inventory->CARD_NO ?></th>
                                <td colspan="2" style="width:18%">Nombor Kod:</th>
                                <td colspan="4" style="width:auto"><?= $inventory->CODE_NO ?></th>
                            </tr>
                            <tr>
                                <td colspan="2">Perihal Stok:</th>
                                <td colspan="11"><?= $inventory->DESCRIPTION ?></th>
                            </tr>
                            <tr>
                                <td colspan="2">Kumpulan Stok:</th>
                                <td colspan="5" style="width:13%"><?= $inventory->category->NAME ?></th>
                                <td colspan="2" style="width:15%">Lokasi Stok:</th>
                                <td colspan="4"><?= $inventory->LOCATION ?></th>
                            </tr>
                        </tbody>
                    </table>
                    <!--/.row-->
                </div>
            </div>
            <div class="box-body table-responsive">
                <div>
                    <table class="kv-grid-table table table-hover table-bordered table-striped table-condensed kv-table-wrap">
                    <!--<table class="table table-bordered table-hover">-->
                        <thead>
                            <!--<tr class="kartik-sheet-style">-->
                            <tr>
                                <th class="text-center" rowspan="2">Bil</th>
                                <th class="text-center" rowspan="2">Tarikh</th>
                                <th class="text-center" rowspan="2">Keterangan</th>
                                <th class="text-center" rowspan="2">No Rujukan</th>
                                <th class="text-center" colspan="4" style="text-align: center">Kuantiti</span></th>
                                <th class="text-center" rowspan="2">Nama Pegawai Stor</th>
                            </tr>
                            <tr>
                                <th class="text-center">Terima</th>
                                <th class="text-center">Seunit (RM)</th>
                                <th class="text-center">Keluar</th>
                                <th class="text-center">Baki</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">/</td>
                                <td class="text-center"></td>
                                <td class="text-center"></td>
                                <td colspan="4" class="text-right">Baki dibawa kehadapan</td>
                                <!--<td class="text-right">baki kad lepas</td>-->
                                <?php if ($transactions) { ?>
                                    <td class="text-center"><?= $transactions[0]->COUNT_CURRENT - $transactions[0]->COUNT_IN + $transactions[0]->COUNT_OUT ?></td>
                                <?php } else { ?>
                                    <td class="center">0</td>
                                <?php } ?>
                            </tr>
                            <?php
                            if (sizeof($transactions) >= 1) {
                                foreach ($transactions as $i => $transaction) {
                                    ?>
                                    <tr>
                                        <td class="text-center"><?= $i + 1 ?></td>
                                        <td class="text-center"><?= $transaction->CHECK_DATE; ?></td>
                                        <td class="text-center"><?php
                                            if ($transaction->TYPE == 1) {
                                                echo 'IN';
                                            } else {
//                                            echo 'REG_NO';
//                                            echo $transaction->ID;
                                                echo $transaction->order->VEHICLE_ID;
//                                            echo $transaction->vehicle->REG_NO;
                                            }
                                            ?></td>
                                        <td class="text-center"><?= $transaction->REFFERENCE; ?></td>
                                        <td class="text-center"><?= $transaction->COUNT_IN; ?></td>
                                        <td class="text-center"><?= $transaction->UNIT_PRICE; ?></td>
                                        <td class="text-center"><?= $transaction->COUNT_OUT; ?></td>
                                        <td class="text-center"><?= $transaction->COUNT_CURRENT; ?></td>
                                        <td class="text-center"><?= $transaction->checkBy->mpspProfile->NAMA ?><?php
                                            if ($transaction->TYPE == 2 && isset($transaction->order->vehicle)) {
                                                echo '<br>Kenderaan :' . $transaction->order->vehicle->REG_NO;
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } else { ?>
                                <?php for ($j = 0; $j < 10; $j++) { ?>
                                    <tr>
                                        <td class="kv-align-center"><?= $j + 1 ?></td>
                                        <td class="text-center"></td>
                                        <td class="text-center"></td>
                                        <td class="text-center"></td>
                                        <td class="text-center"></td>
                                        <td class="text-center"></td>
                                        <td class="text-center"></td>
                                        <td class="text-center"></td>
                                        <td class="text-center">&nbsp;</td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!--/.row-->
                </div>
            </div>
            <!--./box-body-->
            <div class="box-footer">
                <div class="pull-right">
                    <?= yii\widgets\LinkPager::widget(['pagination' => $dataProvider->pagination,]); ?>
                    <?= Html::a('<i class="glyphicon glyphicon-list-alt"></i> Format KEW-PS 4', ['official-form/pdf-kewps4', 'id' => $inventory->ID,'printAll' => true], ['class' => 'btn btn-info', 'target' => '_blank']); ?>
                    <?= Html::a('<i class="glyphicon glyphicon-print"></i> Print', ['inventories/pdf-item-transactions', 'id' => $inventory->ID], ['class' => 'btn btn-info', 'target' => '_blank']); ?>
                </div>

                <!--footer-->
                <!--/.row-->
            </div>
            <!--/.box-footer-->
        </div>
        <!--/.box-->
    </div>
    <!--/.col-->
</div>

