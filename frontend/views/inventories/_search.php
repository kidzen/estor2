<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\InventoriesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inventories-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'CATEGORY_ID') ?>

    <?= $form->field($model, 'CARD_NO') ?>

    <?= $form->field($model, 'CODE_NO') ?>

    <?= $form->field($model, 'DESCRIPTION') ?>

    <?php // echo $form->field($model, 'QUANTITY') ?>

    <?php // echo $form->field($model, 'MIN_STOCK') ?>

    <?php // echo $form->field($model, 'LOCATION') ?>

    <?php // echo $form->field($model, 'APPROVED') ?>

    <?php // echo $form->field($model, 'APPROVED_AT') ?>

    <?php // echo $form->field($model, 'APPROVED_BY') ?>

    <?php // echo $form->field($model, 'CREATED_AT') ?>

    <?php // echo $form->field($model, 'CREATED_BY') ?>

    <?php // echo $form->field($model, 'UPDATED_AT') ?>

    <?php // echo $form->field($model, 'UPDATED_BY') ?>

    <?php // echo $form->field($model, 'DELETED') ?>

    <?php // echo $form->field($model, 'DELETED_AT') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
