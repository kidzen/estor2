<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel-body">
        <div class="row">
                        <div class="col-md-4">
                <?= $form->field($model, 'TRANSACTION_ID')->textInput() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'ORDER_DATE')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'ORDERED_BY')->textInput() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'ORDER_NO')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'APPROVED')->textInput() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'VEHICLE_ID')->textInput() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'REQUIRED_DATE')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'CHECKOUT_DATE')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'CHECKOUT_BY')->textInput() ?>
            </div>

        </div>
        <div>
            <?= Html::a(Yii::t('app', 'Batal') , Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Kemaskini'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
