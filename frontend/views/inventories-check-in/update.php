<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\InventoriesCheckIn */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Inventories Check In',
]) . $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inventories Check Ins'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Kemaskini');
?>
<div class="inventories-check-in-update">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>

    </div>
</div>
