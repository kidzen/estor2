<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VehicleReport */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Vehicle Report',
]) . $model->REG_NO;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vehicle Reports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->REG_NO, 'url' => ['view', 'id' => $model->REG_NO]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Kemaskini');
?>
<div class="vehicle-report-update">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>

    </div>
</div>
