/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



function doSubmit(thisform) {
    var form_mobile = "Please enter Mobile Number.";
    var form_accno = "Please enter No Meter PLN.";
    var form_replymobile = "Please enter No HP Terima SMS.";

    var i = 0;
    var selectedItem = false;
    if (thisform.SI_.length > 0) {
        for (; thisform.SI_ != null && i < thisform.SI_.length; i++) {
            if (thisform.SI_[i].checked == "1") {
                selectedItem = true;
                break;
            }
        }
    } else {
        if (thisform.SI_.checked == "1") {
            selectedItem = true;
        }
    }
    if (!selectedItem) {
        alert("Please select Service.");
        return false;
    }
    
    if (thisform.SI_[i].value == "137") {
        // utility bill , pln

        if (thisform.Remark.value == "" || !thisform.Remark.value.match(/^[0-9]+$/)) {
            focusAndSelect(thisform.Remark);
            alert(form_accno);

            return false;
        }
        if (thisform.Receiver.value == "" || !thisform.Receiver.value.match(/^[0-9]+$/)) {
            focusAndSelect(thisform.Receiver);
            alert(form_replymobile);

            return false;
        }

    } else {
        // mobile bill

        if (thisform.Remark.value == "" || !thisform.Remark.value.match(/^[0-9]+$/)) {
            focusAndSelect(thisform.Remark);
            alert(form_mobile);
            return false;
        }
        if (thisform.MobileTelArea.value != "65" && thisform.Remark.value.length < 9) {
            focusAndSelect(thisform.Remark);
            alert(form_mobile);
            return false;
        }
        if (thisform.MobileTelArea.value == "6" && thisform.Remark.value.charAt(0) != "0") {
            // my
            focusAndSelect(thisform.Remark);
            alert(form_mobile);
            return false;
        }
        if (thisform.MobileTelArea.value == "62" && thisform.Remark.value.charAt(0) != "0") {
            // indo
            focusAndSelect(thisform.Remark);
            alert(form_mobile);
            return false;
        }
        if (thisform.MobileTelArea.value == "880" && thisform.Remark.value.charAt(0) != "0") {
            // vertnam
            focusAndSelect(thisform.Remark);
            alert(form_mobile);
            return false;
        }
        if (thisform.MobileTelArea.value == "65" && !(thisform.Remark.value.charAt(0) == "8" || thisform.Remark.value.charAt(0) == "9")) {
            // sg
            focusAndSelect(thisform.Remark);
            alert(form_mobile);
            return false;
        }
    }
    return true;
}
function pservice(thisform) {
    var si_id = "";

    if (thisform.SI_ != null && thisform.SI_.length > 0) {
        for (var i = 0; i < thisform.SI_.length; i++) {
            if (thisform.SI_[i].checked == "1") {
                si_id = thisform.SI_[i].value;
                break;
            }
        }
    }

    if (si_id == "137") {
        lbl_service.innerText = "No Meter PLN";
        showhideDO(1);
    } else {
        lbl_service.innerText = "Mobile Tel. No.";
        showhideDO(0);
    }
}
function showhideDO(id) {
    var doN = document.getElementById("tab_replymobile_n");
    var doV = document.getElementById("tab_replymobile_v");

    if (id == 1) {
        doN.style.display = 'block';
        doV.style.display = 'block';
    } else {
        doN.style.display = 'none';
        doV.style.display = 'none';
    }
}
window.onload = function () {
    pservice(document.salesform);
    self.focus();
}

var capturedOnLoad = window.onload;
function body_load() {
    if (capturedOnLoad != null) {
        setTimeout(capturedOnLoad, 100);
    }
    addUrlTimestamp();
}