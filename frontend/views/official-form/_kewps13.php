

<div class="box-body table-responsive">
    <div>
        <!--<p><span>Pekeliling Perbendaharaan Malaysia</span><span class="pull-right">AM 6.5 Lampiran A</span></p>-->
        <p class="pull-right form-lampiran"><strong>LAMPIRAN C</strong><br><strong>KEW.PS-13</strong></p>
        <!--<div class="clearfix"></div>-->
        <p class="text-center form-name"><strong>LAPORAN KEDUDUKAN STOK TAHUN <?= $year ?></strong></p>
        <p><strong>KEMENTERIAN/JABATAN: <br>KATEGORI STOR:</strong></p>
        <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
        <!--<table bordered>-->
            <thead>
                <tr>
                    <th class="default text-center" rowspan="4">TAHUN SEMASA</th>
                    <th class="default text-center" colspan="8">KEDUDUKAN STOK</th>
                    <!-- <th class="default text-center" rowspan="4">KADAR PUSINGAN STOK<br>c<br><span><hr style="font-size: 10" width="50%" color="black"></span><br>[(a + d) / 2]</th> -->
                    <th class="default text-center" rowspan="4">KADAR PUSINGAN STOK<br>c<br>------------<br>[(a + d) / 2]</th>
                </tr>
                <tr>
                    <th class="default text-center" colspan="2">Sedia Ada</th>
                    <th class="default text-center" colspan="2">Penerimaan</th>
                    <th class="default text-center" colspan="2">Pengeluaran</th>
                    <th class="default text-center" colspan="2">Stok Semasa</th>
                </tr>
                <tr>
                    <th class="default text-center">Bilangan Stok</th>
                    <th class="default text-center">Jumlah Nilai Stok (RM)</th>
                    <th class="default text-center">Bilangan Stok</th>
                    <th class="default text-center">Jumlah Nilai Stok (RM)</th>
                    <th class="default text-center">Bilangan Stok</th>
                    <th class="default text-center">Jumlah Nilai Stok (RM)</th>
                    <th class="default text-center">Bilangan Stok</th>
                    <th class="default text-center">Jumlah Nilai Stok (RM)</th>
                </tr>
                <tr>
                    <th class="default text-center">(i)</th>
                    <th class="default text-center">(a)</th>
                    <th class="default text-center">(ii)</th>
                    <th class="default text-center">(b)</th>
                    <th class="default text-center">(iii)</th>
                    <th class="default text-center">(c)</th>
                    <th class="default text-center">(i+ii)-(iii)</th>
                    <th class="default text-center">d=(a+b)-(c)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="default">Baki Bawa<br>Hadapan</td>
                    <td class="default" colspan="6">Baki Stok Akhir Tahun <?= $year - 1 ?>:</td>
                    <td class="text-center"> </td>
                    <td class="text-center"> </td>
                    <td class="default text-center">
                    <?php //var_dump($items[0]);die(); ?>
                        <?= isset($items[1]) ? $items[1]['COUNT_CURRENT'] + $items[1]['COUNT_OUT'] - $items[1]['COUNT_IN'] : '0' ?>

                    </td>
                </tr>
                <?php
                for ($j = 1; $j < 5; $j++) {
                    if ($j == 1) {
                        $quarterlabel = 'Pertama';
                    } else if ($j == 2) {
                        $quarterlabel = 'Kedua';
                    } else if ($j == 3) {
                        $quarterlabel = 'Ketiga';
                    } else if ($j == 4) {
                        $quarterlabel = 'Keempat';
                    } if (isset($items[$j])) {
                        ?>
                        <tr>
                            <td class="default">Suku Tahun<br><?= $quarterlabel ?></td>
                            <td class="i text-center" style="width: 9%;"><?= $items[$j]['COUNT_CURRENT'] + $items[$j]['COUNT_OUT'] - $items[$j]['COUNT_IN'] ?></td>
                            <td class="a text-center" style="width: 9%;"><?= $items[$j]['PRICE_CURRENT'] + $items[$j]['PRICE_OUT'] - $items[$j]['PRICE_IN'] ?></td>
                            <td class="ii text-center" style="width: 9%;"><?= $items[$j]['COUNT_IN'] ?></td>
                            <td class="b text-center" style="width: 9%;"><?= $items[$j]['PRICE_IN'] ?></td>
                            <td class="iii text-center" style="width: 9%;"><?= $items[$j]['COUNT_OUT'] ?></td>
                            <td class="c text-center" style="width: 9%;"><?= $items[$j]['PRICE_OUT'] ?></td>
                            <td class="i+ii-iii text-center" style="width: 12%;"><?= $items[$j]['COUNT_CURRENT'] ?></td>
                            <td class="d text-center" style="width: 12%;"><?= $items[$j]['PRICE_CURRENT'] ?></td>
                            <?php
                                    $a = $items[$j]['PRICE_CURRENT'] + $items[$j]['PRICE_OUT'] - $items[$j]['PRICE_IN'];
                                    $b = $items[$j]['PRICE_IN'];
                                    $c = $items[$j]['PRICE_OUT'];
                                    $d = $items[$j]['PRICE_CURRENT'];
                                    $total = $c / (($a + $d) / 2);
                            ?>
                            <td class="text-center"><?= round($total,4) ?></td>
                        </tr>
                    <?php } else { ?>
                        <tr>
                            <td class="default">Suku Tahun<br><?= $quarterlabel ?></td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center">&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                <tr>
                    <td class="default" colspan="3">Nilai Tahunan</td>
                    <td class="text-center"> </td>
                    <td class="text-center"> </td>
                    <td class="text-center"> </td>
                    <td class="text-center"> </td>
                    <td class="text-center" colspan="2">Kadar Pusingan Stok Tahunan adalah: </td>
                </tr>
            </tbody>

        </table><br>
        <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
        <!--<table bordered>-->
            <tbody>

                <tr>
                    <td class="" colspan="5">
                        <strong>Disediakan oleh: </strong><br><br><br>
                        ...............................................................<br>
                        (Tandatangan Pegawai Stor)<br>
                        <strong>Nama: </strong><br>
                        <strong>Jawatan: </strong><br>
                        <strong>Jabatan: </strong><br>
                        <strong>Tarikh:	</strong><br>

                    </td>
                    <td class="" colspan="5">
                        <strong>(DILULUSKAN/ TIDAK DILULUSKAN)*</strong><br><br><br>
                        ...............................................................<br>
                        (Tandatangan Ketua Jabatan)<br>
                        <strong>Nama: </strong><br>
                        <strong>Jawatan: </strong><br>
                        <strong>Jabatan: </strong><br>
                        <strong>Tarikh:	</strong><br>

                    </td>
                </tr>
            </tbody>

        </table>
    </div>
</div>
