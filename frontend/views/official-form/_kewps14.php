


<div class="box-body table-responsive">
    <div>
        <!--<p><span>Pekeliling Perbendaharaan Malaysia</span><span class="pull-right">AM 6.5 Lampiran A</span></p>-->
        <p class="pull-right form-lampiran"><strong>LAMPIRAN A</strong><br><strong>KEW.PS-14</strong></p>
        <!--<div class="clearfix"></div>-->
        <p class="text-center form-name"><strong>LAPORAN PEMERIKSAAN/ VERIFIKASI STOR TAHUN <?= 2015 ?></strong></p>
        <p><strong>Kementerian/ Jabatan:</strong></p>
        <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
        <!--<table bordered>-->
            <thead>
                <tr>
                    <th class="col-1 row-4 text-center info " colspan="3">DIISI OLEH PEGAWAI STOR</th>
                    <th class="col-2 row-4 text-center info right-border-bold" colspan="4">DIISI OLEH PEGAWAI PEMERIKSA</th>
                    <th class="col-3 row-4 text-center info right-border-bold" colspan="7">DIISI OLEH PEMVERIFIKASI</th>
                </tr>
                <tr>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">No. Kad Kawalan Stok</th>
                    <th class="col-2 vertical-align-top text-center text-center info" rowspan="2">Perihal Stok</th>
                    <th class="col-3 vertical-align-top text-center text-center info" rowspan="2">Kos Seunit (RM)</th>

                    <th class="col-4 vertical-align-top text-center text-center info" colspan="4">Kuantiti Stok</th>


                    <th class="no-border-right text-center vertical-align-top text-center text-center info" colspan="4">Kuantiti Stok</th>
                    <th class="no-border-right text-center vertical-align-top text-center text-center info" colspan="2">Penemuan Keadaan Stok (usang/rosak/tidakdigunakan lagi/tidak aktif/luput tempoh/ dll)</th>
                    <th class="no-border-right text-center vertical-align-top text-center text-center info" rowspan="2">Syor/Cadangan (lupus/hapus kira/pelarasan stok/pindahan stok)</th>

                </tr>
                <tr>
                    <th class="col-1 vertical-align-top text-center text-center info">Fizikal Stok</th>
                    <th class="col-2 vertical-align-top text-center text-center info">Baki Di Kad Kawalan Stok</th>
                    <th class="col-3 vertical-align-top text-center text-center info">lebih</th>
                    <th class="col-4 vertical-align-top text-center text-center info">Kurang</th>
                    <th class="col-1 vertical-align-top text-center text-center info">Fizikal Stok</th>
                    <th class="col-2 vertical-align-top text-center text-center info">Baki Di Kad Kawalan Stok</th>
                    <th class="col-3 vertical-align-top text-center text-center info">lebih</th>
                    <th class="col-1 vertical-align-top text-center text-center info">Kurang</th>
                    <th class="col-2 vertical-align-top text-center text-center info">Kuantiti</th>
                    <th class="col-3 vertical-align-top text-center text-center info">Justifikasi</th>
                </tr>
            </thead>
            <tbody>
                <?php if (sizeof($items) >= 1) { ?>
                    <?php foreach ($items as $item) { ?>
                        <tr>
                            <td class="col-1 pull-left"><?= $item->inventory->CODE_NO ?></td>
                            <td class="col-2 pull-left" colspan="2"><?= $item->inventory->DESCRIPTION ?></td>
                            <td class="col-3 pull-left"><?= $item->RQ_QUANTITY ?></td>
                            <td class="col-4 pull-left"><?= $item->inventory->CARD_NO ?></td>
                            <td class="col-5 pull-left"><?= $item->CURRENT_BALANCE ?></td>
                            <td class="col-6 pull-left"><?= $item->APP_QUANTITY ?></td>
                            <td class="col-7 pull-left"><?= $item->UNIT_PRICE / $item->APP_QUANTITY ?></td>
                            <td class="no-border-right text-center pull-left"><?= $item->UNIT_PRICE ?></td>
                            <td class="col-9 pull-left"><?= $item->APP_QUANTITY ?></td>
                            <td class="col-10 pull-left"></td>
                        </tr>
                    <?php } ?>
                    <?php for ($j = sizeof($items); $j < 5; $j++) { ?>
                        <tr>
                            <td class="col-1 pull-left"> </td>
                            <td class="col-2 pull-left" colspan="2"> </td>
                            <td class="col-3 pull-left"> </td>
                            <td class="col-4 pull-left"> </td>
                            <td class="col-5 pull-left"> </td>
                            <td class="col-6 pull-left"> </td>
                            <td class="col-7 pull-left"> </td>
                            <td class="no-border-right text-center pull-left"> </td>
                            <td class="col-9 pull-left"> </td>
                            <td class="col-10 pull-left">&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <?php for ($j = 0; $j < 3; $j++) { ?>
                        <tr>
                            <td class="col-1 pull-left"> </td>
                            <td class="col-2 pull-left"> </td>
                            <td class="col-3 pull-left"> </td>
                            <td class="col-4 pull-left"> </td>
                            <td class="col-5 pull-left"> </td>
                            <td class="col-6 pull-left"> </td>
                            <td class="col-7 pull-left"> </td>
                            <td class="no-border-right text-center pull-left"> </td>
                            <td class="col-9 pull-left"> </td>
                            <td class="col-10 pull-left">&nbsp;</td>
                            <td class="col-7 pull-left"> </td>
                            <td class="no-border-right text-center pull-left"> </td>
                            <td class="col-9 pull-left"> </td>
                            <td class="col-10 pull-left">&nbsp;</td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td class="col-1 pull-left" colspan="3">JUMLAH</td>
                        <td class="col-4 pull-left"> </td>
                        <td class="col-5 pull-left"> </td>
                        <td class="col-6 pull-left"> </td>
                        <td class="col-7 pull-left"> </td>
                        <td class="no-border-right text-center pull-left"> </td>
                        <td class="col-9 pull-left"> </td>
                        <td class="col-10 pull-left">&nbsp;</td>
                        <td class="col-7 pull-left"> </td>
                        <td class="no-border-right text-center pull-left"> </td>
                        <td class="col-9 pull-left"> </td>
                        <td class="col-10 pull-left">&nbsp;</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <p><strong>JUMLAH</strong></p>
        <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
            <thead>
                <tr>
                    <th class="col-1 row-4" colspan="12">Dilengkapkan oleh Pegawai Pemeriksa</th>
                    <th class="col-2 row-4 " colspan="12">Dilengkapkan oleh Pegawai Pemverifikasi</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="col-10 pull-left" colspan="2">&nbsp;</td>
                    <td class="col-10 pull-left" colspan="4">&nbsp;</td>
                    <td class="col-10 pull-left" colspan="2">&nbsp;</td>
                    <td class="col-10 pull-left" colspan="4">&nbsp;</td>
                    <td class="col-10 pull-left" colspan="2">&nbsp;</td>
                    <td class="col-10 pull-left" colspan="4">&nbsp;</td>
                    <td class="col-10 pull-left" colspan="2">&nbsp;</td>
                    <td class="col-10 pull-left" colspan="4">&nbsp;</td>
                </tr>
            </tbody>
        </table><br>
        <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
            <tbody>
                <tr>
                    <th class="col-1 row-4">Dilengkapkan oleh Pegawai Pemeriksa</th>
                    <th class="col-2 row-4">Dilengkapkan oleh Pegawai Pemverifikasi</th>
                </tr>
            </tbody>
        </table>
        <p>Nota: Ruang Tandatangan boleh di lampiran terakhir </p>


    </div>
</div>
