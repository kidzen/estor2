<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\InventoryItemsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inventory-items-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'INVENTORY_ID') ?>

    <?= $form->field($model, 'CHECKIN_TRANSACTION_ID') ?>

    <?= $form->field($model, 'CHECKOUT_TRANSACTION_ID') ?>

    <?= $form->field($model, 'SKU') ?>

    <?php // echo $form->field($model, 'UNIT_PRICE') ?>

    <?php // echo $form->field($model, 'CREATED_AT') ?>

    <?php // echo $form->field($model, 'UPDATED_AT') ?>

    <?php // echo $form->field($model, 'CREATED_BY') ?>

    <?php // echo $form->field($model, 'UPDATED_BY') ?>

    <?php // echo $form->field($model, 'DELETED') ?>

    <?php // echo $form->field($model, 'DELETED_AT') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
