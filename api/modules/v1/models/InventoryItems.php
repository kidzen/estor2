<?php

namespace api\modules\v1\models;
use \yii\db\ActiveRecord;

use Yii;
// use yii\behaviors\TimestampBehavior;
// use yii\behaviors\BlameableBehavior;
// use yii\db\Expression;

/**
 * This is the model class for table "INVENTORY_ITEMS".
 *
 * @property string $ID
 * @property integer $INVENTORY_ID
 * @property integer $CHECKIN_TRANSACTION_ID
 * @property integer $CHECKOUT_TRANSACTION_ID
 * @property string $SKU
 * @property string $UNIT_PRICE
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class InventoryItems extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    // const STATUS_DELETED = 1;
    // const STATUS_ACTIVE = 0;

    public static function primaryKey()
    {
        return ['ID'];
    }

    // public function behaviors() {
    //     return [
    //         [
    //             'class' => TimestampBehavior::className(),
    //             'createdAtAttribute' => 'CREATED_AT',
    //             'updatedAtAttribute' => 'UPDATED_AT',
    //             'value' => date('d-M-y h.i.s a'),
    //         ],
    //         [
    //             'class' => BlameableBehavior::className(),
    //             'createdByAttribute' => 'CREATED_BY',
    //             'updatedByAttribute' => 'UPDATED_BY',
    //             'value' => Yii::$app->user->id,
    //         ],
    //     ];
    // }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'INVENTORY_ITEMS';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
        [['UNIT_PRICE'], 'required', 'on' => 'entry-create'],
        [['ID', 'UNIT_PRICE'], 'number'],
        [['INVENTORY_ID', 'CHECKIN_TRANSACTION_ID', 'CHECKOUT_TRANSACTION_ID', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
        [['CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
        [['SKU'], 'string', 'max' => 50],
        [['ID', 'SKU'], 'unique'],
        [['inventory.ID', 'SKU'], 'safe'],
        ];
    }
    public function getCheckInTransaction() {
        return $this->hasOne(\common\models\InventoriesCheckIn::className(), ['ID' => 'CHECKIN_TRANSACTION_ID']);
    }

    public function getCheckOutTransaction() {
        return $this->hasOne(\common\models\OrderItems::className(), ['ID' => 'CHECKOUT_TRANSACTION_ID']);
    }

    public function getTransactionIn() {
        return $this->hasOne(Transactions::className(), ['ID' => 'TRANSACTION_ID'])
        ->via('checkInTransaction');
    }

    public function getTransactionOut() {
        return $this->hasOne(Transactions::className(), ['ID' => 'TRANSACTION_ID'])
        ->via('orders');
    }

    public function getInventory() {
        return $this->hasOne(\common\models\Inventories::className(), ['ID' => 'INVENTORY_ID'])
        ->via('checkInTransaction');
    }

    public function getCategory() {
        return $this->hasOne(Categories::className(), ['ID' => 'CATEGORY_ID'])
        ->via('inventory');
    }

    public function getOrders() {
        return $this->hasOne(\common\models\Orders::className(), ['ID' => 'ORDER_ID'])
        ->via('checkOutTransaction');
    }
    // public function getRequestOrderItems() {
    //     return $this->hasOne(OrderItems::className(), ['INVENTORY_ID' => 'ID'])
    //     ->via('inventory');
    // }

    public function getRequestInventory() {
        return $this->hasOne(Inventories::className(), ['INVENTORY_ID' => 'INVENTORY_ID'])
        ->via('orderItems');
    }

    public function getPeople($id) {
        return $this->hasOne(People::className(), ['ID' => $id]);
    }

    public function getCreator() {
        return $this->hasOne(People::className(), ['ID' => 'CREATED_BY']);
    }

    public function getUpdator() {
        return $this->hasOne(People::className(), ['ID' => 'UPDATED_BY']);
    }

    public function getSku() {
        return $this->SKU;
    }

    public static function findByInventoryId($id) {
        return static::find()->joinWith('inventory')->where(['INVENTORIES.ID' => $id])->all();
    }

    public static function findByOrderItemId($id) {
        return static::find()->joinWith('checkOutTransaction')->where(['CHECKOUT_TRANSACTION_ID' => $id])->all();
    }

    public static function findByIdStatusInStore($id) {
        return static::find($id)->where(['CHECKOUT_TRANSACTION_ID' => null])->one();
    }

    public static function checkoutFifo($inventoryId) {
        return static::find()
        ->select('INVENTORY_ITEMS.ID,CHECKIN_TRANSACTION_ID,CHECKOUT_TRANSACTION_ID')
        ->joinWith('inventory')
        ->where(['INVENTORIES.ID' => $inventoryId])
        ->andWhere(['CHECKOUT_TRANSACTION_ID' => null])
        ->orderBy('INVENTORY_ITEMS.CREATED_AT')
        ->asArray()
        ->all();
    }

    public static function printSku($id, $printInventoryCode = 0) {
        $model = static::find()->where(['in', 'ID' ,$id])->with('inventory')->asArray()->all();
        $mpdf = static::generatePdfBarcode($model, $printInventoryCode);
        $mpdf->Output();
    }

    public static function generatePdfBarcode($model, $printInventoryCode) {
        $mpdf = new \mPDF('utf-8', array(50, 20), 0, '', 1, 1, 5, 1, 2, 0, 'P');
        $mpdf->defaultheaderfontsize = 5;
        $mpdf->defaultfooterfontsize = 5;
        $mpdf->SetHeader('|© Majlis Perbandaran Seberang Perai|');
        $mpdf->WriteHTML(''
            . 'barcode {padding: 1.5mm;margin: 0;vertical-align: top;color: #000044;}'
            . '.barcodecell {text-align: center;vertical-align: middle;color:white;text-align:center;padding:2px;}'
            . '.barcodetext {text-align:center;font-size:6px;}'
            , 1);
        foreach ($model as $data) {
            $mpdf->WriteHTML(''
                . '<div style="text-align:center;font-size:8px">' . $data['inventory']['DESCRIPTION'] . '</div>'
                . '<div class="barcodecell"><barcode class="barcode" code="' . $data['SKU'] . '" type="C128A" size="0.48" height="1.5"/></div>'
                . '<div class="barcodetext">SKU : ' . $data['SKU'] . '</div>'
                , 2);
            $printInventoryCode = 1;
            if ($printInventoryCode) {
                $mpdf->AddPage();
                $mpdf->WriteHTML(''
                    . '<div style="text-align:center;font-size:8px">' . $data['inventory']['DESCRIPTION'] . '</div>'
                    . '<div class="barcodecell"><barcode class="barcode" code="' . $data['inventory']['CODE_NO'] . '" type="C128A" size="0.5" height="1.5"/></div>'
                    . '<div class="barcodetext">CODE : ' . $data['inventory']['CODE_NO'] . '</div>'
                    , 2);
            }
        }
        return $mpdf;
    }


}
