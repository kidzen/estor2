<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Inventories;

/**
 * backend\models\search\InventoriesSearch represents the model behind the search form about `backend\models\Inventories`.
 */
 class InventoriesSearch extends Inventories
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'QUANTITY'], 'number'],
            [['CATEGORY_ID', 'MIN_STOCK', 'APPROVED', 'APPROVED_BY', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['CARD_NO', 'CODE_NO', 'DESCRIPTION', 'LOCATION', 'APPROVED_AT', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Inventories::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'CATEGORY_ID' => $this->CATEGORY_ID,
            'QUANTITY' => $this->QUANTITY,
            'MIN_STOCK' => $this->MIN_STOCK,
            'APPROVED' => $this->APPROVED,
            'APPROVED_AT' => $this->APPROVED_AT,
            'APPROVED_BY' => $this->APPROVED_BY,
            'CREATED_AT' => $this->CREATED_AT,
            'CREATED_BY' => $this->CREATED_BY,
            'UPDATED_AT' => $this->UPDATED_AT,
            'UPDATED_BY' => $this->UPDATED_BY,
            'DELETED' => $this->DELETED,
            'DELETED_AT' => $this->DELETED_AT,
        ]);

        $query->andFilterWhere(['like', 'CARD_NO', $this->CARD_NO])
            ->andFilterWhere(['like', 'CODE_NO', $this->CODE_NO])
            ->andFilterWhere(['like', 'DESCRIPTION', $this->DESCRIPTION])
            ->andFilterWhere(['like', 'LOCATION', $this->LOCATION]);

        return $dataProvider;
    }
}
