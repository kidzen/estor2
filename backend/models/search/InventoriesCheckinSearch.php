<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\InventoriesCheckin;

/**
 * backend\models\search\InventoriesCheckinSearch represents the model behind the search form about `backend\models\InventoriesCheckin`.
 */
 class InventoriesCheckinSearch extends InventoriesCheckin
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'ITEMS_QUANTITY', 'ITEMS_TOTAL_PRICE'], 'number'],
            [['TRANSACTION_ID', 'INVENTORY_ID', 'VENDOR_ID', 'CHECK_BY', 'APPROVED', 'APPROVED_BY', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['CHECK_DATE', 'APPROVED_AT', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InventoriesCheckin::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'TRANSACTION_ID' => $this->TRANSACTION_ID,
            'INVENTORY_ID' => $this->INVENTORY_ID,
            'VENDOR_ID' => $this->VENDOR_ID,
            'ITEMS_QUANTITY' => $this->ITEMS_QUANTITY,
            'ITEMS_TOTAL_PRICE' => $this->ITEMS_TOTAL_PRICE,
            'CHECK_BY' => $this->CHECK_BY,
            'APPROVED' => $this->APPROVED,
            'APPROVED_BY' => $this->APPROVED_BY,
            'APPROVED_AT' => $this->APPROVED_AT,
            'CREATED_AT' => $this->CREATED_AT,
            'UPDATED_AT' => $this->UPDATED_AT,
            'CREATED_BY' => $this->CREATED_BY,
            'UPDATED_BY' => $this->UPDATED_BY,
            'DELETED' => $this->DELETED,
            'DELETED_AT' => $this->DELETED_AT,
        ]);

        $query->andFilterWhere(['like', 'CHECK_DATE', $this->CHECK_DATE]);

        return $dataProvider;
    }
}
