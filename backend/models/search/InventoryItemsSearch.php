<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\InventoryItems;

/**
 * backend\models\search\InventoryItemsSearch represents the model behind the search form about `backend\models\InventoryItems`.
 */
 class InventoryItemsSearch extends InventoryItems
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'UNIT_PRICE'], 'number'],
            [['INVENTORY_ID', 'CHECKIN_TRANSACTION_ID', 'CHECKOUT_TRANSACTION_ID', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['SKU', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InventoryItems::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'INVENTORY_ID' => $this->INVENTORY_ID,
            'CHECKIN_TRANSACTION_ID' => $this->CHECKIN_TRANSACTION_ID,
            'CHECKOUT_TRANSACTION_ID' => $this->CHECKOUT_TRANSACTION_ID,
            'UNIT_PRICE' => $this->UNIT_PRICE,
            'CREATED_AT' => $this->CREATED_AT,
            'UPDATED_AT' => $this->UPDATED_AT,
            'CREATED_BY' => $this->CREATED_BY,
            'UPDATED_BY' => $this->UPDATED_BY,
            'DELETED' => $this->DELETED,
            'DELETED_AT' => $this->DELETED_AT,
        ]);

        $query->andFilterWhere(['like', 'SKU', $this->SKU]);

        return $dataProvider;
    }
}
