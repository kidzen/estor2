<?php

namespace backend\models\query;

/**
 * This is the ActiveQuery class for [[\backend\models\query\Orders]].
 *
 * @see \backend\models\query\Orders
 */
class OrdersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\models\query\Orders[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\models\query\Orders|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
