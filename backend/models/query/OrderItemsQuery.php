<?php

namespace backend\models\query;

/**
 * This is the ActiveQuery class for [[\backend\models\query\OrderItems]].
 *
 * @see \backend\models\query\OrderItems
 */
class OrderItemsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\models\query\OrderItems[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\models\query\OrderItems|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
