<?php

namespace backend\models;

use Yii;
use \backend\models\base\Categories as BaseCategories;

/**
 * This is the model class for table "CATEGORIES".
 */
class Categories extends BaseCategories
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['ID'], 'number'],
            [['DELETED', 'CREATED_BY', 'UPDATED_BY'], 'integer'],
            [['DELETED_AT', 'CREATED_AT', 'UPDATED_AT'], 'safe'],
            [['NAME'], 'string', 'max' => 255],
            [['ID', 'ID', 'ID'], 'unique', 'targetAttribute' => ['ID', 'ID', 'ID'], 'message' => 'The combination of  and ID has already been taken.'],
            [['ID', 'ID', 'DELETED', 'DELETED'], 'unique', 'targetAttribute' => ['ID', 'ID', 'DELETED', 'DELETED'], 'message' => 'The combination of ID and Deleted has already been taken.']
        ]);
    }
	
}
