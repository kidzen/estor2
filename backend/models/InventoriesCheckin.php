<?php

namespace backend\models;

use Yii;
use \backend\models\base\InventoriesCheckin as BaseInventoriesCheckin;

/**
 * This is the model class for table "INVENTORIES_CHECKIN".
 */
class InventoriesCheckin extends BaseInventoriesCheckin
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['ID', 'ITEMS_QUANTITY', 'ITEMS_TOTAL_PRICE'], 'number'],
            [['TRANSACTION_ID', 'INVENTORY_ID'], 'required'],
            [['TRANSACTION_ID', 'INVENTORY_ID', 'VENDOR_ID', 'CHECK_BY', 'APPROVED', 'APPROVED_BY', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['APPROVED_AT', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
            [['CHECK_DATE'], 'string', 'max' => 7],
            [['ID', 'ID', 'ID'], 'unique', 'targetAttribute' => ['ID', 'ID', 'ID'], 'message' => 'The combination of  and ID has already been taken.'],
            [['ID', 'ID', 'DELETED', 'DELETED'], 'unique', 'targetAttribute' => ['ID', 'ID', 'DELETED', 'DELETED'], 'message' => 'The combination of ID and Deleted has already been taken.']
        ]);
    }
	
}
