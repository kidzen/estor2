<?php

namespace backend\models\base;

use Yii;

/**
 * This is the base model class for table "INVENTORIES_CHECKIN".
 *
 * @property string $ID
 * @property integer $TRANSACTION_ID
 * @property integer $INVENTORY_ID
 * @property integer $VENDOR_ID
 * @property string $ITEMS_QUANTITY
 * @property string $ITEMS_TOTAL_PRICE
 * @property string $CHECK_DATE
 * @property integer $CHECK_BY
 * @property integer $APPROVED
 * @property integer $APPROVED_BY
 * @property string $APPROVED_AT
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class InventoriesCheckin extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'ITEMS_QUANTITY', 'ITEMS_TOTAL_PRICE'], 'number'],
            [['TRANSACTION_ID', 'INVENTORY_ID'], 'required'],
            [['TRANSACTION_ID', 'INVENTORY_ID', 'VENDOR_ID', 'CHECK_BY', 'APPROVED', 'APPROVED_BY', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['APPROVED_AT', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
            [['CHECK_DATE'], 'string', 'max' => 7],
            [['ID', 'ID', 'ID'], 'unique', 'targetAttribute' => ['ID', 'ID', 'ID'], 'message' => 'The combination of  and ID has already been taken.'],
            [['ID', 'ID', 'DELETED', 'DELETED'], 'unique', 'targetAttribute' => ['ID', 'ID', 'DELETED', 'DELETED'], 'message' => 'The combination of ID and Deleted has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'INVENTORIES_CHECKIN';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'TRANSACTION_ID' => 'Transaction  ID',
            'INVENTORY_ID' => 'Inventory  ID',
            'VENDOR_ID' => 'Vendor  ID',
            'ITEMS_QUANTITY' => 'Items  Quantity',
            'ITEMS_TOTAL_PRICE' => 'Items  Total  Price',
            'CHECK_DATE' => 'Check  Date',
            'CHECK_BY' => 'Check  By',
            'APPROVED' => 'Approved',
            'APPROVED_BY' => 'Approved  By',
            'APPROVED_AT' => 'Approved  At',
            'CREATED_AT' => 'Created  At',
            'UPDATED_AT' => 'Updated  At',
            'CREATED_BY' => 'Created  By',
            'UPDATED_BY' => 'Updated  By',
            'DELETED' => 'Deleted',
            'DELETED_AT' => 'Deleted  At',
        ];
    }


    /**
     * @inheritdoc
     * @return \backend\models\query\InventoriesCheckinQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\query\InventoriesCheckinQuery(get_called_class());
    }
}
