<?php

namespace backend\models\base;

use Yii;

/**
 * This is the base model class for table "CATEGORIES".
 *
 * @property string $ID
 * @property string $NAME
 * @property integer $DELETED
 * @property string $DELETED_AT
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 */
class Categories extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'number'],
            [['DELETED', 'CREATED_BY', 'UPDATED_BY'], 'integer'],
            [['DELETED_AT', 'CREATED_AT', 'UPDATED_AT'], 'safe'],
            [['NAME'], 'string', 'max' => 255],
            [['ID', 'ID', 'ID'], 'unique', 'targetAttribute' => ['ID', 'ID', 'ID'], 'message' => 'The combination of  and ID has already been taken.'],
            [['ID', 'ID', 'DELETED', 'DELETED'], 'unique', 'targetAttribute' => ['ID', 'ID', 'DELETED', 'DELETED'], 'message' => 'The combination of ID and Deleted has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CATEGORIES';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'NAME' => 'Name',
            'DELETED' => 'Deleted',
            'DELETED_AT' => 'Deleted  At',
            'CREATED_AT' => 'Created  At',
            'UPDATED_AT' => 'Updated  At',
            'CREATED_BY' => 'Created  By',
            'UPDATED_BY' => 'Updated  By',
        ];
    }


    /**
     * @inheritdoc
     * @return \backend\models\query\CategoriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\query\CategoriesQuery(get_called_class());
    }
}
