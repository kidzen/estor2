<?php

namespace backend\models\base;

use Yii;

/**
 * This is the base model class for table "INVENTORIES".
 *
 * @property string $ID
 * @property integer $CATEGORY_ID
 * @property string $CARD_NO
 * @property string $CODE_NO
 * @property string $DESCRIPTION
 * @property string $QUANTITY
 * @property integer $MIN_STOCK
 * @property string $LOCATION
 * @property integer $APPROVED
 * @property string $APPROVED_AT
 * @property integer $APPROVED_BY
 * @property string $CREATED_AT
 * @property integer $CREATED_BY
 * @property string $UPDATED_AT
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class Inventories extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'QUANTITY'], 'number'],
            [['CATEGORY_ID'], 'required'],
            [['CATEGORY_ID', 'MIN_STOCK', 'APPROVED', 'APPROVED_BY', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['APPROVED_AT', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
            [['CARD_NO', 'CODE_NO', 'DESCRIPTION', 'LOCATION'], 'string', 'max' => 255],
            [['ID', 'ID', 'ID'], 'unique', 'targetAttribute' => ['ID', 'ID', 'ID'], 'message' => 'The combination of  and ID has already been taken.'],
            [['ID', 'ID', 'DELETED', 'DELETED'], 'unique', 'targetAttribute' => ['ID', 'ID', 'DELETED', 'DELETED'], 'message' => 'The combination of ID and Deleted has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'INVENTORIES';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CATEGORY_ID' => 'Category  ID',
            'CARD_NO' => 'Card  No',
            'CODE_NO' => 'Code  No',
            'DESCRIPTION' => 'Description',
            'QUANTITY' => 'Quantity',
            'MIN_STOCK' => 'Min  Stock',
            'LOCATION' => 'Location',
            'APPROVED' => 'Approved',
            'APPROVED_AT' => 'Approved  At',
            'APPROVED_BY' => 'Approved  By',
            'CREATED_AT' => 'Created  At',
            'CREATED_BY' => 'Created  By',
            'UPDATED_AT' => 'Updated  At',
            'UPDATED_BY' => 'Updated  By',
            'DELETED' => 'Deleted',
            'DELETED_AT' => 'Deleted  At',
        ];
    }


    /**
     * @inheritdoc
     * @return \backend\models\query\InventoriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\query\InventoriesQuery(get_called_class());
    }
}
