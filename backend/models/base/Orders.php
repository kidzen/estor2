<?php

namespace backend\models\base;

use Yii;

/**
 * This is the base model class for table "ORDERS".
 *
 * @property string $ID
 * @property integer $TRANSACTION_ID
 * @property integer $ARAHAN_KERJA_ID
 * @property string $ORDER_DATE
 * @property string $ORDERED_BY
 * @property string $ORDER_NO
 * @property integer $APPROVED
 * @property integer $APPROVED_BY
 * @property string $APPROVED_AT
 * @property integer $VEHICLE_ID
 * @property string $REQUIRED_DATE
 * @property string $CHECKOUT_DATE
 * @property integer $CHECKOUT_BY
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class Orders extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'number'],
            [['TRANSACTION_ID'], 'required'],
            [['TRANSACTION_ID', 'ARAHAN_KERJA_ID', 'APPROVED', 'APPROVED_BY', 'VEHICLE_ID', 'CHECKOUT_BY', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['APPROVED_AT', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
            [['ORDER_DATE', 'REQUIRED_DATE', 'CHECKOUT_DATE'], 'string', 'max' => 7],
            [['ORDERED_BY', 'ORDER_NO'], 'string', 'max' => 20],
            [['ID', 'ID', 'DELETED', 'DELETED'], 'unique', 'targetAttribute' => ['ID', 'ID', 'DELETED', 'DELETED'], 'message' => 'The combination of ID and Deleted has already been taken.'],
            [['ID', 'ID', 'ID'], 'unique', 'targetAttribute' => ['ID', 'ID', 'ID'], 'message' => 'The combination of  and ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ORDERS';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'TRANSACTION_ID' => 'Transaction  ID',
            'ARAHAN_KERJA_ID' => 'Arahan  Kerja  ID',
            'ORDER_DATE' => 'Order  Date',
            'ORDERED_BY' => 'Ordered  By',
            'ORDER_NO' => 'Order  No',
            'APPROVED' => 'Approved',
            'APPROVED_BY' => 'Approved  By',
            'APPROVED_AT' => 'Approved  At',
            'VEHICLE_ID' => 'Vehicle  ID',
            'REQUIRED_DATE' => 'Required  Date',
            'CHECKOUT_DATE' => 'Checkout  Date',
            'CHECKOUT_BY' => 'Checkout  By',
            'CREATED_AT' => 'Created  At',
            'UPDATED_AT' => 'Updated  At',
            'CREATED_BY' => 'Created  By',
            'UPDATED_BY' => 'Updated  By',
            'DELETED' => 'Deleted',
            'DELETED_AT' => 'Deleted  At',
        ];
    }


    /**
     * @inheritdoc
     * @return \backend\models\query\OrdersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\query\OrdersQuery(get_called_class());
    }
}
