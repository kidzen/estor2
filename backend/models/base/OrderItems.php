<?php

namespace backend\models\base;

use Yii;

/**
 * This is the base model class for table "ORDER_ITEMS".
 *
 * @property string $ID
 * @property integer $INVENTORY_ID
 * @property integer $ORDER_ID
 * @property string $RQ_QUANTITY
 * @property string $APP_QUANTITY
 * @property string $CURRENT_BALANCE
 * @property string $UNIT_PRICE
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class OrderItems extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'RQ_QUANTITY', 'APP_QUANTITY', 'CURRENT_BALANCE', 'UNIT_PRICE'], 'number'],
            [['INVENTORY_ID', 'ORDER_ID'], 'required'],
            [['INVENTORY_ID', 'ORDER_ID', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
            [['ID', 'ID', 'DELETED', 'DELETED'], 'unique', 'targetAttribute' => ['ID', 'ID', 'DELETED', 'DELETED'], 'message' => 'The combination of ID and Deleted has already been taken.'],
            [['ID', 'ID', 'ID', 'ID'], 'unique', 'targetAttribute' => ['ID', 'ID', 'ID', 'ID'], 'message' => 'The combination of  and ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ORDER_ITEMS';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'INVENTORY_ID' => 'Inventory  ID',
            'ORDER_ID' => 'Order  ID',
            'RQ_QUANTITY' => 'Rq  Quantity',
            'APP_QUANTITY' => 'App  Quantity',
            'CURRENT_BALANCE' => 'Current  Balance',
            'UNIT_PRICE' => 'Unit  Price',
            'CREATED_AT' => 'Created  At',
            'UPDATED_AT' => 'Updated  At',
            'CREATED_BY' => 'Created  By',
            'UPDATED_BY' => 'Updated  By',
            'DELETED' => 'Deleted',
            'DELETED_AT' => 'Deleted  At',
        ];
    }


    /**
     * @inheritdoc
     * @return \backend\models\query\OrderItemsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\query\OrderItemsQuery(get_called_class());
    }
}
