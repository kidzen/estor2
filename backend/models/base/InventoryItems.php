<?php

namespace backend\models\base;

use Yii;

/**
 * This is the base model class for table "INVENTORY_ITEMS".
 *
 * @property string $ID
 * @property integer $INVENTORY_ID
 * @property integer $CHECKIN_TRANSACTION_ID
 * @property integer $CHECKOUT_TRANSACTION_ID
 * @property string $SKU
 * @property string $UNIT_PRICE
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class InventoryItems extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'UNIT_PRICE'], 'number'],
            [['INVENTORY_ID', 'CHECKIN_TRANSACTION_ID', 'CHECKOUT_TRANSACTION_ID', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['CHECKIN_TRANSACTION_ID'], 'required'],
            [['CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
            [['SKU'], 'string', 'max' => 50],
            [['ID', 'ID', 'ID'], 'unique', 'targetAttribute' => ['ID', 'ID', 'ID'], 'message' => 'The combination of  and ID has already been taken.'],
            [['ID', 'ID', 'DELETED', 'DELETED'], 'unique', 'targetAttribute' => ['ID', 'ID', 'DELETED', 'DELETED'], 'message' => 'The combination of ID and Deleted has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'INVENTORY_ITEMS';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'INVENTORY_ID' => 'Inventory  ID',
            'CHECKIN_TRANSACTION_ID' => 'Checkin  Transaction  ID',
            'CHECKOUT_TRANSACTION_ID' => 'Checkout  Transaction  ID',
            'SKU' => 'Sku',
            'UNIT_PRICE' => 'Unit  Price',
            'CREATED_AT' => 'Created  At',
            'UPDATED_AT' => 'Updated  At',
            'CREATED_BY' => 'Created  By',
            'UPDATED_BY' => 'Updated  By',
            'DELETED' => 'Deleted',
            'DELETED_AT' => 'Deleted  At',
        ];
    }


    /**
     * @inheritdoc
     * @return \backend\models\query\InventoryItemsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\query\InventoryItemsQuery(get_called_class());
    }
}
