<?php

namespace backend\models;

use Yii;
use \backend\models\base\InventoryItems as BaseInventoryItems;

/**
 * This is the model class for table "INVENTORY_ITEMS".
 */
class InventoryItems extends BaseInventoryItems
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['ID', 'UNIT_PRICE'], 'number'],
            [['INVENTORY_ID', 'CHECKIN_TRANSACTION_ID', 'CHECKOUT_TRANSACTION_ID', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['CHECKIN_TRANSACTION_ID'], 'required'],
            [['CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
            [['SKU'], 'string', 'max' => 50],
            [['ID', 'ID', 'ID'], 'unique', 'targetAttribute' => ['ID', 'ID', 'ID'], 'message' => 'The combination of  and ID has already been taken.'],
            [['ID', 'ID', 'DELETED', 'DELETED'], 'unique', 'targetAttribute' => ['ID', 'ID', 'DELETED', 'DELETED'], 'message' => 'The combination of ID and Deleted has already been taken.']
        ]);
    }
	
}
