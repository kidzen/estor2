<?php

namespace backend\models;

use Yii;
use \backend\models\base\OrderItems as BaseOrderItems;

/**
 * This is the model class for table "ORDER_ITEMS".
 */
class OrderItems extends BaseOrderItems
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['ID', 'RQ_QUANTITY', 'APP_QUANTITY', 'CURRENT_BALANCE', 'UNIT_PRICE'], 'number'],
            [['INVENTORY_ID', 'ORDER_ID'], 'required'],
            [['INVENTORY_ID', 'ORDER_ID', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
            [['ID', 'ID', 'DELETED', 'DELETED'], 'unique', 'targetAttribute' => ['ID', 'ID', 'DELETED', 'DELETED'], 'message' => 'The combination of ID and Deleted has already been taken.'],
            [['ID', 'ID', 'ID', 'ID'], 'unique', 'targetAttribute' => ['ID', 'ID', 'ID', 'ID'], 'message' => 'The combination of  and ID has already been taken.']
        ]);
    }
	
}
