<?php

namespace backend\models;

use Yii;
use \backend\models\base\Orders as BaseOrders;

/**
 * This is the model class for table "ORDERS".
 */
class Orders extends BaseOrders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['ID'], 'number'],
            [['TRANSACTION_ID'], 'required'],
            [['TRANSACTION_ID', 'ARAHAN_KERJA_ID', 'APPROVED', 'APPROVED_BY', 'VEHICLE_ID', 'CHECKOUT_BY', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['APPROVED_AT', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
            [['ORDER_DATE', 'REQUIRED_DATE', 'CHECKOUT_DATE'], 'string', 'max' => 7],
            [['ORDERED_BY', 'ORDER_NO'], 'string', 'max' => 20],
            [['ID', 'ID', 'DELETED', 'DELETED'], 'unique', 'targetAttribute' => ['ID', 'ID', 'DELETED', 'DELETED'], 'message' => 'The combination of ID and Deleted has already been taken.'],
            [['ID', 'ID', 'ID'], 'unique', 'targetAttribute' => ['ID', 'ID', 'ID'], 'message' => 'The combination of  and ID has already been taken.']
        ]);
    }
	
}
