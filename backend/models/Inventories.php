<?php

namespace backend\models;

use Yii;
use \backend\models\base\Inventories as BaseInventories;

/**
 * This is the model class for table "INVENTORIES".
 */
class Inventories extends BaseInventories
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['ID', 'QUANTITY'], 'number'],
            [['CATEGORY_ID'], 'required'],
            [['CATEGORY_ID', 'MIN_STOCK', 'APPROVED', 'APPROVED_BY', 'CREATED_BY', 'UPDATED_BY', 'DELETED'], 'integer'],
            [['APPROVED_AT', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT'], 'safe'],
            [['CARD_NO', 'CODE_NO', 'DESCRIPTION', 'LOCATION'], 'string', 'max' => 255],
            [['ID', 'ID', 'ID'], 'unique', 'targetAttribute' => ['ID', 'ID', 'ID'], 'message' => 'The combination of  and ID has already been taken.'],
            [['ID', 'ID', 'DELETED', 'DELETED'], 'unique', 'targetAttribute' => ['ID', 'ID', 'DELETED', 'DELETED'], 'message' => 'The combination of ID and Deleted has already been taken.']
        ]);
    }
	
}
