<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\InventoriesCheckin */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="inventories-checkin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'ID')->textInput(['maxlength' => true, 'placeholder' => 'ID']) ?>

    <?= $form->field($model, 'TRANSACTION_ID')->textInput(['placeholder' => 'TRANSACTION ID']) ?>

    <?= $form->field($model, 'INVENTORY_ID')->textInput(['placeholder' => 'INVENTORY ID']) ?>

    <?= $form->field($model, 'VENDOR_ID')->textInput(['placeholder' => 'VENDOR ID']) ?>

    <?= $form->field($model, 'ITEMS_QUANTITY')->textInput(['maxlength' => true, 'placeholder' => 'ITEMS QUANTITY']) ?>

    <?= $form->field($model, 'ITEMS_TOTAL_PRICE')->textInput(['maxlength' => true, 'placeholder' => 'ITEMS TOTAL PRICE']) ?>

    <?= $form->field($model, 'CHECK_DATE')->textInput(['maxlength' => true, 'placeholder' => 'CHECK DATE']) ?>

    <?= $form->field($model, 'CHECK_BY')->textInput(['placeholder' => 'CHECK BY']) ?>

    <?= $form->field($model, 'APPROVED')->textInput(['placeholder' => 'APPROVED']) ?>

    <?= $form->field($model, 'APPROVED_BY')->textInput(['placeholder' => 'APPROVED BY']) ?>

    <?= $form->field($model, 'APPROVED_AT')->textInput(['maxlength' => true, 'placeholder' => 'APPROVED AT']) ?>

    <?= $form->field($model, 'CREATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'CREATED AT']) ?>

    <?= $form->field($model, 'UPDATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'UPDATED AT']) ?>

    <?= $form->field($model, 'CREATED_BY')->textInput(['placeholder' => 'CREATED BY']) ?>

    <?= $form->field($model, 'UPDATED_BY')->textInput(['placeholder' => 'UPDATED BY']) ?>

    <?= $form->field($model, 'DELETED')->textInput(['placeholder' => 'DELETED']) ?>

    <?= $form->field($model, 'DELETED_AT')->textInput(['maxlength' => true, 'placeholder' => 'DELETED AT']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
