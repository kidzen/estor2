<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\InventoriesCheckinSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-inventories-checkin-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID')->textInput(['maxlength' => true, 'placeholder' => 'ID']) ?>

    <?= $form->field($model, 'TRANSACTION_ID')->textInput(['placeholder' => 'TRANSACTION ID']) ?>

    <?= $form->field($model, 'INVENTORY_ID')->textInput(['placeholder' => 'INVENTORY ID']) ?>

    <?= $form->field($model, 'VENDOR_ID')->textInput(['placeholder' => 'VENDOR ID']) ?>

    <?= $form->field($model, 'ITEMS_QUANTITY')->textInput(['maxlength' => true, 'placeholder' => 'ITEMS QUANTITY']) ?>

    <?php /* echo $form->field($model, 'ITEMS_TOTAL_PRICE')->textInput(['maxlength' => true, 'placeholder' => 'ITEMS TOTAL PRICE']) */ ?>

    <?php /* echo $form->field($model, 'CHECK_DATE')->textInput(['maxlength' => true, 'placeholder' => 'CHECK DATE']) */ ?>

    <?php /* echo $form->field($model, 'CHECK_BY')->textInput(['placeholder' => 'CHECK BY']) */ ?>

    <?php /* echo $form->field($model, 'APPROVED')->textInput(['placeholder' => 'APPROVED']) */ ?>

    <?php /* echo $form->field($model, 'APPROVED_BY')->textInput(['placeholder' => 'APPROVED BY']) */ ?>

    <?php /* echo $form->field($model, 'APPROVED_AT')->textInput(['maxlength' => true, 'placeholder' => 'APPROVED AT']) */ ?>

    <?php /* echo $form->field($model, 'CREATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'CREATED AT']) */ ?>

    <?php /* echo $form->field($model, 'UPDATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'UPDATED AT']) */ ?>

    <?php /* echo $form->field($model, 'CREATED_BY')->textInput(['placeholder' => 'CREATED BY']) */ ?>

    <?php /* echo $form->field($model, 'UPDATED_BY')->textInput(['placeholder' => 'UPDATED BY']) */ ?>

    <?php /* echo $form->field($model, 'DELETED')->textInput(['placeholder' => 'DELETED']) */ ?>

    <?php /* echo $form->field($model, 'DELETED_AT')->textInput(['maxlength' => true, 'placeholder' => 'DELETED AT']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
