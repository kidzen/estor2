<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\InventoriesCheckin */

$this->title = 'Create Inventories Checkin';
$this->params['breadcrumbs'][] = ['label' => 'Inventories Checkin', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventories-checkin-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
