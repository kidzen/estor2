<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\InventoriesCheckin */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Inventories Checkin', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventories-checkin-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Inventories Checkin'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'ID',
        'TRANSACTION_ID',
        'INVENTORY_ID',
        'VENDOR_ID',
        'ITEMS_QUANTITY',
        'ITEMS_TOTAL_PRICE',
        'CHECK_DATE',
        'CHECK_BY',
        'APPROVED',
        'APPROVED_BY',
        'APPROVED_AT',
        'CREATED_AT',
        'UPDATED_AT',
        'CREATED_BY',
        'UPDATED_BY',
        'DELETED',
        'DELETED_AT',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
