<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\InventoryItemsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-inventory-items-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID')->textInput(['maxlength' => true, 'placeholder' => 'ID']) ?>

    <?= $form->field($model, 'INVENTORY_ID')->textInput(['placeholder' => 'INVENTORY ID']) ?>

    <?= $form->field($model, 'CHECKIN_TRANSACTION_ID')->textInput(['placeholder' => 'CHECKIN TRANSACTION ID']) ?>

    <?= $form->field($model, 'CHECKOUT_TRANSACTION_ID')->textInput(['placeholder' => 'CHECKOUT TRANSACTION ID']) ?>

    <?= $form->field($model, 'SKU')->textInput(['maxlength' => true, 'placeholder' => 'SKU']) ?>

    <?php /* echo $form->field($model, 'UNIT_PRICE')->textInput(['maxlength' => true, 'placeholder' => 'UNIT PRICE']) */ ?>

    <?php /* echo $form->field($model, 'CREATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'CREATED AT']) */ ?>

    <?php /* echo $form->field($model, 'UPDATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'UPDATED AT']) */ ?>

    <?php /* echo $form->field($model, 'CREATED_BY')->textInput(['placeholder' => 'CREATED BY']) */ ?>

    <?php /* echo $form->field($model, 'UPDATED_BY')->textInput(['placeholder' => 'UPDATED BY']) */ ?>

    <?php /* echo $form->field($model, 'DELETED')->textInput(['placeholder' => 'DELETED']) */ ?>

    <?php /* echo $form->field($model, 'DELETED_AT')->textInput(['maxlength' => true, 'placeholder' => 'DELETED AT']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
