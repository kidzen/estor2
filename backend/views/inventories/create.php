<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Inventories */

$this->title = 'Create Inventories';
$this->params['breadcrumbs'][] = ['label' => 'Inventories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
