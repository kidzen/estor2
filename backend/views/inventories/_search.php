<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\InventoriesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-inventories-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID')->textInput(['maxlength' => true, 'placeholder' => 'ID']) ?>

    <?= $form->field($model, 'CATEGORY_ID')->textInput(['placeholder' => 'CATEGORY ID']) ?>

    <?= $form->field($model, 'CARD_NO')->textInput(['maxlength' => true, 'placeholder' => 'CARD NO']) ?>

    <?= $form->field($model, 'CODE_NO')->textInput(['maxlength' => true, 'placeholder' => 'CODE NO']) ?>

    <?= $form->field($model, 'DESCRIPTION')->textInput(['maxlength' => true, 'placeholder' => 'DESCRIPTION']) ?>

    <?php /* echo $form->field($model, 'QUANTITY')->textInput(['maxlength' => true, 'placeholder' => 'QUANTITY']) */ ?>

    <?php /* echo $form->field($model, 'MIN_STOCK')->textInput(['placeholder' => 'MIN STOCK']) */ ?>

    <?php /* echo $form->field($model, 'LOCATION')->textInput(['maxlength' => true, 'placeholder' => 'LOCATION']) */ ?>

    <?php /* echo $form->field($model, 'APPROVED')->textInput(['placeholder' => 'APPROVED']) */ ?>

    <?php /* echo $form->field($model, 'APPROVED_AT')->textInput(['maxlength' => true, 'placeholder' => 'APPROVED AT']) */ ?>

    <?php /* echo $form->field($model, 'APPROVED_BY')->textInput(['placeholder' => 'APPROVED BY']) */ ?>

    <?php /* echo $form->field($model, 'CREATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'CREATED AT']) */ ?>

    <?php /* echo $form->field($model, 'CREATED_BY')->textInput(['placeholder' => 'CREATED BY']) */ ?>

    <?php /* echo $form->field($model, 'UPDATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'UPDATED AT']) */ ?>

    <?php /* echo $form->field($model, 'UPDATED_BY')->textInput(['placeholder' => 'UPDATED BY']) */ ?>

    <?php /* echo $form->field($model, 'DELETED')->textInput(['placeholder' => 'DELETED']) */ ?>

    <?php /* echo $form->field($model, 'DELETED_AT')->textInput(['maxlength' => true, 'placeholder' => 'DELETED AT']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
