<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Inventories */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="inventories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'ID')->textInput(['maxlength' => true, 'placeholder' => 'ID']) ?>

    <?= $form->field($model, 'CATEGORY_ID')->textInput(['placeholder' => 'CATEGORY ID']) ?>

    <?= $form->field($model, 'CARD_NO')->textInput(['maxlength' => true, 'placeholder' => 'CARD NO']) ?>

    <?= $form->field($model, 'CODE_NO')->textInput(['maxlength' => true, 'placeholder' => 'CODE NO']) ?>

    <?= $form->field($model, 'DESCRIPTION')->textInput(['maxlength' => true, 'placeholder' => 'DESCRIPTION']) ?>

    <?= $form->field($model, 'QUANTITY')->textInput(['maxlength' => true, 'placeholder' => 'QUANTITY']) ?>

    <?= $form->field($model, 'MIN_STOCK')->textInput(['placeholder' => 'MIN STOCK']) ?>

    <?= $form->field($model, 'LOCATION')->textInput(['maxlength' => true, 'placeholder' => 'LOCATION']) ?>

    <?= $form->field($model, 'APPROVED')->textInput(['placeholder' => 'APPROVED']) ?>

    <?= $form->field($model, 'APPROVED_AT')->textInput(['maxlength' => true, 'placeholder' => 'APPROVED AT']) ?>

    <?= $form->field($model, 'APPROVED_BY')->textInput(['placeholder' => 'APPROVED BY']) ?>

    <?= $form->field($model, 'CREATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'CREATED AT']) ?>

    <?= $form->field($model, 'CREATED_BY')->textInput(['placeholder' => 'CREATED BY']) ?>

    <?= $form->field($model, 'UPDATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'UPDATED AT']) ?>

    <?= $form->field($model, 'UPDATED_BY')->textInput(['placeholder' => 'UPDATED BY']) ?>

    <?= $form->field($model, 'DELETED')->textInput(['placeholder' => 'DELETED']) ?>

    <?= $form->field($model, 'DELETED_AT')->textInput(['maxlength' => true, 'placeholder' => 'DELETED AT']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
