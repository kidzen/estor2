<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\OrderItems */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="order-items-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'ID')->textInput(['maxlength' => true, 'placeholder' => 'ID']) ?>

    <?= $form->field($model, 'INVENTORY_ID')->textInput(['placeholder' => 'INVENTORY ID']) ?>

    <?= $form->field($model, 'ORDER_ID')->textInput(['placeholder' => 'ORDER ID']) ?>

    <?= $form->field($model, 'RQ_QUANTITY')->textInput(['maxlength' => true, 'placeholder' => 'RQ QUANTITY']) ?>

    <?= $form->field($model, 'APP_QUANTITY')->textInput(['maxlength' => true, 'placeholder' => 'APP QUANTITY']) ?>

    <?= $form->field($model, 'CURRENT_BALANCE')->textInput(['maxlength' => true, 'placeholder' => 'CURRENT BALANCE']) ?>

    <?= $form->field($model, 'UNIT_PRICE')->textInput(['maxlength' => true, 'placeholder' => 'UNIT PRICE']) ?>

    <?= $form->field($model, 'CREATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'CREATED AT']) ?>

    <?= $form->field($model, 'UPDATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'UPDATED AT']) ?>

    <?= $form->field($model, 'CREATED_BY')->textInput(['placeholder' => 'CREATED BY']) ?>

    <?= $form->field($model, 'UPDATED_BY')->textInput(['placeholder' => 'UPDATED BY']) ?>

    <?= $form->field($model, 'DELETED')->textInput(['placeholder' => 'DELETED']) ?>

    <?= $form->field($model, 'DELETED_AT')->textInput(['maxlength' => true, 'placeholder' => 'DELETED AT']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
