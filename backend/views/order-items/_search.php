<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\OrderItemsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-order-items-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID')->textInput(['maxlength' => true, 'placeholder' => 'ID']) ?>

    <?= $form->field($model, 'INVENTORY_ID')->textInput(['placeholder' => 'INVENTORY ID']) ?>

    <?= $form->field($model, 'ORDER_ID')->textInput(['placeholder' => 'ORDER ID']) ?>

    <?= $form->field($model, 'RQ_QUANTITY')->textInput(['maxlength' => true, 'placeholder' => 'RQ QUANTITY']) ?>

    <?= $form->field($model, 'APP_QUANTITY')->textInput(['maxlength' => true, 'placeholder' => 'APP QUANTITY']) ?>

    <?php /* echo $form->field($model, 'CURRENT_BALANCE')->textInput(['maxlength' => true, 'placeholder' => 'CURRENT BALANCE']) */ ?>

    <?php /* echo $form->field($model, 'UNIT_PRICE')->textInput(['maxlength' => true, 'placeholder' => 'UNIT PRICE']) */ ?>

    <?php /* echo $form->field($model, 'CREATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'CREATED AT']) */ ?>

    <?php /* echo $form->field($model, 'UPDATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'UPDATED AT']) */ ?>

    <?php /* echo $form->field($model, 'CREATED_BY')->textInput(['placeholder' => 'CREATED BY']) */ ?>

    <?php /* echo $form->field($model, 'UPDATED_BY')->textInput(['placeholder' => 'UPDATED BY']) */ ?>

    <?php /* echo $form->field($model, 'DELETED')->textInput(['placeholder' => 'DELETED']) */ ?>

    <?php /* echo $form->field($model, 'DELETED_AT')->textInput(['maxlength' => true, 'placeholder' => 'DELETED AT']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
