<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Categories */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="categories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'ID')->textInput(['maxlength' => true, 'placeholder' => 'ID']) ?>

    <?= $form->field($model, 'NAME')->textInput(['maxlength' => true, 'placeholder' => 'NAME']) ?>

    <?= $form->field($model, 'DELETED')->textInput(['placeholder' => 'DELETED']) ?>

    <?= $form->field($model, 'DELETED_AT')->textInput(['maxlength' => true, 'placeholder' => 'DELETED AT']) ?>

    <?= $form->field($model, 'CREATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'CREATED AT']) ?>

    <?= $form->field($model, 'UPDATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'UPDATED AT']) ?>

    <?= $form->field($model, 'CREATED_BY')->textInput(['placeholder' => 'CREATED BY']) ?>

    <?= $form->field($model, 'UPDATED_BY')->textInput(['placeholder' => 'UPDATED BY']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
