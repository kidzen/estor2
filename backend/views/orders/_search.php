<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\OrdersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-orders-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID')->textInput(['maxlength' => true, 'placeholder' => 'ID']) ?>

    <?= $form->field($model, 'TRANSACTION_ID')->textInput(['placeholder' => 'TRANSACTION ID']) ?>

    <?= $form->field($model, 'ARAHAN_KERJA_ID')->textInput(['placeholder' => 'ARAHAN KERJA ID']) ?>

    <?= $form->field($model, 'ORDER_DATE')->textInput(['maxlength' => true, 'placeholder' => 'ORDER DATE']) ?>

    <?= $form->field($model, 'ORDERED_BY')->textInput(['maxlength' => true, 'placeholder' => 'ORDERED BY']) ?>

    <?php /* echo $form->field($model, 'ORDER_NO')->textInput(['maxlength' => true, 'placeholder' => 'ORDER NO']) */ ?>

    <?php /* echo $form->field($model, 'APPROVED')->textInput(['placeholder' => 'APPROVED']) */ ?>

    <?php /* echo $form->field($model, 'APPROVED_BY')->textInput(['placeholder' => 'APPROVED BY']) */ ?>

    <?php /* echo $form->field($model, 'APPROVED_AT')->textInput(['maxlength' => true, 'placeholder' => 'APPROVED AT']) */ ?>

    <?php /* echo $form->field($model, 'VEHICLE_ID')->textInput(['placeholder' => 'VEHICLE ID']) */ ?>

    <?php /* echo $form->field($model, 'REQUIRED_DATE')->textInput(['maxlength' => true, 'placeholder' => 'REQUIRED DATE']) */ ?>

    <?php /* echo $form->field($model, 'CHECKOUT_DATE')->textInput(['maxlength' => true, 'placeholder' => 'CHECKOUT DATE']) */ ?>

    <?php /* echo $form->field($model, 'CHECKOUT_BY')->textInput(['placeholder' => 'CHECKOUT BY']) */ ?>

    <?php /* echo $form->field($model, 'CREATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'CREATED AT']) */ ?>

    <?php /* echo $form->field($model, 'UPDATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'UPDATED AT']) */ ?>

    <?php /* echo $form->field($model, 'CREATED_BY')->textInput(['placeholder' => 'CREATED BY']) */ ?>

    <?php /* echo $form->field($model, 'UPDATED_BY')->textInput(['placeholder' => 'UPDATED BY']) */ ?>

    <?php /* echo $form->field($model, 'DELETED')->textInput(['placeholder' => 'DELETED']) */ ?>

    <?php /* echo $form->field($model, 'DELETED_AT')->textInput(['maxlength' => true, 'placeholder' => 'DELETED AT']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
