<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Orders */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'ID')->textInput(['maxlength' => true, 'placeholder' => 'ID']) ?>

    <?= $form->field($model, 'TRANSACTION_ID')->textInput(['placeholder' => 'TRANSACTION ID']) ?>

    <?= $form->field($model, 'ARAHAN_KERJA_ID')->textInput(['placeholder' => 'ARAHAN KERJA ID']) ?>

    <?= $form->field($model, 'ORDER_DATE')->textInput(['maxlength' => true, 'placeholder' => 'ORDER DATE']) ?>

    <?= $form->field($model, 'ORDERED_BY')->textInput(['maxlength' => true, 'placeholder' => 'ORDERED BY']) ?>

    <?= $form->field($model, 'ORDER_NO')->textInput(['maxlength' => true, 'placeholder' => 'ORDER NO']) ?>

    <?= $form->field($model, 'APPROVED')->textInput(['placeholder' => 'APPROVED']) ?>

    <?= $form->field($model, 'APPROVED_BY')->textInput(['placeholder' => 'APPROVED BY']) ?>

    <?= $form->field($model, 'APPROVED_AT')->textInput(['maxlength' => true, 'placeholder' => 'APPROVED AT']) ?>

    <?= $form->field($model, 'VEHICLE_ID')->textInput(['placeholder' => 'VEHICLE ID']) ?>

    <?= $form->field($model, 'REQUIRED_DATE')->textInput(['maxlength' => true, 'placeholder' => 'REQUIRED DATE']) ?>

    <?= $form->field($model, 'CHECKOUT_DATE')->textInput(['maxlength' => true, 'placeholder' => 'CHECKOUT DATE']) ?>

    <?= $form->field($model, 'CHECKOUT_BY')->textInput(['placeholder' => 'CHECKOUT BY']) ?>

    <?= $form->field($model, 'CREATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'CREATED AT']) ?>

    <?= $form->field($model, 'UPDATED_AT')->textInput(['maxlength' => true, 'placeholder' => 'UPDATED AT']) ?>

    <?= $form->field($model, 'CREATED_BY')->textInput(['placeholder' => 'CREATED BY']) ?>

    <?= $form->field($model, 'UPDATED_BY')->textInput(['placeholder' => 'UPDATED BY']) ?>

    <?= $form->field($model, 'DELETED')->textInput(['placeholder' => 'DELETED']) ?>

    <?= $form->field($model, 'DELETED_AT')->textInput(['maxlength' => true, 'placeholder' => 'DELETED AT']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
